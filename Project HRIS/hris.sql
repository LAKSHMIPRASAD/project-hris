-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hris` ;
-- -----------------------------------------------------
-- Schema hris
-- -----------------------------------------------------
USE `hris` ;

CREATE TABLE IF NOT EXISTS `hris`.`holiday_list` (
  `holiday_id` INT(11) NOT NULL,
  `holiday_name` VARCHAR(90) NULL DEFAULT NULL,
  `holiday_date` TIMESTAMP NULL DEFAULT NULL,
  `holiday_location` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`holiday_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`address_table`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`address_table` (
  `address_id` INT(11) NOT NULL AUTO_INCREMENT,
  `line1` VARCHAR(45) NULL DEFAULT NULL,
  `line2` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `state` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `pincode` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`address_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 63
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`address_table_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`address_table_audit` (
  `address_id` INT(11) NULL DEFAULT NULL,
  `line1` VARCHAR(45) NULL DEFAULT NULL,
  `line2` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `state` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `pincode` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `table_address_id_audit` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`table_address_id_audit`),
  INDEX `address_to_audit` (`address_id` ASC),
  CONSTRAINT `address_to_audit`
    FOREIGN KEY (`address_id`)
    REFERENCES `hris`.`address_table` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`messenger_table`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`messenger_table` (
  `type_of_messenger` VARCHAR(45) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `messenger_id` VARCHAR(100) NULL DEFAULT NULL,
  `messenger_key` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`messenger_key`))
ENGINE = InnoDB
AUTO_INCREMENT = 40
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`comment_box`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`comment_box` (
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `commet_box_id` INT(11) NOT NULL,
  PRIMARY KEY (`commet_box_id`),
  UNIQUE INDEX `commet_box_id_UNIQUE` (`commet_box_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`role_access`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`role_access` (
  `role` VARCHAR(45) NOT NULL,
  `mode` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`role`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '				';


-- -----------------------------------------------------
-- Table `hris`.`personal_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`personal_details` (
  `emp_id` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NULL DEFAULT NULL,
  `last_name` VARCHAR(45) NULL DEFAULT NULL,
  `gender` VARCHAR(20) NULL DEFAULT NULL,
  `date_of_birth` DATE NULL DEFAULT NULL,
  `current_address_id` INT(11) NULL DEFAULT NULL,
  `permanent_address_id` INT(11) NULL DEFAULT NULL,
  `phone_number_1` VARCHAR(15) NULL DEFAULT NULL,
  `phone_number_2` VARCHAR(15) NULL DEFAULT NULL,
  `email_id` VARCHAR(100) NULL DEFAULT NULL,
  `messenger_key` INT(11) NULL DEFAULT NULL,
  `emergency_contact_person` VARCHAR(45) NULL DEFAULT NULL,
  `Relation` VARCHAR(45) NULL DEFAULT NULL,
  `emergency_contact_number` VARCHAR(15) NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `aadhar_id` VARCHAR(20) NULL DEFAULT NULL,
  `pan_number` VARCHAR(20) NULL DEFAULT NULL,
  `UAN` VARCHAR(20) NULL DEFAULT NULL,
  `user_name` VARCHAR(45) NULL DEFAULT NULL,
  `two_wheeler_vehicle_no` VARCHAR(45) NULL DEFAULT NULL,
  `four_wheeler_vehicle_no` VARCHAR(45) NULL DEFAULT NULL,
  `role` VARCHAR(45) NULL DEFAULT NULL,
  `mode` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`emp_id`),
  INDEX `personal_details_comment_box_idx` (`commet_box_id` ASC),
  INDEX `current_address_idx` (`current_address_id` ASC),
  INDEX `permanent_address_idx` (`permanent_address_id` ASC),
  INDEX `messenger_idx` (`messenger_key` ASC),
  INDEX `role_idx` (`role` ASC),
  CONSTRAINT `current_address`
    FOREIGN KEY (`current_address_id`)
    REFERENCES `hris`.`address_table` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `messenger`
    FOREIGN KEY (`messenger_key`)
    REFERENCES `hris`.`messenger_table` (`messenger_key`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `permanent_address`
    FOREIGN KEY (`permanent_address_id`)
    REFERENCES `hris`.`address_table` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `personal_details_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `role`
    FOREIGN KEY (`role`)
    REFERENCES `hris`.`role_access` (`role`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`certification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`certification` (
  `emp_id` VARCHAR(45) NOT NULL,
  `certification_id` INT(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NULL DEFAULT NULL,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`certification_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  CONSTRAINT `certification_personal`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`personal_details` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`certification_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`certification_audit` (
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `certification_id` INT(11) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `certification_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`certification_audit_id`),
  INDEX `certification_comment_box_idx` (`commet_box_id` ASC),
  INDEX `certification_audit` (`emp_id` ASC),
  CONSTRAINT `certification_audit`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`certification` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `certification_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`certification_bonus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`certification_bonus` (
  `certification_id` INT(11) NULL DEFAULT NULL,
  `bonus` DECIMAL(10,0) NULL DEFAULT NULL,
  `certification_name` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `certification_bonus_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`certification_bonus_id`),
  INDEX `certification_bonus` (`certification_id` ASC),
  CONSTRAINT `certification_bonus`
    FOREIGN KEY (`certification_id`)
    REFERENCES `hris`.`certification` (`certification_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`certification_bonus_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`certification_bonus_audit` (
  `certification_id` INT(11) NULL DEFAULT NULL,
  `bonus` DECIMAL(10,0) NULL DEFAULT NULL,
  `certification_name` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `certification_bonus_audit_certification_id` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `certification_bonus_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`certification_bonus_audit_id`),
  INDEX `certification_bonus_audit` (`certification_id` ASC),
  CONSTRAINT `certification_bonus_audit`
    FOREIGN KEY (`certification_id`)
    REFERENCES `hris`.`certification_bonus` (`certification_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`checklist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`checklist` (
  `doc_id` INT(11) NOT NULL AUTO_INCREMENT,
  `doc_type` VARCHAR(45) NOT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`doc_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`checklist_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`checklist_audit` (
  `doc_id` INT(11) NULL DEFAULT NULL,
  `doc_type` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `checklist_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`checklist_audit_id`),
  INDEX `FK_CHECKLIST` (`doc_id` ASC),
  CONSTRAINT `FK_CHECKLIST`
    FOREIGN KEY (`doc_id`)
    REFERENCES `hris`.`checklist` (`doc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`comment_box_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`comment_box_audit` (
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `comment_box_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`comment_box_audit_id`),
  INDEX `comment_box_audit_idx` (`commet_box_id` ASC),
  CONSTRAINT `comment_box_audit`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`emp_doc_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`emp_doc_map` (
  `emp_doc_map_id` INT(11) NOT NULL AUTO_INCREMENT,
  `emp_id` VARCHAR(45) NOT NULL,
  `doc_id` INT(11) NULL DEFAULT NULL,
  `doc_path` VARCHAR(45) NOT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `flag` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`emp_doc_map_id`),
  UNIQUE INDEX `DocPath_UNIQUE` (`doc_path` ASC),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  UNIQUE INDEX `doc_id_UNIQUE` (`doc_id` ASC),
  INDEX `FK_HRIS_DOC_DOCTYPE_02_idx` (`doc_id` ASC),
  CONSTRAINT `FK_HRIS_DOC_DOCTYPE_02`
    FOREIGN KEY (`doc_id`)
    REFERENCES `hris`.`checklist` (`doc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_HRIS_EMP_DOC_01`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`personal_details` (`emp_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`emp_doc_encoded`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`emp_doc_encoded` (
  `emp_doc_encoded_id` INT(11) NOT NULL AUTO_INCREMENT,
  `doc_id` INT(11) NULL DEFAULT NULL,
  `document` BLOB NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`emp_doc_encoded_id`),
  INDEX `fk_emp_doc_encodded_idx` (`doc_id` ASC),
  CONSTRAINT `fk_emp_doc_encodded`
    FOREIGN KEY (`doc_id`)
    REFERENCES `hris`.`emp_doc_map` (`doc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`emp_doc_encoded_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`emp_doc_encoded_audit` (
  `doc_id_audit` INT(11) NOT NULL AUTO_INCREMENT,
  `document` BLOB NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `doc_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`doc_id_audit`),
  INDEX `encodded_audit_idx` (`doc_id` ASC),
  CONSTRAINT `encodded_audit`
    FOREIGN KEY (`doc_id`)
    REFERENCES `hris`.`emp_doc_encoded` (`doc_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`emp_doc_map_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`emp_doc_map_audit` (
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `doc_id` INT(11) NULL DEFAULT NULL,
  `doc_path` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `created_date` TIMESTAMP NULL DEFAULT NULL,
  `updated_date` TIMESTAMP NULL DEFAULT NULL,
  `emp_doc_map_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`emp_doc_map_audit_id`),
  INDEX `FK_EMPDOC` (`emp_id` ASC),
  CONSTRAINT `FK_EMPDOC`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`emp_doc_map` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`employment_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`employment_details` (
  `is_current` TINYINT(1) NULL DEFAULT NULL,
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `company_name` VARCHAR(250) NULL DEFAULT NULL,
  `location` INT(11) NULL DEFAULT NULL,
  `date_of_joining` DATE NULL DEFAULT NULL,
  `date_of_relieving` DATE NULL DEFAULT NULL,
  `designation` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `pk_employment_details` INT(11) NOT NULL AUTO_INCREMENT,
  `mode` INT(11) NOT NULL DEFAULT '1',
  `department_name` VARCHAR(45) NULL DEFAULT NULL,
  `manager_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`pk_employment_details`),
  INDEX `emplyee_details_comment_box_idx` (`commet_box_id` ASC),
  INDEX `FK_HRIS_EMP_EMPLOYMENT_02` (`emp_id` ASC),
  INDEX `office_location` (`location` ASC),
  CONSTRAINT `emplyee_details_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_HRIS_EMP_EMPLOYMENT_02`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`personal_details` (`emp_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `office_location`
    FOREIGN KEY (`location`)
    REFERENCES `hris`.`address_table` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`employment_details_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`employment_details_audit` (
  `is_current` TINYINT(1) NULL DEFAULT NULL,
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `company_name` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_joining` DATE NULL DEFAULT NULL,
  `date_of_relieving` DATE NULL DEFAULT NULL,
  `designation` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `created_on` DATE NULL DEFAULT NULL,
  `updated_on` DATE NULL DEFAULT NULL,
  `employment_details_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`employment_details_audit_id`),
  INDEX `FK_employment_details_audit` (`emp_id` ASC),
  CONSTRAINT `FK_employment_details_audit`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`employment_details` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`endorsement_form`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`endorsement_form` (
  `emp_id` VARCHAR(45) NOT NULL,
  `relation` VARCHAR(45) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `gender` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `endorsement_form_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`endorsement_form_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  INDEX `endorsement_form_comment_box_idx` (`commet_box_id` ASC),
  CONSTRAINT `endorsement_form_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `endorsement_personaldetails`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`personal_details` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`endorsement_form_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`endorsement_form_audit` (
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `relation` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_birth` DATE NULL DEFAULT NULL,
  `gender` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `endorsement_form_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`endorsement_form_audit_id`),
  INDEX `endorsement_audit` (`emp_id` ASC),
  CONSTRAINT `endorsement_audit`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`endorsement_form` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`exit_form_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`exit_form_question` (
  `question_id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_description` VARCHAR(45) NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`question_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`exit_form_question_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`exit_form_question_audit` (
  `queation_id` INT(11) NULL DEFAULT NULL,
  `question_description` VARCHAR(45) NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `exit_form_question_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`exit_form_question_audit_id`),
  INDEX `exit_question_audit` (`queation_id` ASC),
  CONSTRAINT `exit_question_audit`
    FOREIGN KEY (`queation_id`)
    REFERENCES `hris`.`exit_form_question` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`option_table`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`option_table` (
  `option_id` INT(11) NOT NULL AUTO_INCREMENT,
  `option_desciption` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`option_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`exit_form_question_response`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`exit_form_question_response` (
  `emp_id` VARCHAR(45) NOT NULL,
  `question_id` INT(11) NULL DEFAULT NULL,
  `option_id` INT(11) NULL DEFAULT NULL,
  `responce` VARCHAR(45) NULL DEFAULT NULL,
  `remarks` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `exit_form_question_response_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`exit_form_question_response_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  INDEX `opton_responce_idx` (`option_id` ASC),
  INDEX `question_responce_idx` (`question_id` ASC),
  CONSTRAINT `opton_responce`
    FOREIGN KEY (`option_id`)
    REFERENCES `hris`.`option_table` (`option_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `question_responce`
    FOREIGN KEY (`question_id`)
    REFERENCES `hris`.`exit_form_question` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`exit_form_question_responce_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`exit_form_question_responce_audit` (
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `question_id` INT(11) NULL DEFAULT NULL,
  `option_id` INT(11) NULL DEFAULT NULL,
  `responce` VARCHAR(45) NULL DEFAULT NULL,
  `remarks` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `exit_form_response_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`exit_form_response_audit_id`),
  INDEX `exit_form_comment_box_idx` (`commet_box_id` ASC),
  INDEX `exit_form_question_responce_audit` (`emp_id` ASC),
  CONSTRAINT `exit_form_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `exit_form_question_responce_audit`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`exit_form_question_response` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`message_comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`message_comment` (
  `comment` VARCHAR(250) NULL DEFAULT NULL,
  `message_id` INT(11) NOT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `commet_box_id` INT(11) NOT NULL,
  PRIMARY KEY (`message_id`),
  INDEX `fk_comment_box_idx` (`commet_box_id` ASC),
  CONSTRAINT `fk_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`message_comment_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`message_comment_audit` (
  `comment` VARCHAR(250) NULL DEFAULT NULL,
  `message_id` INT(11) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `message_comment_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`message_comment_audit_id`),
  INDEX `fk_messge_comment_idx` (`message_id` ASC),
  CONSTRAINT `fk_messge_comment`
    FOREIGN KEY (`message_id`)
    REFERENCES `hris`.`message_comment` (`message_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`messenger_table_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`messenger_table_audit` (
  `messenger_id` VARCHAR(100) NULL DEFAULT NULL,
  `type_of_messenger` VARCHAR(45) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `messenger_table_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `messenger_key` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`messenger_table_audit_id`),
  INDEX `messenger_audit_idx` (`messenger_key` ASC),
  CONSTRAINT `messenger_audit`
    FOREIGN KEY (`messenger_key`)
    REFERENCES `hris`.`messenger_table` (`messenger_key`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`option_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`option_audit` (
  `option_id` INT(11) NULL DEFAULT NULL,
  `option_desciption` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(1) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `option_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`option_audit_id`),
  INDEX `option_audit` (`option_id` ASC),
  CONSTRAINT `option_audit`
    FOREIGN KEY (`option_id`)
    REFERENCES `hris`.`option_table` (`option_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`passport_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`passport_details` (
  `emp_id` VARCHAR(45) NOT NULL,
  `passport_number` VARCHAR(45) NOT NULL,
  `date_of_issue` DATE NULL DEFAULT NULL,
  `date_of_expiry` DATE NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `passport_details_id` INT(11) NOT NULL,
  `emp_doc_map_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`passport_details_id`),
  UNIQUE INDEX `passnumber_UNIQUE` (`passport_number` ASC),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC),
  UNIQUE INDEX `passport_details_id_UNIQUE` (`passport_details_id` ASC),
  INDEX `fk_passport_details_comment_box_idx` (`commet_box_id` ASC),
  INDEX `fk_emp_doc` (`emp_doc_map_id` ASC),
  CONSTRAINT `fk_emp_doc`
    FOREIGN KEY (`emp_doc_map_id`)
    REFERENCES `hris`.`emp_doc_map` (`emp_doc_map_id`),
  CONSTRAINT `FK_HRIS_EMP_PASSPORT_01`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`personal_details` (`emp_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_passport_details_comment_box`
    FOREIGN KEY (`commet_box_id`)
    REFERENCES `hris`.`comment_box` (`commet_box_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`passport_details_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`passport_details_audit` (
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `passport_number` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_issue` DATE NULL DEFAULT NULL,
  `date_of_expiry` DATE NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `passport_details_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `passport_details_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`passport_details_audit_id`),
  INDEX `FK_PASSPORT` (`emp_id` ASC),
  CONSTRAINT `FK_PASSPORT`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`passport_details` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`personal_details_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`personal_details_audit` (
  `emp_id` VARCHAR(45) NULL DEFAULT NULL,
  `first_name` VARCHAR(45) NULL DEFAULT NULL,
  `middle_name` VARCHAR(45) NULL DEFAULT NULL,
  `last_name` VARCHAR(45) NULL DEFAULT NULL,
  `gender` VARCHAR(20) NULL DEFAULT NULL,
  `date_of_birth` DATE NULL DEFAULT NULL,
  `current_address_id` INT(11) NULL DEFAULT NULL,
  `permanent_address_id` INT(11) NULL DEFAULT NULL,
  `phone_number_1` VARCHAR(15) NULL DEFAULT NULL,
  `phone_number_2` VARCHAR(15) NULL DEFAULT NULL,
  `email_id` VARCHAR(100) NULL DEFAULT NULL,
  `messenger_key` INT(11) NOT NULL,
  `emergency_contact_person` VARCHAR(45) NULL DEFAULT NULL,
  `relation` VARCHAR(45) NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT NULL,
  `emergency_contact_number` VARCHAR(15) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `personal_details_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  `commet_box_id` INT(11) NULL DEFAULT NULL,
  `aadhar_id` VARCHAR(20) NULL DEFAULT NULL,
  `pan_number` VARCHAR(20) NULL DEFAULT NULL,
  `UAN` VARCHAR(20) NULL DEFAULT NULL,
  `two_wheeler_vehicle_no` VARCHAR(45) NULL DEFAULT NULL,
  `four_wheeler_vehicle_no` VARCHAR(45) NULL DEFAULT NULL,
  `role` VARCHAR(45) NULL DEFAULT NULL,
  `mode` INT(11) NULL DEFAULT NULL,
  `user_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`personal_details_audit_id`),
  UNIQUE INDEX `messenger_id_UNIQUE` (`messenger_key` ASC),
  INDEX `FK_PERSONAL` (`emp_id` ASC),
  CONSTRAINT `FK_PERSONAL`
    FOREIGN KEY (`emp_id`)
    REFERENCES `hris`.`personal_details` (`emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`visa_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`visa_details` (
  `passport_number` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_expiry` DATE NULL DEFAULT NULL,
  `visa_type` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `visa_details_id` INT(11) NOT NULL AUTO_INCREMENT,
  `emp_doc_map_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`visa_details_id`),
  INDEX `FK_HRIS_EMP_PASSPORT_VISA_01` (`passport_number` ASC),
  INDEX `fk_emp_doc_id` (`emp_doc_map_id` ASC),
  CONSTRAINT `fk_emp_doc_id`
    FOREIGN KEY (`emp_doc_map_id`)
    REFERENCES `hris`.`emp_doc_map` (`emp_doc_map_id`),
  CONSTRAINT `FK_HRIS_EMP_PASSPORT_VISA_01`
    FOREIGN KEY (`passport_number`)
    REFERENCES `hris`.`passport_details` (`passport_number`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `hris`.`visa_details_audit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`visa_details_audit` (
  `passport_number` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_expiry` DATE NULL DEFAULT NULL,
  `visa_type` VARCHAR(45) NULL DEFAULT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `created_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` TIMESTAMP NULL DEFAULT NULL,
  `created_on` TIMESTAMP NULL DEFAULT NULL,
  `visa_details_audit_id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`visa_details_audit_id`),
  INDEX `FK_VISA` (`passport_number` ASC),
  CONSTRAINT `FK_VISA`
    FOREIGN KEY (`passport_number`)
    REFERENCES `hris`.`visa_details` (`passport_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `hris` ;

-- -----------------------------------------------------
-- Placeholder table for view `hris`.`adhoc_report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hris`.`adhoc_report` (`emp_id` INT, `first_name` INT, `middle_name` INT, `last_name` INT, `gender` INT, `email_id` INT, `date_of_birth` INT, `emergency_contact_number` INT, `Relation` INT, `passport_number` INT, `date_of_issue` INT, `passport_date_of_expiry` INT, `visa_type` INT, `visa_date_of_expiry` INT, `visa_country` INT, `type_of_messenger` INT, `location` INT, `date_of_joining` INT, `designation` INT, `company_name` INT, `current_address` INT, `permanent_address` INT, `certification_id` INT, `certification_date` INT, `certification_bonus_id` INT, `certification_bonus` INT, `certification_name` INT, `certification_country` INT);

-- -----------------------------------------------------
-- View `hris`.`adhoc_report`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hris`.`adhoc_report`;
USE `hris`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `hris`.`adhoc_report` AS select `hris`.`personal_details`.`emp_id` AS `emp_id`,`hris`.`personal_details`.`first_name` AS `first_name`,`hris`.`personal_details`.`middle_name` AS `middle_name`,`hris`.`personal_details`.`last_name` AS `last_name`,`hris`.`personal_details`.`gender` AS `gender`,`hris`.`personal_details`.`email_id` AS `email_id`,`hris`.`personal_details`.`date_of_birth` AS `date_of_birth`,`hris`.`personal_details`.`emergency_contact_number` AS `emergency_contact_number`,`hris`.`personal_details`.`Relation` AS `Relation`,`hris`.`passport_details`.`passport_number` AS `passport_number`,`hris`.`passport_details`.`date_of_issue` AS `date_of_issue`,`hris`.`passport_details`.`date_of_expiry` AS `passport_date_of_expiry`,`hris`.`visa_details`.`visa_type` AS `visa_type`,`hris`.`visa_details`.`date_of_expiry` AS `visa_date_of_expiry`,`hris`.`visa_details`.`country` AS `visa_country`,`hris`.`messenger_table`.`type_of_messenger` AS `type_of_messenger`,`hris`.`employment_details`.`location` AS `location`,`hris`.`employment_details`.`date_of_joining` AS `date_of_joining`,`hris`.`employment_details`.`designation` AS `designation`,`hris`.`employment_details`.`company_name` AS `company_name`,concat_ws('\n',`a`.`line1`,`a`.`line2`,char(32),`a`.`city`,char(32),`a`.`state`,char(32),`a`.`country`,char(32),`a`.`pincode`) AS `current_address`,concat_ws('\n',`b`.`line1`,`b`.`line2`,char(32),`b`.`city`,char(32),`b`.`state`,char(32),`b`.`country`,char(32),`b`.`pincode`) AS `permanent_address`,`hris`.`certification`.`certification_id` AS `certification_id`,`hris`.`certification`.`date` AS `certification_date`,`hris`.`certification_bonus`.`certification_id` AS `certification_bonus_id`,`hris`.`certification_bonus`.`bonus` AS `certification_bonus`,`hris`.`certification_bonus`.`certification_name` AS `certification_name`,`hris`.`certification_bonus`.`country` AS `certification_country` from ((((((((`hris`.`personal_details` left join `hris`.`passport_details` on((`hris`.`personal_details`.`emp_id` = `hris`.`passport_details`.`emp_id`))) left join `hris`.`visa_details` on((`hris`.`passport_details`.`passport_number` = `hris`.`visa_details`.`passport_number`))) left join `hris`.`employment_details` on((`hris`.`employment_details`.`emp_id` = `hris`.`personal_details`.`emp_id`))) left join `hris`.`messenger_table` on((`hris`.`personal_details`.`messenger_key` = `hris`.`messenger_table`.`messenger_key`))) left join `hris`.`address_table` `a` on((`hris`.`personal_details`.`current_address_id` = `a`.`address_id`))) left join `hris`.`address_table` `b` on((`hris`.`personal_details`.`permanent_address_id` = `b`.`address_id`))) left join `hris`.`certification` on((`hris`.`personal_details`.`emp_id` = `hris`.`certification`.`emp_id`))) left join `hris`.`certification_bonus` on((`hris`.`certification_bonus`.`certification_id` = `hris`.`certification`.`certification_id`)));
USE `Notification_Sys`;

DELIMITER $$
USE `Notification_Sys`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Notification_Sys`.`provider_BEFORE_INSERT` BEFORE INSERT ON `provider` FOR EACH ROW
BEGIN

END$$

USE `Notification_Sys`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Notification_Sys`.`provider_BEFORE_UPDATE` BEFORE UPDATE ON `provider` FOR EACH ROW
BEGIN

END$$

USE `Notification_Sys`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Notification_Sys`.`status_code_BEFORE_INSERT` BEFORE INSERT ON `status_code` FOR EACH ROW
BEGIN

END$$

USE `Notification_Sys`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Notification_Sys`.`status_code_BEFORE_UPDATE` BEFORE UPDATE ON `status_code` FOR EACH ROW
BEGIN

END$$

USE `Notification_Sys`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Notification_Sys`.`notification_system_BEFORE_INSERT` BEFORE INSERT ON `notification_system` FOR EACH ROW
BEGIN

END$$

USE `Notification_Sys`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Notification_Sys`.`notification_system_BEFORE_UPDATE` BEFORE UPDATE ON `notification_system` FOR EACH ROW
BEGIN

END$$


DELIMITER ;
USE `hris`;

DELIMITER $$
USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`address_table_trg`
BEFORE UPDATE ON `hris`.`address_table`
FOR EACH ROW
BEGIN
INSERT INTO address_table_audit(address_id,line1,line2,city,state,country,pincode,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.address_id,OLD.line1,OLD.line2,OLD.city,OLD.state,OLD.country,OLD.pincode,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`messenger_table_trg`
BEFORE UPDATE ON `hris`.`messenger_table`
FOR EACH ROW
BEGIN
INSERT INTO messenger_table_audit(messenger_key,messenger_id,type_of_messenger,status,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.messenger_key,OLD.messenger_id,OLD.type_of_messenger,OLD.status,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`comment_box_BEFORE_UPDATE`
BEFORE UPDATE ON `hris`.`comment_box`
FOR EACH ROW
BEGIN
INSERT INTO comment_box_audit(commet_box_id,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.commet_box_id,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`personal_details_audit_trg`
BEFORE UPDATE ON `hris`.`personal_details`
FOR EACH ROW
BEGIN

INSERT INTO personal_details_audit(role,four_wheeler_vehicle_no,two_wheeler_vehicle_no,commet_box_id,emp_id,first_name,middle_name,last_name,gender,date_of_birth,current_address_id,permanent_address_id,phone_number_1,phone_number_2,email_id,messenger_key,emergency_contact_person,Relation,emergency_contact_number,active,created_by,created_on,updated_by,updated_on) VALUES 

(OLD.role,OLD.four_wheeler_vehicle_no,OLD.two_wheeler_vehicle_no,OLD.commet_box_id,OLD.emp_id,OLD.first_name,OLD.middle_name,OLD.last_name,OLD.gender,OLD.date_of_birth,OLD.current_address_id,OLD.permanent_address_id,OLD.phone_number_1,OLD.phone_number_2,OLD.email_id,messenger_key,OLD.emergency_contact_person,OLD.Relation,OLD.emergency_contact_number,OLD.active,OLD.created_by,OLD.created_on,OLD.updated_by,now());

END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`certification_trg`
BEFORE UPDATE ON `hris`.`certification`
FOR EACH ROW
BEGIN
INSERT INTO certification_audit(commet_box_id,emp_id,certification_id,date,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.commet_box_id,OLD.emp_id,OLD.certification_id,OLD.date,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`certification_bonus_trg`
BEFORE UPDATE ON `hris`.`certification_bonus`
FOR EACH ROW
BEGIN
INSERT INTO certification_bonus_audit(certification_id,bonus,certification_name,country,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.certification_id,OLD.bonus,OLD.certification_name,OLD.country,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`checklist_audit_trg`
BEFORE UPDATE ON `hris`.`checklist`
FOR EACH ROW
BEGIN
INSERT INTO checklist_audit(doc_id,doc_type,status,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.doc_id,OLD.doc_type,OLD.status,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`emp_doc_map_audit_trg`
BEFORE UPDATE ON `hris`.`emp_doc_map`
FOR EACH ROW
BEGIN
INSERT INTO emp_doc_map_audit(emp_id,doc_id,doc_path,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.emp_id,OLD.doc_id,OLD.doc_path,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`emp_doc_encoded_trg`
BEFORE UPDATE ON `hris`.`emp_doc_encoded`
FOR EACH ROW
BEGIN
INSERT INTO emp_doc_encoded_audit(doc_id,document,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.doc_id,OLD.document,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`employment_details_audit_trg`
BEFORE UPDATE ON `hris`.`employment_details`
FOR EACH ROW
BEGIN
INSERT INTO employment_details_audit(commet_box_id,is_current,emp_id,company_name,location,date_of_joining,date_of_relieving,designation,created_by,created_on,updated_by,updated_on) VALUES 
(old.commet_box_id,OLD.is_current,OLD.emp_id,OLD.company_name,OLD.location,OLD.date_of_joining,OLD.date_of_relieving,OLD.designation,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`endorsement_form_audit_trg`
BEFORE UPDATE ON `hris`.`endorsement_form`
FOR EACH ROW
BEGIN
INSERT INTO endorsement_form_audit(commet_box_id,emp_id,relation,date_of_birth,gender,created_by,created_on,updated_by,updated_on) VALUES 
(old.commet_box_id,OLD.emp_id,OLD.relation,OLD.date_of_birth,OLD.gender,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`exit_form_question_trg`
BEFORE UPDATE ON `hris`.`exit_form_question`
FOR EACH ROW
BEGIN
INSERT INTO exit_form_question_audit(question_id,question_description,active,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.question_id,OLD.question_description,OLD.active,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`option_table_trg`
BEFORE UPDATE ON `hris`.`option_table`
FOR EACH ROW
BEGIN
INSERT INTO option_audit(option_id,option_desciption,status,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.option_id,OLD.option_desciption,OLD.status,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`exit_form_question_response_trg`
BEFORE UPDATE ON `hris`.`exit_form_question_response`
FOR EACH ROW
BEGIN
INSERT INTO exit_form_question_response_audit(commet_box_id,emp_id,question_id,option_id,responce,remarks,created_by,created_on,updated_by,updated_on) VALUES 
(old.commet_box_id,OLD.emp_id,OLD.question_id,OLD.option_id,OLD.responce,OLD.remarks,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`message_comment_BEFORE_UPDATE`
BEFORE UPDATE ON `hris`.`message_comment`
FOR EACH ROW
BEGIN
INSERT INTO message_comment_audit(comment,message_id,comment_box_id,created_by,created_on,updated_by,updated_on) VALUES 
(comment,message_id,comment_box_id,OLD.created_by,OLD.created_on,OLD.updated_by,now());

END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`passport_details_audit_trg`
BEFORE UPDATE ON `hris`.`passport_details`
FOR EACH ROW
BEGIN
INSERT INTO passport_details_audit(passport_details_id,commet_box_id,emp_id,passport_number,date_of_issue,date_of_expiry,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.passport_details_id,OLD.commet_box_id,OLD.emp_id,OLD.passport_number,OLD.date_of_issue,OLD.date_of_expiry,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$

USE `hris`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `hris`.`visa_details_audit_trg`
BEFORE UPDATE ON `hris`.`visa_details`
FOR EACH ROW
BEGIN
INSERT INTO visa_details_audit(passport_number,date_of_expiry,visa_type,country,created_by,created_on,updated_by,updated_on) VALUES 
(OLD.passport_number,OLD.date_of_expiry,OLD.visa_type,OLD.country,OLD.created_by,OLD.created_on,OLD.updated_by,now());
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
