<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript" src="js/tablefilter.js"> </script> 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<style type="text/css" src="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css"></style>
<style>
	table {
	width:90%;
	border-top:1px solid #e5eff8;
	border-right:1px solid #e5eff8;
	margin:1em auto;
	border-collapse:collapse;
	}
	td,th{
	
	border-bottom:1px solid #e5eff8;
	border-left:1px solid #e5eff8;
	padding:.3em 1em;
	text-align:center;
	}
	#submit{
	display:block;
	margin-top: 250px;
	margin-left:10%;
	}
	sel{
	display:inline-block;
	flaot:left;
	}
	#columns{
	margin-left:5%;
	display:inline-block;
	float:left;
	}
	#column,#criteria{
	display:none;
	flaot:left;
	}
	.crit{
	display:none;
	}
	#where{
	display:block;
	}
	.row{
	width:80%;
	margin-left:5%;
	}
	input{
	    line-height: normal;
	    }
</style>
<script type="text/javascript">
$(document).ready(function() {

	$('table[name=example-table]').tableFilter({
	
		//input : "input[type=search]", Default element
		
		trigger : {
		
			event 	: "keyup",
			//element : "button[name=btn-filtro]"
		},

		//timeout: 80,

		sort : true,

		//caseSensitive : false, Default

		callback : function() { /* Callback ap�s o filtro */

		},
		
		notFoundElement : ".not-found"
	});

	
});
$(document).ready(function(){
	
	$("#where").click(function(){
		$(".crit").css("display","inline-block");
		
	});
	
	$("#add").click(function(){
		$('#criteria').append(" <select name='criteria'><option>and</option><option>or</option></select>"+'<br>'
+"<select class='column_name' name='criteria'><option selected='selected'>Column</option><option value='emp_id'>Employee ID</option><option value='first_name'>First Name</option>"
		        +" <option value='middle_name'>Middle Name</option><option	value='last_name' >Last Name</option>"
		        +" <option value='company_name'>Company</option><option value='designation'>Designation</option>"
		        +" <option value='passport_number'>Passport Number</option><option	value='visa_type'>Visa Type</option>"
		        +" <option value='visa_country'>Visa Country</option><option value='type_of_messenger'>Messenger</option></select>"
	       +" <select class='condition' name='criteria'><option selected='selected'>Condtion</option>"
	        +"	<option value='='>Equal</option>  	<option value='!='>Not Equal</option>"
	        +"	<option value='<'>Less than</option><option value='>'> Greater than</option> </select>"
	       +" <input type='text' name='criteria'/>");
	});
	
	
});


</script>
<title>Insert title here</title>
</head>
<body>
	<h3 style="margin-left:4%">Adhoc Reporting</h3>
	<form action="Controller.do?customAction=queryBuilder" method="post">
	<div id="columns">
	    <input type=checkbox value="emp_id" name="select" class="sel">Employee ID<br>
        <input type=checkbox value="first_name" name="select" class="sel">First Name<br>
        <input type=checkbox value="middle_name" name="select" class="sel">Middle Name<br>
        <input type=checkbox value="last_name" name="select"class="sel">Last Name<br>                
        <input type="checkbox" value="email_id" name="select"class="sel"> Email ID<br>
        <input type="checkbox" value="company_name" name="select"class="sel">Company<br>
        <input type="checkbox" value="designation" name="select"class="sel">Designation<br>
        <input type="checkbox" value="current_address" name="select"class="sel">Current Address <br>
        <input type="checkbox" value="permanent_address" name="select"class="sel">Permanent Address <br>
        <input type=checkbox value="passport_number" name="select"class="sel">Passport Number<br>
        <input type=checkbox value="visa_type" name="select"class="sel">Visa Type<br>
        <input type=checkbox value="visa_country" name="select"class="sel">Visa Country<br>
        <input type="checkbox" value="type_of_messenger" name="select"class="sel">Messenger<br>
        </div>
        <button id="where" type="button">where</button>
        <div id="criteria" class="crit">
        <select class="column_name" name="criteria">
        	<option selected="selected">Column</option>
	        <option value="emp_id">Employee ID</option>
	        <option value="first_name">First Name</option>
	        <option value="middle_name">Middle Name</option>
	        <option	value="last_name" >Last Name</option>
	        <option value="company_name">Company</option>
	        <option value="designation">Designation</option>
	        <option value="passport_number">Passport Number</option>
	        <option	value="visa_type" >Visa Type</option>
	        <option value="visa_country">Visa Country</option>
			<option value="type_of_messenger">Messenger</option>
        </select>
        <select class="condition" name="criteria">
        	<option selected="selected">Condtion</option>
        	<option value="=">Equal</option>
        	<option value="!=">Not Equal</option>
        	<option value='<'>Less than</option>
        	<option value='>'> Greater than</option>
        	<option value='like'>Like</option>
        </select>
        <input type="text" name="criteria"/>
        
        </div>
        <button id="add" type="button" class="crit" >+</button>
    <input type="submit" id="submit">
    <br>
    <div class="row">
				<div class="col-md-10">
					<input type="search" class="form-control" placeholder="Filter">
				</div>
				
				<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-block" name="btn-filtro">Search</button>
				</div>
			</div>
    
    <table  name="example-table"  class="table table-bordered table-striped">
    	<tr>
	    	<c:forEach var="column" items="${columnNames }">
	    		<th>${column }</th>
	    	</c:forEach>
    	</tr>
    	<c:forEach var="result" items="${results }" >
	    	<tr>
	    		<c:forEach var="data" items="${result}">
	    				<td>${data }</td>
	    		</c:forEach>
	    	</tr>
    	</c:forEach>
    	
    </table>
    

</form>
</body>
</html>