<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | HRIS</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>  
     <script src="external/jquery/jquery.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="jquery-ui.css" rel="stylesheet">
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/Chart.js"></script>



</head><!--/head-->

<body class="homepage">

    <header id="header">
       



        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
            <div id="partition">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"></a><img src="images/logo.gif" id="logo1" alt="logo1"><div id="lefttabs"><font color="white"><br>Welcome Shriya Shekhar| E297<br>Software Engineer Level-1</font></div>

                     <div id="helptab"><a href="#">Help?</a></div><div id="display"><img src="images/imagepic.jpg" id="logo2" alt="logo2"></div>
                </div>
				



            </div>   

             

                <!--new right end-->
                <div id="profiling">


                <div id="leftones">
                 <div class="collapse navbar-collapse navbar-right">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="index.html">Home</a></li>
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Profile<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                 <li><a href="blog-item.html">Settings</a></li>
                                                <li><a href="password.html">Change password</a></li>
                                                <li><a href="update.html">Update info</a></li>
                                                <li><a href="docs.html">My documents</a></li>
                                            </ul>
                                        </li>
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Forms<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog-item.html">Item 1</a></li>
                                                <li><a href="pricing.html">Item 2</a></li>
                                                <li><a href="404.html">Item 3</a></li>
                                                <li><a href="shortcodes.html">Item 4</a></li>
                                            </ul>
                                        </li>
                                       
                                        <li><a href="portfolio.html">TimeSheet</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog-item.html">Attrition Report</a></li>
                                                <li><a href="pricing.html">New Joiner Report</a></li>
                                                
                                            </ul>
                                        </li>
                                        <li><a href="blog.html">System Set Up</a></li> 
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Policy Centre<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog-item.html">Work Culture and Env. Policies</a></li>
                                                <li><a href="pricing.html">Condition of Employment</a></li>
                                                <li><a href="pricing.html">Payroll and Employee benifits</a></li>
                                                <li><a href="pricing.html">Leave Policies</a></li>
                                                <li><a href="pricing.html">Relocation Policy</a></li>
                                                <li><a href="pricing.html">Travel Policy</a></li>
                                                <li><a href="pricing.html">Performance appraisal</a></li>
                                                <li><a href="pricing.html">Discipline and misconduct</a></li>
                                                <li><a href="pricing.html">Dress code policy</a></li>
                                                <li><a href="pricing.html">General Policy</a></li>
                                                
                                            </ul>
                                        </li>
                                                               
                                    </ul>
                                </div>
                </div>

                <div id="righttabs">
                                 <div class="collapse navbar-collapse navbar-right">
                                    <ul class="nav navbar-nav">
                                       
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Widgets<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                
                                                
                                                   <li> <label><input type="checkbox"  data-widget-name="js-widget-A" class="widget" checked/> &nbsp;Widget A</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-B" class="widget" checked/> &nbsp;Widget B</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-C" class="widget" checked/> &nbsp;Widget C</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-D" class="widget" checked/>&nbsp; Widget D</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-E" class="widget" checked/> &nbsp;Widget E</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-F" class="widget" checked/> &nbsp;Widget F</label></li>
                                           





                                               
                                            </ul>
                                        </li>
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog-item.html">Item 1</a></li>
                                                <li><a href="pricing.html">Item 2</a></li>
                                                <li><a href="404.html">Item 3</a></li>
                                                <li><a href="shortcodes.html">Item 4</a></li>
                                            </ul>
                                        </li>
                                       
                                        <li><a href="portfolio.html">Change Password</a></li>
                                        
                                                               
                                    </ul>
                </div>               

                                
                </div>

            

            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
            <div style="height:245px">
                


                        <!-- BEGIN: XHTML for example 1.4 -->

                        <div id="example-1-4">

                            <div id="containment">
                               
                                <div class="column">

                                    <ul class="sortable-list">
                                        <li class="sortable-item js-widget-A js-widget-container col-sm-3" id="graph">
                                        <canvas id="mycanvas" width:"200" height:"60"></li>
                                        <li class="sortable-item js-widget-B js-widget-container col-sm-3" >
                                            
                                            <a  href="http://www.openair.com/" title="Openair" target="_blank"><img class="imglink" src="http://www.geelus.com/wp-content/uploads/2013/09/TimeSheet.png"/></a>
                                            <a  href="https://mcfadyen.atlassian.net/secure/Dashboard.jspa" title="Jira" target="_blank"><img class="imglink" src="https://design.atlassian.com/images/brand/logo-02.png"/></a>
                                             <a  href="https://mcfadyenconsulting.greythr.com/home.do" title="GreyTip" target="_blank"><img class="imglink" src="https://cdn4.iconfinder.com/data/icons/money/512/21-512.png"/></a>
                                            <a  href="https://www.smartrecruiters.com/" title="SmartRecruiter" target="_blank"><img class="imglink"  src="http://ww1.prweb.com/prfiles/2014/08/29/12363891/gI_59710_SmartRecruiters%20Bulb.png"/></a>
 

                                        </li>
                                        <li class="sortable-item js-widget-C js-widget-container col-sm-3" >Sortable item C</li>
                                        <li class="sortable-item js-widget-D js-widget-container col-sm-3" >Sortable item D</li>
                                        <li class="sortable-item js-widget-E js-widget-container col-sm-3" >Sortable item E</li>
                                        <li class="sortable-item js-widget-F js-widget-container col-sm-3" >Sortable item F</li>
                                    </ul>

                                </div>

                                
                                

                            </div>



                            

                        </div>
                        <div id="todolist">
                            <div id="google_calender" ><iframe src="https://calendar.google.com/calendar/embed?height=350&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=shriya711%40gmail.com&amp;color=%231B887A&amp;src=en.indian%23holiday%40group.v.calendar.google.com&amp;color=%23125A12&amp;ctz=Asia%2FCalcutta" style="border-width:0" width="450" height="350" frameborder="0" scrolling="no"></iframe></div>
                        </div>
                 
            <!-- END: XHTML for example 1.4 -->
        





    
    <section id="bottom" >
        
    </section><!--/#bottom-->

    




    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">McFadyen Consulting Solutions</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
    </footer><!--/#footer-->
    
    
</body>
</html>