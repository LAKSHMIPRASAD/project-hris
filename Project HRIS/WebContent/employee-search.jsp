<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="header.jsp"/><br>
<body>
	<div class="container">
	<form class="form-horizontal" method="post" action="employee-search">
		<div class="col-xs-4">
		<label class="control-label ">Search Criteria:</label> 
		<select	class="form-control" id="search-column" name="type">
			<option value="employeeId">Employee Id</option>
			<option value="employeeName">Employee Name</option>
			<option value="phoneNumber">Phone Number</option>
			<option value="email">Email</option>
		</select>
		</div>	
		<div class="col-xs-4">
		<label class="control-label">Search Text:</label>
		<input type="text" name="searchText" class="form-control ">
		</div>
		<div class="col-xs-4">
		<label class="control-label">Click to Search:</label>
		<input type="submit" class="btn btn-success form-control" value="Search">
		</div>
		<span class="col-xs-12" style="height:25px"></span>
		<div class="col-xs-12">
		
			<div class="panel-heading clearfix">
				<p id="displayResult" hidden>No results, Please alter
					your query parameters</p>
			</div>
			<table class="table table-hover" style="width: 100%">
				<tr>
					<th>Employee - id</th>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Last Name</th>
					<th>Gender</th>
					<th>Date of Birth</th>
					<th>Phone Number</th>
					<th>Email</th>
				</tr>
				<c:if test="${not empty resultList }">
					<c:forEach items="${resultList}" var="map">
						<tr>
							<c:forEach items="${map}" var="entry">
								<td>${entry.value}</td>
							</c:forEach>
						</tr>
					</c:forEach>
				</c:if>
				<c:if test="${empty resultList }">
					<script>
					document.getElementById("displayResult").style.display="block";
					</script>
				</c:if>
			</table>
		</div>			
	</form>
	</div>
</body>
<jsp:include page="footer.jsp"/>