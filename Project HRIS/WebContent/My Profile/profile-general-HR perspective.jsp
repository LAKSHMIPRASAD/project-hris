 <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Profile | General</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/header-footer.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link href="../css/profile.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script type="text/javascript" src="../js/HR perspective.js"></script>
<script type="text/javascript" src="../js/popups.js"></script>
<script type="text/javascript" src="../js/RoleBased.js"></script>


<script> 
$(function(){
  $("#header").load("../header/header.jsp"); 
  $("#footer").load("../footer/footer.html"); 
});
</script>
</head>
<body>
<!--<jsp:useBean id="displayBean" scope="session" class="com.mcfadyen.hris.beans.ProfileBean"/>-->
       <div id="header"></div>
       <div id="overlay"></div>
       <div class="container-floating">

              <div class="form-entire">

                    
                           <form id="integerForm" class="form-horizontal" role="form"
                                  action="profile.do">
                                  <div class="col-sm-2 photos">
                                         <div class="profile-photo">
                                                <img id="uploadPreview1" , width="160px" , height="160px" title="Profile Picture" src="http://www.iitiimatrimony.com/images/iitiim.jpg"/> <label
                                                       class="add-photo-btn">Upload Profile Pic<span> <input
                                                              id="uploadImage1" type="file" class="upload"
                                                              onchange="loadFile()" /></span>
                                                </label>
                                         </div>
                                         <div class="profile-photo">
                                                <img id="uploadPreview2" , width="160px" , height="160px" title="Casual Picture" src="https://cdn2.iconfinder.com/data/icons/windows-8-metro-style/256/cool.png"/> <label
                                                       class="add-photo-btn">Upload Casual Pic<span> <input
                                                              id="uploadImage2" type="file" class="upload"
                                                              onchange="loadFile2()" /></span>
                                                </label>
                                         </div>
                                  </div>
                                  <div class="col-sm-9">
                                  <div class="component">
                                         <div class="panel panel-default">
                                                <div class="panel-heading clearfix">
                                                       <h4 class="form-heading">Personal Details</h4>
                                                </div>
                                                <div class="form-group">
                                                       <div class="required field-item">
                                                              <label class="control-label col-sm-2" for="employee-number">Employee
                                                                     Number:</label>
                                                              

                                                              <div class="col-sm-2">

                                                                     <input type="text" class="form-control subComponent" id="employee-number" name="employeeId" />
                                                                     
                                                              </div>
                                                             <!--  <div class="col-sm-2">
                                                              <jsp:getProperty name="displayBean" property="employeeId" />
                                                                <c:out value="${displayBean.employeeId}"></c:out>
                                                                </div> --> 
                                                       </div>
                                                       <div class="field-item">
                                                             
                                                              <label class="control-label col-sm-2" for="aadhar-id">Aadhar
                                                                     ID:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="aadhar-id" name="aadharId"
                                                                           placeholder="Enter your Aadhar ID">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="required field-item">
                                                              
                                                              <label class="control-label col-sm-2" for="pan-number">PAN
                                                                     number:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="pan-card" name="panNumber"
                                                                           placeholder="Enter your PAN number" pattern=".{10,10}"
                                                                           title="The length must be 10 characters">
                                                                     
                                                              </div>
                                                       </div>
                                                </div>
                                                <div class="form-group">
                                                       <div class="required field-item">
                                                              
                                                              <label class="control-label col-sm-2" for="first-name">First
                                                                     Name:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="first-name" name="firstName"
                                                                           placeholder="Enter first name">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="required field-item">
                                                             
                                                              <label class="control-label col-sm-2" for="middle-name">Middle
                                                                     Name:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="middle-name" name="middleName"
                                                                           placeholder="Enter middle name">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="required field-item">
                                                             
                                                              <label class="control-label col-sm-2" for="last-name">Last
                                                                     Name:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="middle-name" name="lastName"
                                                                           placeholder="Enter last name">

                                                              </div>
                                                       </div>
                                                </div>
                                                <div class="form-group">
                                                <div class="required field-item">
                                                              
                                                              <label class="control-label col-sm-2" for="UAN">UAN</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="UAN" name="uan"
                                                                           placeholder="Enter UAN">
                                                                     
                                                              </div>
                                                       </div>
                                                <div class="required field-item">
                                                              
                                                              <label class="control-label col-sm-2" for="role">Role</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="role" name="role"
                                                                           placeholder="Role">
                                                                     
                                                              </div>
                                                       </div>
                                                
                                                <div class="required field-item">
                                                       
                                                       <div class="col-sm-4"></div>
                                                       <label class="control-label col-sm-2" for="date-of-birth">Date
                                                              of Birth:</label>
                                                              
                                   
                                                       <div class="col-sm-2">

                                                              <input type="date" class="date subComponent" id="dob" name="dob"
                                                                     placeholder="dd/mm/yyyy">
                                                              
                                                       </div>
                                                       <div class="col-sm-4"></div>
                                                </div>
                                                 
                                                
                                                 
                                                </div>
                                                  <div class="form-group">
                                                  <div class="required field-item">
                                                              
                                                              <label class="control-label col-sm-2" for="usermane">UserName</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control subComponent" id="UserName" name="userName"
                                                                           placeholder="User Name">
                                                                     
                                                              </div>
                                                       </div>
                                                       
                                                 <div class="field-item required">
                                                 <label class="control-label col-sm-2" for="gender">Gender:</label>
                                                       <div class="col-sm-2">
                                                              <label class="radio-inline subComponent"><input type="radio"
                                                                     id="female" name="gender" value="female">Female</label> 
															  <label class="radio-inline"><input type="radio" id="male"
                                                                     name="gender" value="male">Male</label>
                                                       </div>
                                                 </div>
                                                  
                                                  </div>
                                         </div>
                                         </div>
                                         
                                              <div class="Label">
                                         <div class="panel panel-default">
                                                <div class="panel-heading clearfix">
                                                       <h4 class="form-heading">Personal Details</h4>
                                                </div>
                                               
                                                <div class="form-group">
                                                      
                                                            <label class="control-label col-sm-2" for="employee-number">Employee Number: </label> 
                                                            <div class="col-sm-2 subLabel">
                                                           
                                                            </div>
                                                              

                                                   
                                                             
                                                            <label class="control-label col-sm-2" for="aadhar-id">Aadhar ID:</label>
                                                            <div class="col-sm-2 subLabel">
                                                           
                                                             </div> 
                                                      
                                                     
                                                              
                                                              <label class="control-label col-sm-2" for="pan-number">PAN number:</label>
                                                              <div class="col-sm-2 subLabel">
                                                             
                                                              </div>
                                                 </div>
                                                 
                                                <div class="form-group">
                                                      
                                                              
                                                              <label class="control-label col-sm-2" for="first-name">First Name:</label>
                                                              <div class="col-sm-2 subLabel">
                                                              
                                                              </div> 
                                                    
                                                       
                                                             
                                                              <label class="control-label col-sm-2" for="middle-name">Middle
                                                                     Name:</label>
                                                              <div class="col-sm-2 subLabel">
                                                              
                                                              </div> 
                                                              
                                                   
                                                       
                                                             
                                                              <label class="control-label col-sm-2" for="last-name">Last
                                                                     Name:</label>
                                                               <div class="col-sm-2 subLabel">
                                                              
                                                               </div> 
                                                     
                                                </div>
                                                
                                                <div class="form-group">
                                               
                                                              
                                                              <label class="control-label col-sm-2" for="UAN">UAN:</label>
                                                               <div class="col-sm-2 subLabel">
                                                                
                                                                </div> 
                                                      
                                                       
                                                
                                                      
                                                       <label class="control-label col-sm-2" for="date-of-birth">Date
                                                              of Birth:</label>
                                                              <div class="col-sm-2 subLabel">
                                                               
                                                                </div> 
                                               
                                               
                                                       
                                                       <label class="control-label col-sm-2" for="gender">Gender:</label>
                                                        <div class="col-sm-2 subLabel">
                                                               
                                                                </div> 
                                                              
                                                
                                                </div>
                                                
                                                  <div class="form-group">
                                                 
                                                              
                                                              <label class="control-label col-sm-2" for="usermane">UserName:</label>
                                                              <div class="col-sm-2 subLabel">
                                                                 
                                                                     
                                                              </div>
                                                 
                                                  
                                                  </div>
                                         </div>
                                         </div>
                                         
                                         <div class="panel panel-default division">
                                                <div class="panel-heading clearfix">
                                                       <h4 class="form-heading">Vehicle Details</h4>
                                                </div>
                                                <div class="form-group">
                                                       <div class="field-item">
                                                           
                                                              <label class="control-label col-sm-4"
                                                                     for="four-wheeler">Four Wheeler</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control"
                                                                           id="four-wheeler" name="four-wheeler" placeholder="Enter Vehicle Number">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="field-item">

                                                           
                                                              <label class="control-label col-sm-2"
                                                                     for="two-wheeler">Two Wheeler</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control"
                                                                           id="two-wheeler" name="two-wheeler" placeholder="Enter Vehicle Number">
                                                                     
                                                              </div>
                                                       </div>
                                                </div>
                                         </div>
                                         <div class="panel panel-default division">
                                                <div class="panel-heading clearfix">
                                                       <h4 class="form-heading">Contact Details</h4>
                                                </div>

                                                <div class="form-group">
                                                       <div class="required field-item">
                                                           
                                                              <label class="control-label col-sm-2" for="phone-number-1">Phone
                                                                     Number 1:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control" id="phone-number-1" name="phoneNumber1"
                                                                           placeholder="Enter phone number" pattern="\d*"
                                                                           title="Only numbers allowed">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="field-item">
                                                           
                                                              <label class="control-label col-sm-2" for="phone-number-2">Phone
                                                                     Number 2:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control" id="phone-number-2" name="phoneNumber2"
                                                                           placeholder="Enter phone number" pattern="\d*"
                                                                           title="Only numbers allowed">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="required field-item">
                                                            
                                                              <label class="control-label col-sm-2" for="email">Personal
                                                                     Email:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="email" class="form-control" id="email" name="emailId"
                                                                           placeholder="Enter email">
                                                                     
                                                              </div>
                                                       </div>
                                                </div>



                                                <div class="col-sm-4"></div>
                                                <div class="form-group">
                                                       <div class="field-item">
                                                            
                                                              <label class="control-label col-sm-2" for="messenger-id"><select
                                                                     class="form-control" name="messenger">
                                                                           <option selected>Select Messenger</option>
                                                                           <option>Skype</option>
                                                                           <option>Yahoo Messenger</option>
                                                                           <option>Google Messenger</option>
                                                              </select></label>

                                                              <div class="col-sm-2 dropdown-text">
                                                                     <input type="text" class="form-control" id="messenger" name="messengerId"
                                                                           placeholder="Enter messenger ID">
                                                                     
                                                              </div>
                                                       </div>
                                                </div>
                                                <div class="col-sm-4"></div>
                                         </div>

                                         <div class="col-sm-12 panel panel-default address division ">
                                                <div class="col-sm-6 division">
                                                       <div class="form-group">
                                                              <div class="panel-heading clearfix" id="quickfix-address-heading">
                                                                     <h4 class="form-heading">Permanent Address</h4>
                                                              </div>
                                                       </div>


                                                       <div class="form-group required field-item">
                                                           

                                                              <label class="control-label col-sm-3"
                                                                     for="permanent-address-line-1">Line 1</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="permanent-address-line-1" name="permanentAddressLine1"
                                                                           placeholder="Enter door number, street number">
                                                                     
                                                              </div>

                                                       </div>
                                                       <div class="form-group required field-item">
                                                            

                                                              <label class="control-label col-sm-3"
                                                                     for="permanent-address-line-2">Line 2</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="permanent-address-line-2" name="permanentAddressLine2"
                                                                           placeholder="Enter area or locality">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="form-group required field-item">
                                                            

                                                              <label class="control-label col-sm-3"
                                                                     for="permanent-address-line-3">City</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="permanent-address-city" name="permanentAddressCity" placeholder="Enter city">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="form-group required field-item">
                                                            

                                                              <label class="control-label col-sm-3"
                                                                     for="permanent-address-state">State</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="permanent-address-state" name="permanentAddressState" placeholder="Enter state">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="form-group required field-item">
                                                            
                                                              <label class="control-label col-sm-3"
                                                                     for="permanent-address-country">Country</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="permanent-address-country" name="permanentAddressCountry" placeholder="Enter country">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="form-group required field-item">
                                                              
                                                              <label class="control-label col-sm-3"
                                                                     for="permanent-address-pin">Pin Code</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="permanent-address-pin" name="permanentAddresspincode" placeholder="Enter Pin Code">
                                                                     
                                                              </div>
                                                       </div>
                                                </div>
                                                <div class="col-sm-6 division">
                                                       <div class="form-group">
                                                              <div class="panel-heading clearfix">
                                                                     <h4 class="form-heading">Current Address</h4>
                                                              </div>
                                                       </div>
                                                       
                                                       <div class="form-group required field-item">
                                                             
                                                              <label class="control-label col-sm-4"
                                                                     for="current-address-line-1">Line 1</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="current-address-line-1" name="currentAddressLine1"
                                                                           placeholder="Enter door number, street number">
                                                                     
                                                              </div>
                                                       </div>

                                                       <div class="form-group required field-item">
                                                             
                                                              <label class="control-label col-sm-4"
                                                                     for="current-address-line-2">Line 2</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="current-address-line-2" name="currenttAddressLine2"
                                                                           placeholder="Enter area or locality">
                                                                     
                                                              </div>

                                                       </div>
                                                       <div class="form-group required field-item">
                                                           
                                                              <label class="control-label col-sm-4"
                                                                     for="current-address-line-3">City</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="current-address-city" name="currentAddressCity" placeholder="Enter city">
                                                                     
                                                              </div>

                                                       </div>
                                                       <div class="form-group required field-item">
                                                           
                                                              <label class="control-label col-sm-4"
                                                                     for="current-address-state">State</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="current-address-state" name="currentAddressState"  placeholder="Enter state">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="form-group required field-item">
                                                             
                                                              <label class="control-label col-sm-4"
                                                                     for="current-address-country">Country</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="current-address-country" name="currentAddressCountry"  placeholder="Enter country">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="form-group required field-item">
                                                              
                                                              <label class="control-label col-sm-4" for="currentAddresspincode">Pin
                                                                     Code</label>
                                                              <div class="col-sm-6">
                                                                     <input type="text" class="form-control"
                                                                           id="current-address-pin" name="currentAddresspincode"  placeholder="Enter Pin Code">
                                                                     
                                                              </div>
                                                       </div>
                                                </div>
                                                <div class="col-sm-5"></div>
                                                <div class="col-sm-7 division">
                                                <input type="checkbox" id="copy-address"> Current Address Same as Permanent Address
                                                </div>
                                         </div>

                                        <div class="division">
                                         <div class="panel panel-default col-sm-12">
                                                <div class="panel-heading clearfix quickfix1">
                                                       <h4 class="form-heading">Emergency Contact</h4>
                                                </div>
                                                <div class="form-group required">
                                                       <div class="field-item">
                                                             
                                                              <label class="control-label col-sm-2"
                                                                     for="emergency-contact-person">Name:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control"
                                                                           id="emergency-contact-person" name="emergencyPersonName"  placeholder="Enter Person Name">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="field-item">

                                                             
                                                              <label class="control-label col-sm-2"
                                                                    for="emergency-contact-relation">Relationship:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control"
                                                                           id="emergency-relation" name="emergencyPersonRelationship"  placeholder="Enter relation">
                                                                     
                                                              </div>
                                                       </div>
                                                       <div class="field-item">
                                                          
                                                              <label class="control-label col-sm-2"
                                                                     for="emergency-contact-number">Phone Number:</label>
                                                              <div class="col-sm-2">
                                                                     <input type="text" class="form-control"
                                                                           id="energency-contact-number" name="emergencyPersonPhoneNumber"
                                                                           placeholder="Enter Phone Number">
                                                                     
                                                             </div>
                                                       </div>
                                                </div>
                                         </div>
                                  </div>
                                         <div class="col-sm-1"></div>
                                         <div class="col-sm-2"></div>
                                         <div class="col-sm-10"></div>
                                         <div class="col-sm-2"></div>
                                         
                                  </div>
                                  <div class="col-sm-1 submit-button field-item">
                                  		 <div class="glyphicon glyphicon-comment comment-box-button"></div>
                                  		 <div class="message-box">
                                                                     <div class="close-comment">
                                                                           <span class="popupclose glyphicon glyphicon-remove"></span>
                                                                     </div>
                                                                     <div class="comment-content">
                                                                    
                                                                     
                                                                           <p>Please enter any additional comments</p>
                                                                           <textarea class="comment-textbox" rows="5" cols="30"></textarea>
                                                                     </div>
                                                              </div>
                                         <button id="submit-button" class="submit" type="submit">Submit</button>
                                  </div>
                           </form>
              </div>
       </div>
       <div id="footer"></div>
</body>
</html>
