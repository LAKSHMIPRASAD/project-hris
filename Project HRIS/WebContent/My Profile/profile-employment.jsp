<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Profile | Employment</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/profile.css" rel="stylesheet">
<link href="../css/header-footer.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="../js/HR perspective.js"></script>
<script type="text/javascript" src="../js/popups.js"></script>
<script> 
$(function(){
  $("#header").load("../header/header.jsp"); 
  $("#footer").load("../footer/footer.html"); 
});
</script>
</head>
<body>
	<div id="header"></div>
	<div id="overlay"></div>
	<div class="container-floating">
		<div class="form-entire">
			<form class="form-horizontal" role="form" method="post" action="../EmploymentController">
				<div class="col-sm-2"></div>
				<div class="col-sm-8 panel panel-default">
					<div class="form-group ">
						<div class="panel-heading clearfix quickfix3">
							<h4 class="form-heading">Current Employment Details</h4>
						</div>
					</div>
					<div class="col-sm-6">

						<div class="form-group required field-item">

							<label class="control-label col-sm-4" for="designation">Designation</label>
							<div class="col-sm-6">

								<input type="text" name="designation" class="form-control" id="designation">

							</div>
						</div>
						
						

							

								<input type="text" name="companyName" value="Mcfadyen Solutions" hidden>

							
						
						<div class="form-group required field-item">

							<label class="control-label col-sm-4" for="joining-date">Date
								of Joining:</label>
							<div class="col-sm-4">

								<input type="date" name="dateOfJoining" class="form-control" id="joining-date"
									>

							</div>
						</div>
						<div class="form-group required field-item">

							<label class="control-label col-sm-4" for="duration">Duration:</label>
							<div class="col-sm-6"></div>
						</div>
						<div class="form-group required field-item">

							<label class="control-label col-sm-4" for="department-name">Department
								Name</label>
							<div class="col-sm-6">

								<input type="text" name="departmentName" class="form-control" id="departnemt-name">

							</div>
						</div>
						<div class="form-group required field-item">

							<label class="control-label col-sm-4" for="manager-name">Manager
								Name</label>
							<div class="col-sm-6">
								<input type="text" name="managerName" class="form-control" id="manager-name">

							</div>
						</div>


					</div>
					<div class="col-sm-6">
						<div class="form-group required field-item">

							<label class="control-label col-sm-4" for="company-location">Company
								Location</label>
							<div class="col-sm-6">

								<select id="select-location" >
									<option selected>Select Location</option>
									<option value="a">Vienna-Virginia</option>
									<option value="b">Dallas-Texas</option>
									<option value="c">TRV</option>
									<option value="d">BLR-Indiranagar</option>
									<option value="e">BLR-Ulsoor</option>
									<option value="f">BLR-Palace Road</option>
									<option value="g">Kochi</option>
									<option value="h">Brazil</option>
								</select>

							</div>
						</div>

						<div class="form-group required">
							<label class="control-label col-sm-4" for="address-line-1">Address
								line 1</label>
							<div class="col-sm-6">
								<input type="text" class="form-control"
									name="addressLineOne"  readonly>
							</div>
						</div>
						<div class="form-group required">
							<label class="control-label col-sm-4" for="address-line-2">Address line 2</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="addressLineTwo"  readonly>
							</div>
						</div>
						<div class="form-group required">
							<label class="control-label col-sm-4" for="city">City</label>
							<div class="col-sm-6">
								<input type="text" class="form-control"
									name="city">
							</div>
						</div>
						<div class="form-group required">
							<label class="control-label col-sm-4" for="state">State</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="state" name="state"
									 readonly>
							</div>
						</div>
						<div class="form-group required">
							<label class="control-label col-sm-4" for="country">Country</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="country"
									name="country"  readonly>
							</div>
						</div>
						<div class="form-group required">
							<label class="control-label col-sm-4" for="pin">Pin Code</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="pin" name="pinCode"
									 readonly>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-2 field-item">
					<div class="glyphicon glyphicon-comment comment-box-button"></div>
					<div class="message-box">
						<div class="close-comment">
							<span class="popupclose glyphicon glyphicon-remove"></span>
						</div>
						<div class="comment-content">


							<p>Please enter any additional comments</p>
							<textarea class="comment-textbox" name="comments" rows="5" cols="30"></textarea>
						</div>
					</div>
				</div>
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<div class="input_fields_wrap"></div>

				</div>
				<div class="col-sm-2"></div>

				<div class="col-sm-2"></div>
				<div class="col-sm-7">
					<div class="add-button">
						<button type="button" class="add_field_button previous-employer">Add
							Previous Employer</button>
					</div>
				</div>


				<div class="col-sm-3">
					<div class="submit-button">
						<button class="submit submit-space" type="submit">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="footer"></div>
</body>

</html>