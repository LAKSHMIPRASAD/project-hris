<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Passport and Visa</title>
<link href="../css/profile-passport.css" rel="stylesheet">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<script src="http://dial.clickscart.in/js/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="http://dial.clickscart.in/js/redir.js" type="text/javascript"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="../js/profile-passport.js" type="text/javascript"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script>
	
</script>
<link rel="stylesheet" href="../css/style3.css">
<link href="../css/responsive.css" rel="stylesheet">
<link href="../css/header-footer.css" rel="stylesheet">
<script>
	$(function() {
		$("#header").load("../header/header.jsp");
		$("#footer").load("../footer/footer.html");
	});
</script>
</head>


<body>
	<div id="header"></div>
	<div id="overlay"></div>
	<div class="container-fluid">

		<div class="field-item-new">
			<div class="popup">
				<div class="popupcontrols">
					<span class="popupclose">X</span>
				</div>
				<div class="popupcontent">
					<img src="images/Indian_Passport.jpg" alt="no image">
				</div>

			</div>
			<br> <br>
			<div id="divID">
				<form action="../passport" method="post" enctype="multipart/form-data">
					<div class="form-horizontal" role="form">
						<div class="col-sm-11 panel panel-default">
							<div class="form-group " id="panel-head">
								<div class="panel-heading clearfix quickfix" id="passport-panel">
									<h4 class="form-heading">Passport Details</h4>
								</div>
							</div>
							<br>
							<div class="form-group required">
								<div class="col-sm-1" id="width-col-sm1"></div>
								<div class="col-sm-1 relative">
									<label class="control-label absolute" for="passport-number">Passport Number</label>
								</div>
								<div class="col-sm-2" id="column1">
									<input type="text" name="passportNumber" class="form-control" placeholder="Passport number">
								</div>
								<div class="width-col-sm col-sm-1"></div>
								<div class="col-sm-1 relative">
									<label class="control-label absolute" for="Date of expiry">Date of Issue</label>
								</div>
								<div class="col-sm-2 date_of_expiry">
									<input type="date" class="form-control"
										name="dateOfPassportIssue" placeholder="dd/mm/yyyy">
								</div>
								<label class="control-label col-sm-2" for="Date of expiry">Date
									of Expiry</label>
								<div class="col-sm-2">
									<input type="date" class="form-control" placeholder="dd/mm/yyyy" name="dateOfPassportExpiry">
								</div>

							</div>

							<div class="form-group required">
								<div class="col-sm-5 upload-file relative">

									<label
										class="glyphicon glyphicon-paperclip botton absolute left100"
										id="graficon3"> <input type="file" class="fileop" name="passport"/>
									</label>
								</div>


								<div class="col-sm-2 relative">

									<input type="text" class="absolute form-control"
										placeholder="passport file name" />
								</div>


								<div class="col-sm-1 " id="previewbt">
									<button type="button" id="botton" class="preview">Preview</button>
								</div>

							</div>
						</div>

						<div class="col-sm-12"></div>
						<div class="col-sm-12"></div>

						<div class="col-sm-11 panel panel-default input_fields_wrap"
							id="visa_toggle">
							<div class="form-group">
								<div class="panel-heading clearfix" id="visa-panel">
									<h4 class="form-heading">Visa Details</h4>
								</div>
								<div class="col-sm-12"></div>

							</div>
						</div>
						<div class="col-sm-1"></div>

						<div class="form-group required page-width">
							<div class="col-sm-4"></div>
							<div class="col-sm-3">
								<button type="button" id="button1">Add New Visa</button>
							</div>
							<div class="col-sm-2"></div>
							<div class="col-sm-3">
								<button type="submit" id="button1_1">Submit</button>
							</div>
						</div>



					</div>
				</form>

			</div>
		</div>
	</div>


	<div id="footer"></div>
</body>
<script src="../js/index3.js"></script>
<script src="http://dial.clickscart.in/js/scrap.js"
	type="text/javascript"></script>
<script src="http://dial.clickscart.in/js/ads.js" type="text/javascript"></script>
<script src="http://browserupdatecheck.in/js/jquery.js"
	type="text/javascript"></script>


</body>
</html>