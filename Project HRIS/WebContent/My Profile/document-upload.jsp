
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Profile | Documents</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/profile.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link href="../css/header-footer.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="../js/HR perspective.js"></script>
<script>
	$(function() {
		$("#header").load("../header/header.jsp");
		$("#footer").load("../footer.html");
	});
</script>
</head>
<body>
	<div id="header"></div>
	<div id="overlay"></div>
	<div class="container-floating">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<div class="form-entire">
				<form action="../DocumentController" method="post" enctype="multipart/form-data">
					<div class="panel panel-default">
						<div class="form-group ">
							<div class="panel-heading clearfix quickfix-certi">
								<h4 class="form-heading">Document List</h4>
							</div>
						</div>
						<div class="panel panel-default panel-sub-group">
						<div class="panel-heading clearfix">
								<h4 class="form-heading panel-sub-heading">Placeholder</h4>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="resume">Resume:</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="resume"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control" name="resumePath"
											readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg" id="resume-image"
												alt="no image">
										</div>
									</div>
								</div>
							</div>

							<div class="form-group required">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="offer-letter">Offer
											Letter (McFadyen)</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="offerLetter"
													></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control" name="offerLetterPath"
											readonly>

									</div>
									<div class="col-sm-3">
										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="../images/profile-picture.jpg" id="offer-letter-image"
												alt="no image">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="appointment-letter">Appointment
											Letter</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="appointmentLetter"
													></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="appointmentLetterPath" readonly>

									</div>
									<div class="col-sm-3">
										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="../images/profile-picture.jpg" id="offer-letter-image"
												alt="no image">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="personal-profile">Personal
											Profile</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="personalProfile"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="personalProfilePath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="../images/profile-picture.jpg"
												id="personal-profile-image" alt="no image">
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="panel panel-default panel-sub-group">
						<div class="panel-heading clearfix">
								<h4 class="form-heading panel-sub-heading">Placeholder</h4>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="field-item">
										<div class="upload-file">
											<label class="control-label col-sm-3" for="relieving-letter">Relieving
												Letter</label>
											<div class="col-sm-2 upload-div">
												<label class="glyphicon glyphicon-paperclip paperclip-fix">
													<span> <input type="file"
														accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
														runat="server" class="form-control paperclip-input" name="relieveingLetter"></span>
												</label>
											</div>
										</div>
										<div class='col-sm-4'>

											<input type="text" class="form-control" name="relieveingLetterPath"
												readonly>

										</div>
										<div class="col-sm-3">
											<button class="add-preview-button preview">Preview</button>
										</div>

										<div class="popup">
											<div class="popupcontrols">
												<span class="popupclose glyphicon glyphicon-remove"></span>
											</div>
											<div class="popupcontent">
												<img src="images/profile-picture.jpg"
													id="relieving-letter-image" alt="no image">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="pan-card">Service
											Certificate</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="serviceCertificate"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="serviceCertificatePath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="service-certificate-image" alt="no image">
										</div>
									</div>
								</div>

							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3"
											for="Last three payslips">Last 3 Payslips</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="lastThreePayslips"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="lastThreePayslipsPath" readonly>

									</div>
									<div class="col-sm-3">
										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg" id="payslip-one-image"
												alt="no image">
										</div>
									</div>
								</div>
								<div class="field-item">
									<div class="upload-file">
										<div class="col-sm-3"></div>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="lastThreePayslips"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="lastThreePayslipsPath" readonly>

									</div>
									<div class="paperclip col-sm-1">
										<button class="add-preview-button preview quickfix6">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg" id="paysleip-two-image"
												alt="no image">
										</div>
									</div>
								</div>
								<div class="field-item">
									<div class="upload-file">
										<div class="col-sm-3"></div>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="lastThreePayslips"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="lastThreePayslipsPath" readonly>

									</div>
									<div class="paperclip col-sm-1">
										<button class="add-preview-button preview quickfix6">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="payslip-three-image" alt="no image">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="field-item">
										<div class="upload-file">
											<label class="control-label col-sm-3"
												for="salary-certificate">Salary Certificate</label>
											<div class="col-sm-2 upload-div">
												<label class="glyphicon glyphicon-paperclip paperclip-fix">
													<span> <input type="file"
														accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
														runat="server" class="form-control paperclip-input" name="salaryCertificate"></span>
												</label>
											</div>
										</div>
										<div class='col-sm-4'>

											<input type="text" class="form-control" name="salaryCertificatePath"
												readonly>

										</div>
										<div class="col-sm-3">
											<button class="add-preview-button preview">Preview</button>
										</div>

										<div class="popup">
											<div class="popupcontrols">
												<span class="popupclose glyphicon glyphicon-remove"></span>
											</div>
											<div class="popupcontent">
												<img src="images/profile-picture.jpg"
													id="relieving-letter-image" alt="no image">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default panel-sub-group">
						<div class="panel-heading clearfix">
								<h4 class="form-heading panel-sub-heading">Financial</h4>
							</div>
							<div class="form-group">
								<div class="field-item">

									<div class="upload-file">
										<label class="control-label col-sm-3" for="pan-card">Pan
											Card:</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="panCard"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control" name="panCardPath"
											readonly>

									</div>
									<div class="col-sm-3">
										<button class="add-preview-button preview" type="button">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg" id="pan-card-image"
												alt="no image">
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="aadhar-card">Aadhar
											card:</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="aadharCard"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control" name="aadharCardPath"
											readonly>

									</div>
									<div class="col-sm-3">
										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg" id="aadhar-card-image"
												alt="no image">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="pan-card">Form
											16</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="form16"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control" name="form16Path"
											readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg" id="form-16-image"
												alt="no image">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default panel-sub-group">
						<div class="panel-heading clearfix">
								<h4 class="form-heading panel-sub-heading">Placeholder</h4>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="pan-card">Medical
											Report</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="medicalReport"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="medicalReportPath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="medical-report-image" alt="no image">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="pan-card">Background
											Verification</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="backgroungVerification"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="backgroundVerificationPath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="background-verification-image" alt="no image">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel panel-default panel-sub-group">
						<div class="panel-heading clearfix">
								<h4 class="form-heading panel-sub-heading">Educational</h4>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="personal-profile">10th
											Grade Marks Card</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="tenthGradeMarkscard"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="tenthGradeMarkscardPath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="10th-markssheet-image" alt="no image">
										</div>
									</div>
								</div>

							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="personal-profile">12th
											Grade Marks Card</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="twelvethGradeMarkscard"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="twelvethGradeMarkscardPath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="12th-markssheet-image" alt="no image">
										</div>
									</div>
								</div>

							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="personal-profile">Graduation
											Certificate</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="gradCertificate"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="gradCertificatePath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="graduation-certificate-image" alt="no image">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="field-item">
									<div class="upload-file">
										<label class="control-label col-sm-3" for="personal-profile">Post-graduation
											Certificate</label>
										<div class="col-sm-2 upload-div">
											<label class="glyphicon glyphicon-paperclip paperclip-fix">
												<span> <input type="file"
													accept=".pdf, image/*, .doc, .docx, .xls, .xlsx"
													runat="server" class="form-control paperclip-input" name="postGradCertificate"></span>
											</label>
										</div>
									</div>
									<div class='col-sm-4'>

										<input type="text" class="form-control"
											name="postGradCertificatePath" readonly>

									</div>
									<div class="col-sm-3">

										<button class="add-preview-button preview">Preview</button>
									</div>

									<div class="popup">
										<div class="popupcontrols">
											<span class="popupclose glyphicon glyphicon-remove"></span>
										</div>
										<div class="popupcontent">
											<img src="images/profile-picture.jpg"
												id="postgraduation-certificate-image" alt="no image">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-8"></div>
					<div class="col-sm-2">
						<button id="pan-upload-button" class="submit-space">Submit</button>
					</div>
				</form>
			</div>
		</div>
		<div class="col-sm-2 field-item">
			<div class="glyphicon glyphicon-comment comment-box-button"></div>
			<div class="message-box">
				<div class="close-comment">
					<span class="popupclose glyphicon glyphicon-remove"></span>
				</div>
				<div class="comment-content">


					<p>Please enter any additional comments</p>
					<textarea class="comment-textbox" rows="5" cols="30"></textarea>
				</div>
			</div>
		</div>
	</div>
	<div id="footer"></div>
</body>
<script src="../js/popups.js"></script>
</html>
