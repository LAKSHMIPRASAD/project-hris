<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="css/holiday-list.css">
<jsp:include page="header.jsp" /><br>
<body>
	<div class="container" >
		<div class="col-xs-12">
			<label class="rad"> <input type="radio" name="location" value="all" onclick="handleClick(this)"/>
				<i></i> All
			</label> 
			<label class="rad"> <input type="radio" name="location" value="Vienna" onclick="handleClick(this)"/>
				<i></i> Vienna
			</label> 
			<label class="rad"> <input type="radio" name="location" value="Bangalore" onclick="handleClick(this)"/>
				<i></i> Bangalore
			</label> 
			<label class="rad"> <input type="radio" name="location" value="Kochi" onclick="handleClick(this)"/>
				<i></i> Kochi
			</label> 
			<label class="rad"> <input type="radio" name="location" value="trivandrum" onclick="handleClick(this)"/>
				<i></i> Trivandrum
			</label>
			<label class="rad"> <input type="radio" name="location" value="brazil" onclick="handleClick(this)"/>
				<i></i> Brazil
			</label> 
					<div class="panel panel-default">
			<div class="panel-heading clearfix">
			</div>
			<table class="table table-hover" id="displayHoliday"style="width: 100%">
				<thead>
				<tr>
					<th>Holiday - id</th>
					<th>Holiday Name</th>
					<th>Date</th>
					<th>Location</th>
				</tr>
				</thead>
				
			</table>
		</div>

		</div>
	</div>
</body>
<script>
function handleClick(locationRadio){
	var currentLocation = locationRadio.value;
	$.ajax({url:"${pageContext.request.contextPath}/holiday",data:{location:currentLocation},success: function(data,status){
		$("#displayHoliday").append("<tbody id='resultBody'>");
		drawTable(data);
		$("#displayHoliday").append("</tbody>");
	}
	});
}
function drawTable(data) {
	$("#resultBody").empty();
    for (var i = 0; i < data.length; i++) {
	        drawRow(data[i]);
    }
}

function drawRow(rowData) {
    var row = $("<tr />")
    $("#displayHoliday").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
    row.append($("<td>" + rowData.holidayId + "</td>"));
    row.append($("<td>" + rowData.holidayName + "</td>"));
    row.append($("<td>" + rowData.holidayDate + "</td>"));
    row.append($("<td>" + rowData.holidayLocation + "</td>"));
}
</script>
<jsp:include page="footer.jsp" />