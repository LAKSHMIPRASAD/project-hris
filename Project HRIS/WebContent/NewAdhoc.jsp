<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator" %>
<%@page import="com.mcfadyen.hris.metrics.Metrics" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">
                <title>Home | HRIS</title>
                
                <!-- core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="css/font-awesome.min.css" rel="stylesheet">
                <link href="css/animate.min.css" rel="stylesheet">
                <link href="css/prettyPhoto.css" rel="stylesheet">
                <link href="css/main.css" rel="stylesheet">
                <link href="css/responsive.css" rel="stylesheet">
                <link href="css/simple-sidebar.css" rel="stylesheet">
                <link href="css/newadhoccss.css" rel="stylesheet">
                <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
                <script src="js/respond.min.js"></script>
                <![endif]-->       
                <link rel="shortcut icon" href="images/ico/favicon.ico">
                <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
                <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
                <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
                <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
                <link href="jquery-ui.css" rel="stylesheet">


                <script src="external/jquery/jquery.js"></script>
                 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>  

                
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jquery.prettyPhoto.js"></script>
                <script src="js/jquery.isotope.min.js"></script>
                <script src="js/main.js"></script>
                <script src="js/jspdf.min.js"></script>
                
                <script src="js/standard_fonts_metrics.js"></script>


                <script src="js/Chart.js"></script>
                <script src="js/wow.min.js"></script>
                
                <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script>
                <script> 
					$(function(){
					  $("#header").load("header.jsp"); 
					  $("#footer").load("footer.html"); 
					});
				</script>
                
    <title>Sidebar</title>

   

    <!-- Custom CSS -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<fmt:setBundle basename="/column_name.properties" var="dbProps"/>
   <div id="header"></div>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <div class="wrapper1">
  <nav class="vertical">
    <ul>
      <li>
        <a href="#">Standard Report</a>
        <div>
          <ul>
             <li><a href="report.html">HR Dashboard</a></li>
            <li><a href="seperation.html">Attrition Report</a></li>
            <li><a href="gender.html">Joiner Gender Ratio</a></li>
            <li><a href="distribution.html">Employee Distribution</a></li>
          
          </ul>
        </div>
      </li>
      <li>
        <a href="#">Custom Report</a>
        <div>
          <ul>
            <li><a href="#"><p id="namesave">MyReport#1</p></a></li>
            <li><a href="#">MyReport#2</a></li>
            <li><a href="#">MyReport#3</a></li>
          </ul>
        </div>
      </li>
      <li>
        <a href="#">Adhoc Report</a>
        <div>
          <ul>
            <li><a href="adhoc.html">Generate New</a></li>
            <li><a href="update_prev.jsp">Update Prev.</a></li>
            
          </ul>
        </div>
      </li>
    </ul>
  </nav>
  
</div>

        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
        
    
     <c:out value='${requestScope["Metrics"]}' ></c:out>
      <c:set var = "columns" value='${requestScope["Metrics"]}' ></c:set>
        <c:out value="${ column.email_id}"></c:out>****
        
        <%
        JSONObject json=new JSONObject();
       json=(JSONObject)request.getAttribute("Metrics");
       Iterator iterator=json.sortedKeys();
       String key=null;
       while(iterator.hasNext()){
        key=(String)iterator.next();
        out.println("<p>"+json.get(key)+"<p>");
        
       }
       
        %>
         
           
    </div>
  <!-- /#page-content-wrapper -->

   <div id="footer"></div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>

  

    </script>



</body>

</html>
