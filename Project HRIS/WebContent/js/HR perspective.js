$(document).ready(function() {
    var max_fields      = 50; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields)
        { //max input box allowed
            x++; //text box increment
            var html="";
            html += "<div class='panel panel-default col-sm-12 quickfix5'>"
            html += "<div class='panel-heading clearfix'>";
            html += '<span class="glyphicon glyphicon-remove removing-cross remove-field"></span>';
            html += "<h4 class='form-heading'>Previous Employment Details</h4>";
            html += "</div>"
            html += "<div class='col-sm-6'>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='company-name'>Company name:</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control'  placeholder='Enter name of previous company' name='companyName' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='company-name'>Designation:</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control' placeholder='Designation previously held' name='designation' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
           
            html += "<label class='control-label col-sm-4' for='joining-date'>Date of joining</label>";
            html += "<div class='col-sm-6'>";            
            html += "<input type='date' class='form-control' name='dateOfJoining' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='duration'>Date of Relieving:</label>";
            html += "<div class='col-sm-6'>";            
            html += "<input type='date' class='form-control' name='dateOfRelieving' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-6'>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='address-line-one'>Address Line 1:</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control' name='addressLineOne' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='address-line-two'>Address Line 2:</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control' name='addressLineTwo' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
           
            html += "<label class='control-label col-sm-4' for='city'>City</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control' name='city' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='state'>State</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control' name='state' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='country'>Country</label>";
            html += "<div class='col-sm-8'>";            
            html += "<input type='text' class='form-control' name='country' required='required'>";
            
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required field-item'>";
            
            html += "<label class='control-label col-sm-4' for='current-address-pin'>Pin Code</label>";
            html += "<div class='col-sm-8'>";                      
            html += "<input type='text' class='form-control' required='required' name='pinCode'>";
            
            html += "</div>";   
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-5'>";
            html += "</div>";
            html += "</div>";
            $(wrapper).append(html); //add input box
        }
    });  
    $(wrapper).on("click",".remove-field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest(".panel").remove(); x--;
    });


    $('#select-location').change(function() {
        var myValue = $(this).val();
        switch (myValue) {
            case 'a':
            $('input[name="addressLineOne"]').val('8230 Old Courthouse Rd');
            $('input[name="addressLineTwo"]').val('Suite 500');
            $('input[name="city"]').val('Vienna');
            $('input[name="state"]').val(' Virginia');
            $('input[name="country"]').val('USA');
            $('input[name="pinCode"]').val('22182');
            break;
            case 'b':
            $('input[name="addressLineOne"]').val('6860');
            $('input[name="addressLineTwo"]').val('North Dallas Parkway Suite 219');
            $('input[name="city"]').val('Plano');
            $('input[name="state"]').val('Texas');
            $('input[name="country"]').val('USA');
            $('input[name="pinCode"]').val(' 75024');
            break;
            case 'c':
            $('input[name="addressLineOne"]').val('12-B Leela InfoPark');
            $('input[name="addressLineTwo"]').val(' Technopark Campus');
            $('input[name="city"]').val('Trivandrum');
            $('input[name="state"]').val('Kerala');
            $('input[name="country"]').val('India');
            $('input[name="pinCode"]').val('695581');
            break;
            case 'd':
            $('input[name="addressLineOne"]').val('BKN Ambaram Estates, #648/L, 1st Stage');
            $('input[name="addressLineTwo"]').val('1st Cross, IndiraNagar');
            $('input[name="city"]').val(' Bangalore');
            $('input[name="state"]').val('Karnataka');
            $('input[name="country"]').val('India');
            $('input[name="pinCode"]').val('560038');
            break;
            case 'e':
            $('input[name="addressLineOne"]').val('Ulsoor');
            $('input[name="addressLineTwo"]').val('this is the third item');
            $('input[name="city"]').val('bangalore');
            $('input[name="state"]').val('Karnataka');
            $('input[name="country"]').val('India');
            $('input[name="pinCode"]').val('560054');
            break;
            case 'f':
            $('input[name="addressLineOne"]').val('Coming Soon');
            $('input[name="addressLineTwo"]').val('Coming Soon');
            $('input[name="city"]').val('Coming Soon');
            $('input[name="state"]').val('Coming Soon');
            $('input[name="country"]').val('Coming Soon');
            $('input[name="pinCode"]').val('Coming Soon');
            break;
            case 'g':
            $('input[name="addressLineOne"]').val('A Wing, 2nd Floor');
            $('input[name="addressLineTwo"]').val('Athulya Bldg, InfoPark, Kakkanad');
            $('input[name="city"]').val('Kochi');
            $('input[name="state"]').val('Kerala');
            $('input[name="country"]').val('India');
            $('input[name="pinCode"]').val('682030');
            break;
            case 'h':
            $('input[name="addressLineOne"]').val('placeholder');
            $('input[name="addressLineTwo"]').val('placeholder');
            $('input[name="city"]').val('placeholder');
            $('input[name="state"]').val('placeholder');
            $('input[name="country"]').val('placeholder');
            $('input[name="pinCode"]').val('placeholder');
            break;
        }
    });


$('.upload-file').on("change",function(){
    var inp_obj = $(this).closest(".field-item"),
    fake_path = inp_obj.find("input[type='file']").val(),
    lastIndex = fake_path.lastIndexOf("\\"),
    path;
    lastIndex = lastIndex + 1 ;
    path = fake_path.substring(lastIndex);
    inp_obj.find('input[type="text"]').val(path);
});




var max_certificates = 100;
    var wrapper_certificate         = $(".new_certificate"); //Fields wrapper
    var add_certificate      = $(".new_certificate_button"); //Add button ID
    $(add_certificate).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_certificates)
        { //max input box allowed
            x++; //text box increment
            var html='';
            html += '<div class="form-group">';
            html += '<div class="field-item">';
            html += '<label class="control-label col-sm-2 dropdown-fix" for="certificate">'; 
            html += '<select name="certificate[]" class="form-control" required="required">';
            html += '<option value="">Certificate</option>';
            html += '<option value="ATG 10 Sales Specialist">ATG 10 Sales Specialist</option>';
            html += '<option value="ATG 10 Presales Specialist">ATG 10 Presales Specialist</option>';
            html += '<option value="ATG Support Specialist">ATG Support Specialist</option>';
            html += '<option value="Java 7 OCA">Java 7 OCA</option>';
            html += '<option value="Java 7 OCP">Java 7 OCP</options>';
            html += '</select>';
            html += '</label>';
            html += '</div>';
            html += '<div class="field-item">';
            
            html += '<div class="certification-date col-sm-2">';
            html += '<label class="control-label col-sm-3" for="certification-date">Date</label>';
            html += '<div class="col-sm-9">';            
            html += '<input type="date" class="form-control" name="certification-date[]" required="required" />';
           
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="field-item">';
            
            html += '<label class="control-label col-sm-1" id="spacefix1" for="bonus">Bonus</label>';
            html += '<div class="col-sm-2">';
            html += '<input type="text" class="form-control" name="bonus[]" required="required" />';
           
            html += '</div>';
            html += '</div>';
            html += '<div class="field-item">';
            
            html += '<div class="certificate-upload-button">';
            html += '<div class="col-sm-1 upload-div">';
            html += '<div class="upload-file quickfix7">';
            html += '<label class="glyphicon glyphicon-paperclip paperclip-fix"><span>';
            html += '<input type="file" accept=".pdf, image/*, .doc, .docx, .xls, .xlsx" runat="server" class="form-control" />';
            html += '</span></label>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="field-item">';
            
            html += '<div class="col-sm-3 spacefix2">';            
            html += '<input type="text" class="form-control" name="test-text" readonly>';
           
            html += '</div>';
            html += '<div class=col-sm-1>';
            html += '<button class="certificate-preview">Preview</button>';
            html += '</div>';
            html += '<div class="popup-certificate">';
            html += '<div class="popupcontrols">';
            html += '<span class="popupclose-cert glyphicon glyphicon-remove"></span>';
            html += '</div>';
            html += '<div class="popupcontent">';
            html += '<img src="images/profile-picture.jpg" id="aadhar-card-image" alt="no image">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<span class="glyphicon glyphicon-minus remove-certificate"></span>';       
            $(".new_certificate").append(html);
        }
    });  
    $(wrapper_certificate).on("click",".remove-certificate", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });

    $(document).on("change",".certificate-upload-button",function(){
        var inp_obj = $(this).closest(".field-item"),
        fake_path = inp_obj.find("input[type='file']").val(),
        lastIndex = fake_path.lastIndexOf("\\"),
        path;
        lastIndex = lastIndex + 1 ;
        path = fake_path.substring(lastIndex);
        inp_obj.find('input[type="text"]').val(path);
    });

    $("td").click(function () {
        var a = $(this).find('input:radio');
        a.prop('checked', true);
   });

    $(document).on("click",".popupclose",function(e){
        console.log(e);
        if ($(this).closest(".message-box").find('.comment-textbox').val().trim().length > 0)
        {
             $(this).closest(".field-item").find(".comment-box-button").css("color", "green");
        }   
        else
        {
            $(this).closest(".field-item").find(".comment-box-button").css("color", "black");
        }
    });

      $(document).on("click",".comment-box-button",function(){
    var overlay = $("#overlay");  
    $(document).on("click",".popupclose",function(){  
      var popup = $(this).closest(".field-item").find(".message-box");
      console.log(popup);
      overlay.css("display","none");
      popup.css("display","none");
    });
    var popup = $(this).closest(".field-item").find(".message-box"); 
    popup.css("display","block");
    overlay.css("display","block");
  });
      
      $(".address").on("click",function(){
    	  if($("#copy-address").is(':checked')){
    	  var line1 = $("#permanent-address-line-1");
    	  var line2 = $("#permanent-address-line-2");
    	  var city = $("#permanent-address-city");
    	  var state = $("#permanent-address-state");
    	  var country = $("#permanent-address-country");
    	  var pin = $("#permanent-address-pin");
    	  $("#current-address-line-1").val(line1.val());
    	  $("#current-address-line-2").val(line2.val());
    	  $("#current-address-city").val(city.val());
    	  $("#current-address-state").val(state.val());
    	  $("#current-address-country").val(country.val());
    	  $("#current-address-pin").val(pin.val());
    	  }
      });
});

function loadFile() {
    var output = document.getElementById('uploadPreview1');
    output.src = URL.createObjectURL(event.target.files[0]);
};
function loadFile2(){
    var output2 = document.getElementById('uploadPreview2');
    output2.src = URL.createObjectURL(event.target.files[0]);
};