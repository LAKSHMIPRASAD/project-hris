$(document).ready(function() {
    var max_fields      = 50; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields)
        { //max input box allowed
            x++; //text box increment
            var html="";
            html += "<div class='panel panel-default col-sm-12 quickfix5'>"
            html += "<div class='panel-heading clearfix'>";
            html += '<span class="glyphicon glyphicon-remove removing-cross remove-field"></span>';
            html += "<h4 class='form-heading'>Previous Employment Details</h4>";
            html += "</div>"
            html += "<div class='col-sm-6'>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='company-name'>Company name:</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='company-name' placeholder='Enter name of previous company' name='company-name[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='company-name'>Designation:</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='designation' placeholder='Designation previously held' name='designation[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='joining-date'>Date of joining</label>";
            html += "<div class='col-sm-6'>";
            html += "<input type='date' class='form-control' id='joining-date' name='joining-date[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='duration'>Date of Relieving:</label>";
            html += "<div class='col-sm-6'>";
            html += "<input type='date' class='form-control' id='duration' name='duration[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-6'>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='address-line-one'>Address Line 1:</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='address-line-one' name='company-address-line-one[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='address-line-two'>Address Line 2:</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='address-line-two' name='company-address-line-two[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='city'>City</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='address-line-one' name='city[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='state'>State</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='address-line-one' name='state[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='country'>Country</label>";
            html += "<div class='col-sm-8'>";
            html += "<input type='text' class='form-control' id='country' name='country[]' required='required'>";
            html += "</div>";
            html += "</div>";
            html += "<div class='form-group required'>";
            html += "<label class='control-label col-sm-4' for='current-address-pin'>Pin Code</label>";
            html += "<div class='col-sm-8'>";          
            html += "<input type='text' class='form-control' id='previous-pin-code' required='required' name='pin-code[]'>";
            html += "</div>";   
            html += "</div>";
            html += "</div>";
            html += "<div class='col-sm-5'>";
            html += "</div>";
            html += "</div>";
            $(wrapper).append(html); //add input box
        }
    });  
    $(wrapper).on("click",".remove-field", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest(".panel").remove(); x--;
    });


    $('#select-location').change(function() {
        var myValue = $(this).val();
        switch (myValue) {
            case 'a':
            $('input[name="line-one"]').val('8230 Old Courthouse Rd');
            $('input[name="line-two"]').val('Suite 500');
            $('input[name="city"]').val('Vienna');
            $('input[name="state"]').val(' Virginia');
            $('input[name="country"]').val('USA');
            $('input[name="pin-code"]').val('22182');
            break;
            case 'b':
            $('input[name="line-one"]').val('6860');
            $('input[name="line-two"]').val('North Dallas Parkway Suite 219');
            $('input[name="city"]').val('Plano');
            $('input[name="state"]').val('Texas');
            $('input[name="country"]').val('USA');
            $('input[name="pin-code"]').val(' 75024');
            break;
            case 'c':
            $('input[name="line-one"]').val('12-B Leela InfoPark');
            $('input[name="line-two"]').val(' Technopark Campus');
            $('input[name="city"]').val('Trivandrum');
            $('input[name="state"]').val('Kerala');
            $('input[name="country"]').val('India');
            $('input[name="pin-code"]').val('695581');
            break;
            case 'd':
            $('input[name="line-one"]').val('BKN Ambaram Estates, #648/L, 1st Stage');
            $('input[name="line-two"]').val('1st Cross, IndiraNagar');
            $('input[name="city"]').val(' Bangalore');
            $('input[name="state"]').val('Karnataka');
            $('input[name="country"]').val('India');
            $('input[name="pin-code"]').val('560038');
            break;
            case 'e':
            $('input[name="line-one"]').val('Ulsoor');
            $('input[name="line-two"]').val('this is the third item');
            $('input[name="city"]').val('bangalore');
            $('input[name="state"]').val('Karnataka');
            $('input[name="country"]').val('India');
            $('input[name="pin-code"]').val('560054');
            break;
            case 'f':
            $('input[name="line-one"]').val('Coming Soon');
            $('input[name="line-two"]').val('Coming Soon');
            $('input[name="city"]').val('Coming Soon');
            $('input[name="state"]').val('Coming Soon');
            $('input[name="country"]').val('Coming Soon');
            $('input[name="pin-code"]').val('Coming Soon');
            break;
            case 'g':
            $('input[name="line-one"]').val('A Wing, 2nd Floor');
            $('input[name="line-two"]').val('Athulya Bldg, InfoPark, Kakkanad');
            $('input[name="city"]').val('Kochi');
            $('input[name="state"]').val('Kerala');
            $('input[name="country"]').val('India');
            $('input[name="pin-code"]').val('682030');
            break;
            case 'h':
            $('input[name="line-one"]').val('placeholder');
            $('input[name="line-two"]').val('placeholder');
            $('input[name="city"]').val('placeholder');
            $('input[name="state"]').val('placeholder');
            $('input[name="country"]').val('placeholder');
            $('input[name="pin-code"]').val('placeholder');
            break;
        }
    });


$('.upload-file').on("change",function(){
    var inp_obj = $(this).closest(".field-item"),
    fake_path = inp_obj.find("input[type='file']").val(),
    lastIndex = fake_path.lastIndexOf("\\"),
    path;
    lastIndex = lastIndex + 1 ;
    path = fake_path.substring(lastIndex);
    inp_obj.find('input[type="text"]').val(path);
});




var max_certificates = 100;
    var wrapper_certificate         = $(".new_certificate"); //Fields wrapper
    var add_certificate      = $(".new_certificate_button"); //Add button ID
    $(add_certificate).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_certificates)
        { //max input box allowed
            x++; //text box increment
            var html='';
            html += '<div class="form-group">';
            html += '<label class="control-label col-sm-2 dropdown-fix" for="certificate">'; 
            html += '<select name="certificate[]" class="form-control" required="required">';
            html += '<option value="">Certificate</option>';
            html += '<option value="ATG 10 Sales Specialist">ATG 10 Sales Specialist</option>';
            html += '<option value="ATG 10 Presales Specialist">ATG 10 Presales Specialist</option>';
            html += '<option value="ATG Support Specialist">ATG Support Specialist</option>';
            html += '<option value="Java 7 OCA">Java 7 OCA</option>';
            html += '<option value="Java 7 OCP">Java 7 OCP</options>';
            html += '</select>';
            html += '</label>';
            html += '<div class="certification-date col-sm-2">';
            html += '<label class="control-label col-sm-3" for="certification-date">Date</label>';
            html += '<div class="col-sm-9">';
            html += '<input type="date" class="form-control" name="certification-date[]" required="required" />';
            html += '</div>';
            html += '</div>';
            html += '<label class="control-label col-sm-1" id="spacefix1" for="bonus">Bonus</label>';
            html += '<div class="col-sm-2">';
            html += '<input type="text" class="form-control" name="bonus[]" required="required" />';
            html += '</div>';
            html += '<div class="field-item">';
            html += '<div class="certificate-upload-button">';
            html += '<div class="col-sm-1 upload-div">';
            html += '<div class="upload-file quickfix7">';
            html += '<label class="glyphicon glyphicon-paperclip paperclip-fix"><span>';
            html += '<input type="file" class="form-control" />';
            html += '</span></label>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-sm-3 spacefix2">';
            html += '<input type="text" class="form-control" name="test-text" readonly>';
            html += '</div>';
            html += '<div class=col-sm-1>';
            html += '<button class="certificate-preview">Preview</button>';
            html += '</div>';
            html += '<div class="popup-certificate">';
            html += '<div class="popupcontrols">';
            html += '<span class="popupclose glyphicon glyphicon-remove"></span>';
            html += '</div>';
            html += '<div class="popupcontent">';
            html += '<img src="images/profile-picture.jpg" id="aadhar-card-image" alt="no image">';
            html += '</div>';
            html += '</div>';
            html += '<span class="glyphicon glyphicon-minus remove-certificate"></span>';       
            $(".new_certificate").append(html);
        }
    });  
    $(wrapper_certificate).on("click",".remove-certificate", function(e){ //user click on remove text
        e.preventDefault(); $(this).closest('.form-group').remove(); x--;
    });

    $(document).on("change",".certificate-upload-button",function(){
        var inp_obj = $(this).closest(".field-item"),
        fake_path = inp_obj.find("input[type='file']").val(),
        lastIndex = fake_path.lastIndexOf("\\"),
        path;
        lastIndex = lastIndex + 1 ;
        path = fake_path.substring(lastIndex);
        inp_obj.find('input[type="text"]').val(path);
    });

    $("td").click(function () {
        var a = $(this).find('input:radio');
        a.prop('checked', true);
   });
});




function loadFile() {
    var output = document.getElementById('uploadPreview1');
    output.src = URL.createObjectURL(event.target.files[0]);
};
function loadFile2(){
    var output2 = document.getElementById('uploadPreview2');
    output2.src = URL.createObjectURL(event.target.files[0]);
};