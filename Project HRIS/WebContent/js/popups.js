$(document).ready(function() {
	$(".preview").click(function(e){
		var overlay = $("#overlay");

		$(".popupclose").click(function(){ 
			
			var popup = $(this).closest(".field-item").find(".popup");
			overlay.css("display","none")
			popup.css("display","none")
		});
		var popup = $(this).closest(".field-item").find(".popup"); 
		popup.css("display","block");
		overlay.css("display","block");
	});
	
	$(document).on("click",".certificate-preview",function(){
		var overlay = $("#overlay");	
		$(document).on("click",".popupclose-cert",function(){ 	
			var popup = $(this).closest(".field-item").find(".popup-certificate");
			console.log(popup);
			overlay.css("display","none");
			popup.css("display","none");
		});
		var popup = $(this).closest(".field-item").find(".popup-certificate"); 
		popup.css("display","block");
		overlay.css("display","block");
	});

  $(document).on("click",".comment-box-button",function(){
    var overlay = $("#overlay");  
    $(document).on("click",".popupclose",function(){  
      var popup = $(this).closest(".field-item").find(".message-box");
      console.log(popup);
      overlay.css("display","none");
      popup.css("display","none");
    });
    var popup = $(this).closest(".field-item").find(".message-box"); 
    popup.css("display","block");
    overlay.css("display","block");
  });
});