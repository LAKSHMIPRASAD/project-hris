$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;
  	footer=$('#footer');
    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false; 
        footer.css("display", "block");
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
        footer.css("display", "none");
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
  
 $('#location-monitor').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="monitor"]').val('McfBlrMon');
         break;
    case 'Trivandram':
        $('input[name="monitor"]').val('McfTriMon');
        break;
    case 'Kochi':
        $('input[name="monitor"]').val('McfKocMon');
        break;
    case 'Vienna':
        $('input[name="monitor"]').val('McfVieMon');
        break;
    case 'Virginia':
        $('input[name="monitor"]').val('McfVirMon');
        break;
    }
}); 
 $('#location-laptop').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="laptop"]').val('McfBlrLap');
         break;
    case 'Trivandram':
        $('input[name="laptop"]').val('McfTriLap');
        break;
    case 'Kochi':
        $('input[name="laptop"]').val('McfKocLap');
        break;
    case 'Vienna':
        $('input[name="laptop"]').val('McfVieLap');
        break;
    case 'Virginia':
        $('input[name="laptop"]').val('McfVirLap');
        break;
    }
});

 $('#location-server').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="server"]').val('McfBlrSer');
         break;
    case 'Trivandram':
        $('input[name="server"]').val('McfTriSer');
        break;
    case 'Kochi':
        $('input[name="server"]').val('McfKocSer');
        break;
    case 'Vienna':
        $('input[name="server"]').val('McfVieSer');
        break;
    case 'Virginia':
        $('input[name="server"]').val('McfVirSer');
        break;
    }
}); 
$('#location-desktop').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="desktop"]').val('McfBlrDes');
         break;
    case 'Trivandram':
        $('input[name="desktop"]').val('McfTriDes');
        break;
    case 'Kochi':
        $('input[name="desktop"]').val('McfKocDes');
        break;
    case 'Vienna':
        $('input[name="desktop"]').val('McfVieDes');
        break;
    case 'Virginia':
        $('input[name="desktop"]').val('McfVirDes');
        break;
    }
}); 
$('#location-storages').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="storages"]').val('McfBlrSto');
         break;
    case 'Trivandram':
        $('input[name="storages"]').val('McfTriSto');
        break;
    case 'Kochi':
        $('input[name="storages"]').val('McfKocSto');
        break;
    case 'Vienna':
        $('input[name="storages"]').val('McfVieSto');
        break;
    case 'Virginia':
        $('input[name="storages"]').val('McfVirSto');
        break;
    }
}); 
$('#location-printers').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="printers"]').val('McfBlrPri');
         break;
    case 'Trivandram':
        $('input[name="printers"]').val('McfTriPri');
        break;
    case 'Kochi':
        $('input[name="printers"]').val('McfKocPri');
        break;
    case 'Vienna':
        $('input[name="printers"]').val('McfViePri');
        break;
    case 'Virginia':
        $('input[name="printers"]').val('McfVirPri');
        break;
    }
}); 
$('#location-switches').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="switches"]').val('McfBlrSwi');
         break;
    case 'Trivandram':
        $('input[name="switches"]').val('McfTriSwi');
        break;
    case 'Kochi':
        $('input[name="switches"]').val('McfKocSwi');
        break;
    case 'Vienna':
        $('input[name="switches"]').val('McfVieSwi');
        break;
    case 'Virginia':
        $('input[name="switches"]').val('McfVirSwi');
        break;
    }
}); 
$('#location-routers').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="routers"]').val('McfBlrRou');
         break;
    case 'Trivandram':
        $('input[name="routers"]').val('McfTriRou');
        break;
    case 'Kochi':
        $('input[name="routers"]').val('McfKocRou');
        break;
    case 'Vienna':
        $('input[name="routers"]').val('McfVieRou');
        break;
    case 'Virginia':
        $('input[name="routers"]').val('McfVirRou');
        break;
    }
}); 
$('#location-firewalls').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="firewalls"]').val('McfBlrFir');
         break;
    case 'Trivandram':
        $('input[name="firewalls"]').val('McfTriFir');
        break;
    case 'Kochi':
        $('input[name="firewalls"]').val('McfKocFir');
        break;
    case 'Vienna':
        $('input[name="firewalls"]').val('McfVieFir');
        break;
    case 'Virginia':
        $('input[name="firewalls"]').val('McfVirFir');
        break;
    }
}); 
$('#location-WiFi').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="Wi-Fi"]').val('McfBlrAcp');
         break;
    case 'Trivandram':
        $('input[name="Wi-Fi"]').val('McfTriAcp');
        break;
    case 'Kochi':
        $('input[name="Wi-Fi"]').val('McfKocAcp');
        break;
    case 'Vienna':
        $('input[name="Wi-Fi"]').val('McfVieAcp');
        break;
    case 'Virginia':
        $('input[name="Wi-Fi"]').val('McfVirAcp');
        break;
    }
}); 
$('#location-VC').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="VC"]').val('McfBlrVcu');
         break;
    case 'Trivandram':
        $('input[name="VC"]').val('McfTriVcu');
        break;
    case 'Kochi':
        $('input[name="VC"]').val('McfKocVcu');
        break;
    case 'Vienna':
        $('input[name="VC"]').val('McfVieVcu');
        break;
    case 'Virginia':
        $('input[name="VC"]').val('McfVirVcu');
        break;
    }
}); 
$('#location-von').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="von"]').val('McfBlrVon');
         break;
    case 'Trivandram':
        $('input[name="von"]').val('McfTriVon');
        break;
    case 'Kochi':
        $('input[name="von"]').val('McfKocVon');
        break;
    case 'Vienna':
        $('input[name="von"]').val('McfVieVon');
        break;
    case 'Virginia':
        $('input[name="von"]').val('McfVirVon');
        break;
    }
}); 
$('#location-poly').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="poly"]').val('McfBlrPol');
         break;
    case 'Trivandram':
        $('input[name="poly"]').val('McfTriPol');
        break;
    case 'Kochi':
        $('input[name="poly"]').val('McfKocPol');
        break;
    case 'Vienna':
        $('input[name="poly"]').val('McfViePol');
        break;
    case 'Virginia':
        $('input[name="poly"]').val('McfVirPol');
        break;
    }
}); 
$('#location-OS').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="OS"]').val('McfBlrOS');
         break;
    case 'Trivandram':
        $('input[name="OS"]').val('McfTriOS');
        break;
    case 'Kochi':
        $('input[name="OS"]').val('McfKocOS');
        break;
    case 'Vienna':
        $('input[name="OS"]').val('McfVieOS');
        break;
    case 'Virginia':
        $('input[name="OS"]').val('McfVirOS');
        break;
    }
}); 
$('#location-app').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="app"]').val('McfBlrApp');
         break;
    case 'Trivandram':
        $('input[name="app"]').val('McfTriApp');
        break;
    case 'Kochi':
        $('input[name="app"]').val('McfKocApp');
        break;
    case 'Vienna':
        $('input[name="app"]').val('McfVieApp');
        break;
    case 'Virginia':
        $('input[name="app"]').val('McfVirApp');
        break;
    }
});  
$('#location-mouse').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="mouse"]').val('McfBlrMou');
         break;
    case 'Trivandram':
        $('input[name="mouse"]').val('McfTriMou');
        break;
    case 'Kochi':
        $('input[name="mouse"]').val('McfKocMou');
        break;
    case 'Vienna':
        $('input[name="mouse"]').val('McfVieMou');
        break;
    case 'Virginia':
        $('input[name="mouse"]').val('McfVirMou');
        break;
    }
});  
$('#location-keyboard').change(function() {
    var myValue = $(this).val();
    switch (myValue) {
    case 'Bangalore':
        $('input[name="keyboard"]').val('McfBlrKey');
         break;
    case 'Trivandram':
        $('input[name="keyboard"]').val('McfTriKey');
        break;
    case 'Kochi':
        $('input[name="keyboard"]').val('McfKocKey');
        break;
    case 'Vienna':
        $('input[name="keyboard"]').val('McfVieKey');
        break;
    case 'Virginia':
        $('input[name="keyboard"]').val('McfVirKey');
        break;
    }
}); 
});