jQuery(function($) {'use strict',

	//#main-slider
	$(function(){
		$('#main-slider.carousel').carousel({
			interval: 800000
		});
	});


	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Initiat WOW JS
	new WOW().init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),

			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
		});
	});

	
	//goto top
	$('.gototop').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 500);
	});	

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});	
});


$(document).ready(function(){



	
   
    // Example 1.4: Sortable and connectable lists (within containment)
    $('#example-1-4 .sortable-list').sortable({

        connectWith: '#example-1-4 .sortable-list',
        //containment: '#containment'
        //receive: This event is triggered when a
        //connected sortable list has received an item from another list.
        receive: function(event, ui) {
            // so if > 10
            if ($(this).children().length > 3) {
                //ui.sender: will cancel the change.
                //Useful in the 'receive' callback.
                $(ui.sender).sortable('cancel');
            }
        }
    }).disableSelection();
});

$(document).ready(function () {
  $('.group').hide();
  $('#option1').show();
  $('#selectMe').change(function () {
    $('.group').hide();
    $('#'+$(this).val()).show();
  })
});


var polarData = [
				{
					value: 180,
					color:"#F7464A",
					highlight: "#FF5A5E",
					label: "TRV"
				},
				{
					value: 30,
					color: "#46BFBD",
					highlight: "#5AD3D1",
					label: "Cochin"
				},
				{
					value: 60,
					color: "#FDB45C",
					highlight: "#FFC870",
					label: "BLR-Indiranagar"
				},
				{
					value: 20,
					color: "#949FB1",
					highlight: "#A8B3C5",
					label: "BLR-Ulsoor"
				}

			];

			

var polarData1 = [
				{
					value: 150,
					color:"#f03e26",
					highlight: "#FF5A5E",
					label: "Virgina"
				},
				{
					value: 50,
					color: "#EBC1D8",
					highlight: "#5AD3D1",
					label: "Dallas"
				}
			];

			

var polarData2 = [
				{
					value: 50,
					color:"#940b0b",
					highlight: "#FF5A5E",
					label: "A city"
				},
				{
					value: 90,
					color: "#4b3832",
					highlight: "#5AD3D1",
					label: "B city"
				}
			];


var dataRadar = {
    labels:["Death", "PIP", "Transfer", "Better Prospects", "Pearsonal"],
    pointLabelFontSize : 16,
    scaleFontSize: 16,
    datasets: [
        {
            label: "India",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [6, 5, 9, 8, 5]
        },
        {
            label: "USA",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [2, 8, 4, 1, 6]
        },
        {
            label: "Brazil",
            fillColor: "rgba(190,155,123,0.4)",
            strokeColor: "rgba(190,155,123,0.4)",
            pointColor: "rgba(190,155,123,0.4)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(190,155,123,0.4)",
            data: [0, 4, 3, 5, 5]
        }
    ]
};

window.onload = function(){
				
				
				if( document.getElementById("chart-area")!=null){
				var ctx = document.getElementById("chart-area").getContext("2d");
								window.myPolarArea = new Chart(ctx).PolarArea(polarData, {
									responsive:true
								});
								$('#js-legend3').html(myPolarArea.generateLegend());

				
				
			

			
				var ctx1 = document.getElementById("chart-area1").getContext("2d");
				window.myPolarArea1 = new Chart(ctx1).PolarArea(polarData1, {
					responsive:true
				});
				$('#js-legend4').html(myPolarArea1.generateLegend());
			


			
				var ctx2 = document.getElementById("chart-area2").getContext("2d");
				window.myPolarArea2 = new Chart(ctx2).PolarArea(polarData2, {
					responsive:true
				});
				$('#js-legend5').html(myPolarArea2.generateLegend());
			}
};
			




    
    $(document).ready(function(){
            $('.widget').change(function(){
            var wid=$(this).data("widget-name");
            $("."+wid).toggle();

            var visible_widget_cnt = 0,
                max_width = 100;

        
            $( ".js-widget-container" ).each(function(e){
                if($(this).is(":visible")){
                    visible_widget_cnt = visible_widget_cnt + 1;
                }
            });


            if(visible_widget_cnt==1){
                 $(".col-sm-3").width(90+"%");
                $(".sortable-item").height(620+"px");
            }else if(visible_widget_cnt==2)
            {
                 $(".col-sm-3").width(43.5+"%");
                $(".sortable-item").height(620+"px");
            }else if(visible_widget_cnt==3)
            {
                 $(".col-sm-3").width(28+"%");
                $(".sortable-item").height(620+"px");
            }else if(visible_widget_cnt==4)
            {
                 $(".col-sm-3").width(43.5+"%");
                $(".sortable-item").height(268+"px");
            }else{
                $(".col-sm-3").width(28+"%");
                $(".sortable-item").height(268+"px");
            }
            
            

            drawChart();
              });
    });
// js for the to do list

	
   
    

    
    