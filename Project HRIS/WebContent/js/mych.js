$(document).ready(function(){

var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November", "December" ],
    datasets: [
        {
            label: "Employee at the start of the Month",
            color: "rgba(220,220,220,0.5)",
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data: [37,39,40,45,47,49,50,53,54,58,60,61]
        },
        {
            label: "Employee at the end of the Month",
            color:"red",
            fillColor: "red",
            strokeColor: "red",
            highlightFill: "red",
            highlightStroke: "red",
            data: [45,39,40,40,47,49,45,53,54,58,60,61]
        },
        {
            label: "No. of employees during the month",
            color:"orange",
            fillColor: "orange",
            strokeColor: "orange",
            highlightFill: "orange",
            highlightStroke: "orange",
            data: [5,3,4,4,7,4,4,5,5,5,6,6]
        }
    ]
};

var data1 = {
    labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November", "December" ],
    datasets: [
        {
            label: "Employee at the start of the Month",
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data: [45,39,40,40,47,49,45,53,54,58,60,61]
        },
        {
            label: "Employee at the end of the Month",
            fillColor: "blue",
            strokeColor: "blue",
            highlightFill: "blue",
            highlightStroke: "blue",
            data: [40,39,40,50,47,46,50,52,50,58,60,61]
        },
        {
            label: "No. of employees during the month",
             fillColor: "green",
            strokeColor: "green",
            highlightFill: "green",
            highlightStroke: "green",
            data: [2,3,4,4,3,4,4,5,5,5,2,2]
        }
    ]
};

var data2 = {
    labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November", "December" ],
    datasets: [
        {
            label: "Employee at the start of the Month",
            fillColor: "rgba(208,194,255,0.5)",
            strokeColor: "rgba(208,194,255,0.5)",
            highlightFill: "rgba(208,194,255,0.5)",
            highlightStroke: "rgba(208,194,255,0.5)",
            data: [40,39,40,50,47,49,50,53,50,58,60,61]
        },
        {
            label: "Employee at the end of the Month",
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,0.8)",
            highlightFill: "rgba(220,220,220,0.75)",
            highlightStroke: "rgba(220,220,220,1)",
            data: [45,39,40,40,47,49,45,53,54,58,60,61]
        },
         {
            label: "No. of employees during the month",
             fillColor: "yellow",
            strokeColor: "yellow",
            highlightFill: "yellow",
            highlightStroke: "yellow",
            data: [2,2,3,4,3,4,4,2,5,5,2,2]
        }
    ]
};


 var options = {
          chart: {
            title: 'Employee Record',
            subtitle: 'Employee joining at start and end of each month: 2014-2015',
          },
          bars: 'vertical', // Required for Material Bar Charts.
          hAxis: {format: 'decimal'},
          height: 500,
          colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var options1 = {
          chart: {
            title: 'Employee Record',
            subtitle: 'Employee joining at start and end of each month: 2014-2015',
          },
          bars: 'vertical', // Required for Material Bar Charts.
          hAxis: {format: 'decimal'},
          height: 500,
          colors: ['#ff6600', '#d95f02', '#8deebb']
        };

        var options2 = {
          chart: {
            title: 'Employee Record',
            subtitle: 'Employee joining at start and end of each month: 2014-2015',
          },
          bars: 'vertical', // Required for Material Bar Charts.
          hAxis: {format: 'decimal'},
          height: 500,
          colors: ['#d0c2ff', '#d95f02', '#7570b3']
        };
			 var ctx = document.getElementById("myChart").getContext("2d");
			 var myNewChart = new Chart(ctx).PolarArea(data);
			var myChart = new Chart(ctx).Bar(data,options);
			$('#js-legend').html(myChart.generateLegend());
			new Chart(ctx).Bar(data, {
			    barStrokeWidth : 6   
			});
			// if(!)
			var ctx = document.getElementById("myChart1").getContext("2d");
			 var myNewChart = new Chart(ctx).PolarArea(data);
			var myChart1 = new Chart(ctx).Bar(data1,options1);
				$('#js-legend1').html(myChart1.generateLegend());
				new Chart(ctx).Bar(data1, {
			    	barStrokeWidth : 6   
				});

			var ctx = document.getElementById("myChart2").getContext("2d");
			var myNewChart = new Chart(ctx).PolarArea(data);
			var myChart2 = new Chart(ctx).Bar(data2,options2);
			$('#js-legend2').html(myChart2.generateLegend());
			new Chart(ctx).Bar(data2, {
			    barStrokeWidth : 6   
			});

            });