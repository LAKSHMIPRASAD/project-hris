<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

 <!DOCTYPE html>
            <html lang="en">
 <head>
 <link href="css/header-footer.css" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">
 <link href="css/responsive.css" rel="stylesheet">
 <script src="js/main.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
 </head>
                    <nav class="navbar navbar-inverse" role="banner">
                        <div class="container">
                        
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                               
                                 <div id="mcfadyen">
                                  <a class="navbar-brand" href="index.jsp">
                                    <img src="images/logo.gif" id="logo1" alt="logo1"></a>



                                    <div id="leftones">
                                       <div class="welcome">
                                          <font color="white"><br>Welcome </font>
                                    </div>
                                    <div class="welcomeName">
                                          <c:if test="${profiledetails.firstName ne null}">
                                          <c:out value="${profiledetails.firstName}"></c:out>
                                          </c:if>
                                           <c:if test="${profiledetails.middleName ne null}">
                                          <c:out value="${profiledetails.middleName}"></c:out>
                                          </c:if>
                                           <c:if test="${profiledetails.lastName ne null}">
                                          <c:out value="${profiledetails.lastName}"></c:out>
                                          </c:if>
                                          
                                          
                                      
                                    </div>
                                    <div class="welcomeEid">
                                     <c:if test="${profiledetails.designation ne null}">
                                           <c:out value="${profiledetails.employeeId}"></c:out>|
                                          <c:out value="${profiledetails.designation}"></c:out>
                                          </c:if>
                                    </div>
                                 
                                    <ul class="nav navbar-nav">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Profile</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="My Profile/profile-general-HR perspective.jsp">General Details</a></li>
                                                <li><a href="My Profile/profile-employment-HR perspective.html">Employment Details</a></li>
                                                <li><a href="My Profile/profile-passport.html">Passport and Visa Details</a></li>
                                                <li><a href="My Profile/document-upload-HR perspective.html">Document Upload</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">HR</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="My Profile/addNewEmployeeGeneralDetails.jsp">Add New Employee</a></li>
                                                <li><a href="employee-search.jsp">Employee Search</a></li>
                                                <li><a href="certification-list.jsp">Certification List</a></li>
                                                <li><a href="holiday-list.jsp">Holiday List</a></li>
                                                 <li><a href="My Profile/document-upload-HR perspective.html">Add New Policy</a></li>
                                            </ul>
                                        </li>
                                        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Finance</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="Reimbursement.html">Reimbursement Form</a></li>
                                                <li><a href="endorsement.html">Endorsement Form</a></li>
                                                <li><a href="form-certification-HR perspective.html">Certification Form</a></li>
                                                <li><a href="#">Salary Certificate</a></li>
                                                <li><a href="reimbursement_track.html">Reimbursement Track</a></li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">NAME TBD</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Releaving Formalities</a>
                                                <ul>
                                                     <li><a href="form-exit interview.html">&nbsp &nbsp Exit Form</a></li>
                                                     <li><a href="noc.html">&nbsp &nbsp NOC Form</a></li>
                                                    
                                                </ul>
                                                </li>

                                                <li><a href="#">Proof of Employment</a>
                                                
                                                </li>
                                                <li><a href="#">Proof of Address</a>
                                                
                                                </li>
                                                
                                            </ul>
                                        </li>

                                       
                                        <li><a href="#">TimeSheet</a></li>
                                        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Assets</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="Laptop.html"> Laptop</a>
                                                <li><a href="desktop.html"> Desktop</a>
                                                <li><a href="monitor.html"> Monitor</a>
                                                <li><a href="server1.html"> Server</a>
                                                <li><a href="storages.html"> Storages</a>
                                                <li><a href="printers.html"> Printer</a>
                                                <li><a href="#"> Network Equipment</a>
                                                <ul>
                                                     <li><a href="routers.html">&nbsp &nbsp Router</a></li>
                                                     <li><a href="switches.html">&nbsp &nbsp Switches</a></li>
                                                    <li><a href="firewalls.html">&nbsp &nbsp Firewall</a></li>
                                                     <li><a href="wifi_aps.html"> &nbsp &nbsp Wifi Aps</a></li>
                                                    
                                                </ul>
                                                </li>

                                                <li><a href="#">VC Unit</a>
                                                <ul>
                                                 <li><a href="vonage-storages.html">&nbsp &nbsp Vonage Router</a></li>
                                                <li><a href="polycom-phones">&nbsp &nbsp Polycom</a></li>
                                                <li><a href="#">&nbsp &nbsp Other Devices</a></li>
                                                 </ul>
                                                </li>
                                                <li><a href="#">Software</a>
                                                <ul>
                                                <li><a href="operating_system.html.html">&nbsp &nbsp Operating System</a></li>
                                                <li><a href="applications.html">&nbsp &nbsp Application</a></li>
            
                                                 </ul>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">&nbsp &nbsp Standard Report</a>
                                                <ul>
                                                     <li><a href="report.html">&nbsp &nbsp HR Dashboard</a></li>
                                                     <li><a href="seperation.html">&nbsp &nbsp Attrition Report</a></li>
                                                    <li><a href="gender.html">&nbsp &nbsp Joiner Gender Ratio</a></li>
                                                     <li><a href="distribution.html"> &nbsp &nbsp Employee Distribution</a></li>
                                                    
                                                </ul>
                                                </li>

                                                <li><a href="#">Custom Report</a>
                                                <ul>
                                                 <li><a href="#">&nbsp &nbsp MyReport#1</a></li>
                                                <li><a href="#">&nbsp &nbsp MyReport#2</a></li>
                                                <li><a href="#">&nbsp &nbsp MyReport#3</a></li>
                                                 </ul>
                                                </li>
                                                <li><a href="#">Adhoc Report</a>
                                                <ul>
                                                <li><a href="Controller.do?customAction=adhoc">&nbsp &nbsp Generate New</a></li>
            
                                                 </ul>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        
                                         
                                        <li class="dropdown">
                                            <a href="policy.html" class="dropdown-toggle" data-toggle="dropdown">Policy Centre</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="navoff.html">Work Culture and Env. Policies</a></li>
                                                <li><a href="condofemp.html">Condition of Employment</a></li>
                                                <li><a href="payroll.html">Payroll and Employee benifits</a></li>
                                                <li><a href="leave.html">Leave Policies</a></li>
                                                <li><a href="relocation.html">Relocation Policy</a></li>
                                                <li><a href="#">Travel Policy</a></li>
                                                <li><a href="#">Performance appraisal</a></li>
                                                <li><a href="#">Discipline and misconduct</a></li>
                                                <li><a href="#">Dress code policy</a></li>
                                                <li><a href="#">General Policy</a></li>
                                                
                                            </ul>
                                        </li>
                                        
                                        
                                                               
                                    </ul>

                                    </div>
                                

                                    <div id="righttabs">
                                    
                                    <div id="personal">
                                     <div id="display">
                                        <img src="images/imagepic.jpg" id="logo2" alt="logo2" class="img-circle">
                                     </div>
                                    
                                     <div id="widgets">
                                        <ul class="nav navbar-nav">
                                       
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-th"></span></a>
                                            <ul class="dropdown-menu">
            
                                                <li> <label><input type="checkbox"  data-widget-name="js-widget-A" class="widget" checked/> &nbsp;Emp Graph</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-B" class="widget" checked/> &nbsp;Quick links</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-C" class="widget" checked/> &nbsp;Time</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-D" class="widget" checked/>&nbsp; Widget D</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-E" class="widget" checked/> &nbsp;Widget E</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-F" class="widget" checked/> &nbsp;Widget F</label></li>
                                           
                                            </ul>
                                         </li>
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog-item.html">Item 1</a></li>
                                                <li><a href="pricing.html">Item 2</a></li>
                                                <li><a href="404.html">Item 3</a></li>
                                                <li><a href="shortcodes.html">Item 4</a></li>
                                            </ul>
                                        </li>
                                                               
                                        </ul>
                                    </div>
                                    </div>
                                          
                            </div>

                                 </div>
                        </div><!--/.container-->
                    </nav><!--/nav-->
                    
                </html>