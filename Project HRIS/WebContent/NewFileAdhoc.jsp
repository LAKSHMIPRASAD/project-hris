<%@page import="com.mcfadyen.hris.util.PropertyUtilNew"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator" %>
<%@page import="com.mcfadyen.hris.metrics.Metrics" %>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

    <%@page import="com.mcfadyen.hris.util.PropertyUtilNew"%>
    <%@page import="com.db.manager.QueryCriteria" %>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">
                <title>Home | HRIS</title>
                
                <!-- core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="css/selectize.default.css" rel="stylesheet">
                <link href="css/NewFileAdhoc.css" rel="stylesheet">
                <link href="css/font-awesome.min.css" rel="stylesheet">
                <link href="css/animate.min.css" rel="stylesheet">
                <link href="css/prettyPhoto.css" rel="stylesheet">
                <link href="css/main.css" rel="stylesheet">
                 
                <link href="css/responsive.css" rel="stylesheet">
                <link href="css/simple-sidebar.css" rel="stylesheet">
                <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
                <script src="js/respond.min.js"></script>
                <![endif]-->       
                <link rel="shortcut icon" href="images/ico/favicon.ico">
                <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
                <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
                <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
                <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
                


                <script src="js/jquery.js"></script>
                
	
	<script type="text/javascript" src="js/tablescript.js"> </script> 
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<style type="text/css" src="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css"></style>
<style>
	table {
	width:90%;
	border-top:1px solid #e5eff8;
	border-right:1px solid #e5eff8;
	margin:1em auto;
	border-collapse:collapse;
	}
	td,th{
	
	border-bottom:1px solid #e5eff8;
	border-left:1px solid #e5eff8;
	padding:.3em 1em;
	text-align:center;
	}
	#submit{
	display:block;
	margin-top: 250px;
	margin-left:10%;
	}
	sel{
	display:inline-block;
	flaot:left;
	}
	#columns{
	margin-left:5%;
	display:inline-block;
	float:left;
	}
	#column,#criteria{
	display:none;
	flaot:left;
	}
	.crit{
	display:none;
	}
	#where{
	display:block;
	}
	.row{
	width:80%;
	margin-left:5%;
	}
	input{
	    line-height: normal;
	    }
</style>

                
                <script src="js/bootstrap.min.js"></script>
               
                <script src="js/main.js"></script>
                
               
                <script src="js/NewFileAdhoc.js"></script>
                <script src="js/jspdf.min.js"></script>
                
                <script src="js/standard_fonts_metrics.js"></script>


                 
                <script src="js/wow.min.js"></script>
                
                
                <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script>
                    <script src="js/jquery.min.js"></script>
                       <script src="js/jquery-ui.min.js"></script>
                        <script src="js/selectize.js"></script>
                        <script type="text/javascript" src="js/tablefilter.js"> </script>
                <script> 
					$(function(){
					  $("#header").load("header.jsp"); 
					  $("#footer").load("footer.html"); 
					});
				</script>
                
    <title>Sidebar</title>

  

    <!-- Custom CSS -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
			    #sortable1, #sortable2 {
        list-style-type: none;
        margin: 0;
        padding: 0;
        float: left;
        margin-right: 10px;
    }
    #sortable1 li, #sortable2 li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        font-size: 1.2em;
        width: 120px;
    }
	</style>

</head>


<body>
<fmt:setBundle basename="/column_name.properties" var="dbProps"/>
   <div id="header"></div>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <div class="wrapper1">
  <nav class="vertical">
    <ul>
      <li>
        <a href="#">Standard Report</a>
        <div>
          <ul>
             <li><a href="report.html">HR Dashboard</a></li>
            <li><a href="seperation.html">Attrition Report</a></li>
            <li><a href="gender.html">Joiner Gender Ratio</a></li>
            <li><a href="distribution.html">Employee Distribution</a></li>
          
          </ul>
        </div>
      </li>
      <li>
        <a href="#">Custom Report</a>
        <div>
          <ul>
            <li><a href="#"><p id="namesave">MyReport#1</p></a></li>
            <li><a href="#">MyReport#2</a></li>
            <li><a href="#">MyReport#3</a></li>
          </ul>
        </div>
      </li>
      <li>
        <a href="#">Adhoc Report</a>
        <div>
          <ul>
            <li><a href="adhoc.html">Generate New</a></li>
            <li><a href="update_prev.jsp">Update Prev.</a></li>
            
          </ul>
        </div>
      </li>
    </ul>
  </nav>
  
</div>

        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
   
 <div class="demo">


<form action="">
        <fieldset id="field1">
            <legend><span>Select Columns</span></legend>
            <select id="selectize" multiple>
   
</select>
<div id="result"></div>
 
        </fieldset>
    </form>
   
	<h3 style="margin-left:4%">Adhoc Reporting</h3>
	<form action="Controller.do?customAction=queryBuilder" method="post">
	<div id="columns">
	<c:forEach items="${MyMap}" var="mapItem">
	<input type=checkbox value="${mapItem.key }" name="select" class="sel">${mapItem.value}<br>
		</c:forEach>
	    
        </div>
        <button id="where" type="button"  class="btn btn-success btn-add">where</button>
        <div id="criteria" class="crit">
            <select  class="column_name" name="criteria">
            <option selected="selected">Column</option>
		<c:forEach items="${MyMap}" var="mapItem">
			
			<option value="${mapItem.key }">
        		<p class="ui-state-default">${mapItem.value} </p><br/>
   			</option>
    	</c:forEach>
				
		</select>
       
        <select class="condition" name="criteria">
        	<option selected="selected">Condtion</option>
        	<option value="=">Equal</option>
        	<option value="!=">Not Equal</option>
        	<option value='<'>Less than</option>
        	<option value='>'> Greater than</option>
        	<option value='like'>Like</option>
        </select>
        <input type="text" name="criteria"/>
        
        </div>
        <button id="add" type="button" class="crit" >+</button>
    <input type="submit" id="submit">
    <br>
    <div class="row">
				<div class="col-md-10">
					<input type="search" class="form-control" placeholder="Filter">
				</div>
				
				<div class="col-md-2">
					<button type="button" class="btn btn-primary btn-block" name="btn-filtro">Search</button>
				</div>
			</div>
    
    <table  name="example-table"  class="table table-bordered table-striped">
    	<tr>
	    	<c:forEach var="column" items="${columnNames }">
	    		<th>${column }</th>
	    	</c:forEach>
    	</tr>
    	<c:forEach var="result" items="${results }" >
	    	<tr>
	    		<c:forEach var="data" items="${result}">
	    				<td>${data }</td>
	    		</c:forEach>
	    	</tr>
    	</c:forEach>
    	
    </table>
    

</form>
   
   
   
   <section class="container">
  
     <div class="control-group" id="fields">
            <label class="control-label" for="field1">Nice Multiple Form Fields</label>
            <div class="controls"> 
                <form role="form" autocomplete="off">
                    <div class="entry input-group col-xs-3" style="width:80%;">
                       <form action="">
        <fieldset id="field">
            <legend><span>Log in</span></legend>
           <select>
		<c:forEach items="${MyMap}" var="mapItem">
			<option>
        		<p class="ui-state-default">${mapItem.value} </p><br/>
   			</option>
    	</c:forEach>
				
		</select>
		<select class="balck">
				<option>Less</option>
				<option>Equals </option>
				<option>Greater</option>
				<option>Greater than Equal</option>
				<option>Less than equal</option>
		</select>
  Field: <input type="text" name="fname" class="tb10">
   
  <br>
   
   <form class="demo">
   <div class="switch switch-blue">
      <input type="radio" class="switch-input" name="view2" value="week2" id="week2" checked>
      <label for="week2" class="switch-label switch-label-off">AND</label>
      <input type="radio" class="switch-input" name="view2" value="month2" id="month2">
      <label for="month2" class="switch-label switch-label-on">OR</label>
      <span class="switch-selection"></span>
    </div>
</form>
 
  </fieldset>
   </form>
                    	<span class="input-group-btn">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                    </div>
                </form>
            <br>
          
            </div>
        </div> 
   
  </section> 
    
   </div>        
    </div>
  <!-- /#page-content-wrapper -->

   <div id="footer"></div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script type="text/javascript">
    $(document).ready(function() {
    	
    	
    	var options=[
    	             {id:0, name:"First Name"},
    	             {id:1, name:"Last Name"},
    	             {id:2, name:"Date of Birth"},
    	             {id:3, name:"Employee ID"},
    	             {id:4, name:"Passport Number"},
    	             {id:5, name:"Passport Expiry Date"},
    	             {id:6, name:"Gender"},
    	             {id:7, name:"Visa Type"},
    	             {id:8, name:"Certification ID"},
    	             {id:9, name:"Email ID"},
    	         ];
    	
    	       

    	$('#selectize').selectize({
    	    plugins: ['remove_button'],
    	    valueField: 'id',
    	    labelField: 'name',
    	    searchField: ['name'],
    	    "options": options,
    	    delimiter: ',',
    	    persist: false,
    	    create: function (input) {
    	      return {
    	        id: input,
    	        name: input,
    	      };
    	    },
    	    hideSelected: true,
    	    openOnFocus: false,
    	});

    	$('#selectize').change(function(){
    	$('#result').html("you select value="+$(this).val());
    	});
    
    
    
			$("#sortable1").sortable({
    connectWith: ".connectedSortable",

    helper: function (e, li) {
        this.copyHelper = li.clone().insertAfter(li);

        $(this).data('copied', false);

        return li.clone();
    },
    stop: function () {

        var copied = $(this).data('copied');

        if (!copied) {
            this.copyHelper.remove();
        }

        this.copyHelper = null;
    }
});

$("#sortable2").sortable({
    receive: function (e, ui) {
        ui.sender.data('copied', true);
    }
});
    });
    
    
    $(function()
    		{
    		    $(document).on('click', '.btn-add', function(e)
    		    {
    		        e.preventDefault();

    		        var controlForm = $('.controls form:first'),
    		            currentEntry = $(this).parents('.entry:first'),
    		            newEntry = $(currentEntry.clone()).appendTo(controlForm);

    		        newEntry.find('input').val('');
    		        controlForm.find('.entry:not(:last) .btn-add')
    		            .removeClass('btn-add').addClass('btn-remove')
    		            .removeClass('btn-success').addClass('btn-danger')
    		            .html('<span class="glyphicon glyphicon-minus"></span>');
    		    }).on('click', '.btn-remove', function(e)
    		    {
    				$(this).parents('.entry:first').remove();

    				e.preventDefault();
    				return false;
    			});
    		});


	</script>



</body>

</html>
