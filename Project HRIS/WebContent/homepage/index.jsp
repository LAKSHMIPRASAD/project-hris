            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">
                <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
                <title>Home | HRIS</title>
                
                <!-- core CSS -->
                <link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="css/font-awesome.min.css" rel="stylesheet">
                <link href="css/animate.min.css" rel="stylesheet">
                <link href="css/prettyPhoto.css" rel="stylesheet">
                <link href="css/main.css" rel="stylesheet">    
                <link href="css/responsive.css" rel="stylesheet">
                <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
                <script src="js/respond.min.js"></script>
                <![endif]-->       
                <link rel="shortcut icon" href="images/ico/favicon.ico">
                <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
                <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
                <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
                <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
                <link href="jquery-ui.css" rel="stylesheet">


				<script type="text/javascript" src="https://www.google.com/jsapi"></script>
				<script src="external/jquery/jquery.js"></script>
				<script src="js/jquery-ui.js"></script>
				<script src="js/bootstrap.min.js"></script>
				<script src="js/jquery.prettyPhoto.js"></script>
				<script src="js/jquery.isotope.min.js"></script>
				<script src="js/wow.min.js"></script>
				<script src="js/main.js"></script>
				<script>
			     google.load("visualization", "1", {packages:["corechart"]});
			     google.setOnLoadCallback(drawChart);
			     function drawChart() {
			       var data = google.visualization.arrayToDataTable([
			         ['Task', 'Hours per Day'],
			             ['Indiranagar',     50],
			             ['Ulsoor',      20],
			             ['Trivandrum',  75],
			             ['Kochi', 54],
			             ['Vienna',    22],
			             ['Dallas',    7],
			             ['Brazil',    5]
			       ]);
			       var options = {
			
			              title: 'Employee Statistics',
			              pieHole: 0.4,
			            };
			      var chart = new google.visualization.PieChart(document.getElementById('chart_div1'));
			        chart.draw(data, options);
			      }
			    </script>
					
				 
                  
				<script> 
					$(function(){
                       $("#header").load("../header/header.jsp"); 
                        $("#footer").load("../footer/footer.html"); 
                  });
				</script>

</head>
<!--/head-->

               
            <body class="homepage">
               <div id="header"></div>
                <div style="height:245px">
                        <!-- BEGIN: XHTML for example 1.4 -->
                                    <div id="example-1-4">

                                        <div id="containment">
                                           
                                            <div class="column">

                                                <ul class="sortable-list">
													<li class="sortable-item js-widget-A js-widget-container col-sm-3" id="chart_div1" class="chart" ></li>
                                                    <li class="sortable-item js-widget-B js-widget-container col-sm-3" > <a  href="http://www.openair.com/" title="Openair" target="_blank"><img class="imglink" src="http://www.geelus.com/wp-content/uploads/2013/09/TimeSheet.png"/></a>
                                            <a  href="https://mcfadyen.atlassian.net/secure/Dashboard.jspa" title="Jira" target="_blank"><img class="imglink" src="https://design.atlassian.com/images/brand/logo-02.png"/></a>
                                             <a  href="https://mcfadyenconsulting.greythr.com/home.do" title="GreyTip" target="_blank"><img class="imglink" src="https://cdn4.iconfinder.com/data/icons/money/512/21-512.png"/></a>
                                            <a  href="https://www.smartrecruiters.com/" title="SmartRecruiter" target="_blank"><img class="imglink"  src="http://ww1.prweb.com/prfiles/2014/08/29/12363891/gI_59710_SmartRecruiters%20Bulb.png"/></a></li>
                                                    <li class="sortable-item js-widget-C js-widget-container col-sm-3" >
                                                    <div >India &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Brazil</div>
                                            <iframe src="http://free.timeanddate.com/clock/i50crfiv/n438/szw110/szh110/hoc000/hbw0/hfceee/cf100/hncccc/fdi76/mqc000/mql10/mqw4/mqd98/mhc000/mhl10/mhw4/mhd98/mmc000/mml10/mmw1/mmd98" frameborder="0" width="120" height="120"></iframe>
                                            &emsp;&emsp;
                                            <iframe src="http://free.timeanddate.com/clock/i50crfiv/n213/szw110/szh110/hoc000/hbw0/hfceee/cf100/hncccc/fdi76/mqc000/mql10/mqw4/mqd98/mhc000/mhl10/mhw4/mhd98/mmc000/mml10/mmw1/mmd98" frameborder="0" width="120" height="120"></iframe>
                                            <!-- <span class="location">Brazil</span> -->
                                            <div >US_EST &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;   US_CST</div>
                                            <iframe src="http://free.timeanddate.com/clock/i50crfiv/n179/szw110/szh110/hoc000/hbw0/hfceee/cf100/hncccc/fdi76/mqc000/mql10/mqw4/mqd98/mhc000/mhl10/mhw4/mhd98/mmc000/mml10/mmw1/mmd98" frameborder="0" width="120" height="120"></iframe>
                                            <!-- <span class="location">US EST<span> -->
                                              &emsp;&emsp;
                                            <iframe src="http://free.timeanddate.com/clock/i50crfiv/n64/szw110/szh110/hoc000/hbw0/hfceee/cf100/hncccc/fdi76/mqc000/mql10/mqw4/mqd98/mhc000/mhl10/mhw4/mhd98/mmc000/mml10/mmw1/mmd98" frameborder="0" width="120" height="120"></iframe>

</li>
                                                    <li class="sortable-item js-widget-D js-widget-container col-sm-3" >Sortable itemD</li>
                                                    <li class="sortable-item js-widget-E js-widget-container col-sm-3" >Sortable item E</li>
                                                    <li class="sortable-item js-widget-F js-widget-container col-sm-3" >Sortable item F</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="todolist"><div id="textfield"><div class="containers">
                        <section class="todo">
                    <ul class="todo-controls">
                        <li><a href="javascript:void(0);" class="icon-add">Add</a></li>
                        <li><a href="javascript:void(0);" class="icon-delete">Delete</a></li>
                        <li class="right"><a href="javascript:void(0);" class="icon-settings">Settings</a></li>
                    </ul>
                
                    <ul class="todo-list">
                        <li class="done">
                            <input type="checkbox" id="find" checked disabled/> 
                            <label class="toggle" for="find"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">Appraisal Details</a>
                           
                        </li>
                        <li>
                            <input type="checkbox" id="build"/> 
                            <label class="toggle" for="build"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">Upcoming planned leaves</a>
                        </li>
                        <li>
                            <input type="checkbox" id="ship"/> 
                            <label class="toggle" for="ship"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">My projects</a>
                        </li>
                        <li>
                            <input type="checkbox" id="ship"/> 
                            <label class="toggle" for="ship"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">My Daily tasks</a>
                        </li>
                        <li>
                            <input type="checkbox" id="ship"/> 
                            <label class="toggle" for="ship"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">Upcoming trainings</a>
                        </li>
                        <li>
                            <input type="checkbox" id="ship"/> 
                            <label class="toggle" for="ship"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">Placeholder 1</a>
                        </li>
                        <li>
                            <input type="checkbox" id="ship"/> 
                            <label class="toggle" for="ship"></label>
                            <a href="file:///C:/HRISEDIT/FINAL/password.html">Placeholder 2</a>
                        </li>
                    </ul>
                    
                    <ul class="todo-pagination">
                        <li class="previous"><span><i class="icon-previous"></i> Previous</span></li>
                        <li class="next"><a href="javascript:void(0);">Next <i class="icon-next"></i></a></li>
                    </ul>
                </section>
            </div></div>


                        <div id="google_calender" ><iframe src="https://calendar.google.com/calendar/embed?showTz=0&amp;height=350&amp;wkst=1&amp;bgcolor=%23ffcccc&amp;src=%23contacts%40group.v.calendar.google.com&amp;color=%232952A3&amp;src=en.indian%23holiday%40group.v.calendar.google.com&amp;color=%23125A12&amp;ctz=Asia%2FCalcutta" style="border:solid 1px #777" width="450" height="350" frameborder="0" scrolling="no"></iframe></div></div>
                             
                        <!-- END: XHTML for example 1.4 -->
                    





                
                <section id="bottom" >
                    
                </section><!--/#bottom-->


	

		<div id="footer"></div>
</body>
</html>
                

