<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="css/header-footer.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">


</head>
<body>
	<nav class="navbar navbar-inverse" role="banner">
		<div class="container">

			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<div id="mcfadyen">
				<a class="navbar-brand" href="index.jsp"> <img
					src="images/logo.gif" id="logo1" alt="logo1"></a>



				<div id="leftones">
					<div class="welcome">
						<font color="white"><br>Welcome </font>
					</div>
					<div class="welcomeName">
						<c:if test="${profiledetails.firstName ne null}">
							<c:out value="${profiledetails.firstName}"></c:out>
						</c:if>
						<c:if test="${profiledetails.middleName ne null}">
							<c:out value="${profiledetails.middleName}"></c:out>
						</c:if>
						<c:if test="${profiledetails.lastName ne null}">
							<c:out value="${profiledetails.lastName}"></c:out>
						</c:if>



					</div>
					<div class="welcomeEid">
						<c:if test="${profiledetails.designation ne null}">
							<c:out value="${profiledetails.employeeId}"></c:out>|
                                          <c:out
								value="${profiledetails.designation}"></c:out>
						</c:if>
					</div>

					<ul class="nav navbar-nav">
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">My Profile</a>
							<ul class="dropdown-menu">
								<li><a href="My Profile/profile-general-HR perspective.jsp">General
										Details</a></li>
								<li><a
									href="My Profile/profile-employment-HR perspective.html">Employment
										Details</a></li>
								<li><a href="My Profile/profile-passport.html">Passport
										and Visa Details</a></li>
								<li><a
									href="My Profile/document-upload-HR perspective.html">Document
										Upload</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">HR</a>
							<ul class="dropdown-menu">
								<li><a href="My Profile/addNewEmployeeGeneralDetails.jsp">Add
										New Employee</a></li>
								<li><a href="employee-search.jsp">Employee Search</a></li>
								<li><a href="certification-list.jsp">Certification List</a></li>
								<li><a href="holiday-list.jsp">Holiday List</a></li>
								<li><a
									href="My Profile/document-upload-HR perspective.html">Add
										New Policy</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">Finance</a>
							<ul class="dropdown-menu">
								<li><a href="Reimbursement.html">Reimbursement Form</a></li>
								<li><a href="endorsement.html">Endorsement Form</a></li>
								<li><a href="form-certification-HR perspective.html">Certification
										Form</a></li>
								<li><a href="#">Salary Certificate</a></li>
								<li><a href="reimbursement_track.html">Reimbursement
										Track</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">NAME TBD</a>
							<ul class="dropdown-menu">
								<li><a href="#">Releaving Formalities</a>
									<ul>
										<li><a href="form-exit interview.html">Exit Form</a></li>
										<li><a href="noc.html">NOC Form</a></li>

									</ul></li>

								<li><a href="#">Proof of Employment</a></li>
								<li><a href="#">Proof of Address</a></li>

							</ul></li>


						<li><a href="#">TimeSheet</a></li>

						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown">Assets</a>
							<ul class="dropdown-menu">
								<li><a href="Laptop.html"> Laptop</a> <li><a
									href="desktop.html"> Desktop</a>
                                                
								<li><a href="monitor.html"> Monitor</a>
                                                
								<li><a href="server1.html"> Server</a>
                                                
								<li><a href="storages.html"> Storages</a>
                                                
								<li><a href="printers.html"> Printer</a>
                                                
								<li><a href="#"> Network Equipment</a>
                                                <ol id="submenu">
                                                     <li><a
											href="routers.html">Router</a></li>
                                                     <li><a
											href="switches.html">Switches</a></li>
                                                  	 <li><a
											href="firewalls.html">Firewall</a></li>
                                                     <li><a
											href="wifi_aps.html">Wifi Aps</a></li>
                                                    
                                                </ol>
                                                </li>

                                                <li><a href="#">VC Unit</a>
                                                <ol id="submenu">
                                                 <li><a
											href="vonage-storages.html">Vonage Router</a></li>
                                                <li><a
											href="polycom-phones">Polycom</a></li>
                                                <li><a href="#">Other Devices</a></li>
                                                 </ol>
                                                </li>
                                                <li><a href="#">Software</a>
                                                <ol id="submenu">
                                                <li><a
											href="operating_system.html">Operating System</a></li>
                                                <li><a
											href="applications.html">Application</a></li>
            
                                                 </ol>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        
                                        <li class="dropdown">
                                            <a href="#"
							class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Standard Report</a>
                                                <ol id="submenu">
                                                     <li><a
											href="report.html">HR Dashboard</a></li>
                                                     <li><a
											href="seperation.html">Attrition Report</a></li>
                                                     <li><a
											href="gender.html">Joiner Gender Ratio</a></li>
                                                     <li><a
											href="distribution.html">Employee Distribution</a></li>
                                                    
                                                </ol>
                                                </li>

                                                <li><a href="#">Custom Report</a>
                                                <ol id="submenu">
                                                 <li><a href="#">MyReport#1</a></li>
                                                <li><a href="#">MyReport#2</a></li>
                                                <li><a href="#">MyReport#3</a></li>
                                                 </ol>
                                                </li>
                                                <li><a href="#">Adhoc Report</a>
                                                <ol id="submenu">
                                                <li><a
											href="Controller.do?customAction=adhoc">Generate New</a><br></li>
            
                                                 </ol>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        
                                         
                                        <li class="dropdown">
                                            <a href="policy.html"
							class="dropdown-toggle" data-toggle="dropdown">Policy Centre</a>
                                            <ul class="dropdown-menu">
                                                <li><a
									href="navoff.html">Work Culture and Env. Policies</a></li>
                                                <li><a
									href="condofemp.html">Condition of Employment</a></li>
                                                <li><a
									href="payroll.html">Payroll and Employee benifits</a></li>
                                                <li><a
									href="leave.html">Leave Policies</a></li>
                                                <li><a
									href="relocation.html">Relocation Policy</a></li>
                                                <li><a href="#">Travel Policy</a></li>
                                                <li><a href="#">Performance appraisal</a></li>
                                                <li><a href="#">Discipline and misconduct</a></li>
                                                <li><a href="#">Dress code policy</a></li>
                                                <li><a href="#">General Policy</a></li>
                                                
                                            </ul>
                                        </li>
                                        
                                        
                                                               
                                    </ul>

                                    </div>
                                

                                    <div id="righttabs">
                                    
                                    <div id="personal">
                                     <div id="display">
                                        <img
								src="http://regentsresidency.com/regentsresidency/usermgmt/img/default.jpg"
								id="logo2" alt="logo2" class="img-circle">
                                     </div>
                                    

                                 </div>
                        </div>
				<!--/.container-->
                    
	</nav><!--/nav-->