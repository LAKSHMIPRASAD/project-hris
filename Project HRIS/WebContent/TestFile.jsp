<%@page import="com.mcfadyen.hris.util.PropertyUtilNew"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Iterator" %>
<%@page import="com.mcfadyen.hris.metrics.Metrics" %>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

    <%@page import="com.mcfadyen.hris.util.PropertyUtilNew"%>
    <%@page import="com.db.manager.QueryCriteria" %>

<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">
                <title>Home | HRIS</title>
                
                <!-- core CSS -->
            
                <link href="css/selectize.default.css" rel="stylesheet">
               
                <link href="css/NewFileAdhoc.css" rel="stylesheet">
                <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
                <script src="js/respond.min.js"></script>
                <![endif]-->       
               

                <script src="js/selectize.js"></script>
                <script src="js/NewFileAdhoc.js"></script>
                 
                <script src="js/wow.min.js"></script>
                
                
                <script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>
                <script> 
					$(function(){
					  $("#header").load("header.jsp"); 
					  $("#footer").load("footer.html"); 
					});
				</script>
                
    <title>Sidebar</title>

   

    <!-- Custom CSS -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  
</head>


<body>
<fmt:setBundle basename="/column_name.properties" var="dbProps"/>
   <div id="header"></div>

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <div class="wrapper1">
  <nav class="vertical">
    <ul>
      <li>
        <a href="#">Standard Report</a>
        <div>
          <ul>
             <li><a href="report.html">HR Dashboard</a></li>
            <li><a href="seperation.html">Attrition Report</a></li>
            <li><a href="gender.html">Joiner Gender Ratio</a></li>
            <li><a href="distribution.html">Employee Distribution</a></li>
          
          </ul>
        </div>
      </li>
      <li>
        <a href="#">Custom Report</a>
        <div>
          <ul>
            <li><a href="#"><p id="namesave">MyReport#1</p></a></li>
            <li><a href="#">MyReport#2</a></li>
            <li><a href="#">MyReport#3</a></li>
          </ul>
        </div>
      </li>
      <li>
        <a href="#">Adhoc Report</a>
        <div>
          <ul>
            <li><a href="adhoc.html">Generate New</a></li>
            <li><a href="update_prev.jsp">Update Prev.</a></li>
            
          </ul>
        </div>
      </li>
    </ul>
  </nav>
  
</div>

        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
        
<%-- <jsp:useBean id="One" class="com.db.manager.QueryCriteria" scope="session">
	<jsp:setProperty name="One" property="column_name" value="emo_id"/>
	<jsp:setProperty name="One" property="condition" value="="/>
	<jsp:setProperty name="One" property="value" value="E 100"/>
	<jsp:getProperty name="One" property="column_name" />
	</jsp:useBean> --%>
	
	<%-- <jsp:useBean id="two" class="com.db.manager.Pojo" scope="session">
	<jsp:setProperty  name="two" property="a" value="abcxyz"></jsp:setProperty>
	<jsp:getProperty name="two" property="a"></jsp:getProperty>
	</jsp:useBean> --%>
	<jsp:useBean id="one" class="com.db.manager.QueryCriteria" scope="session">
	<jsp:setProperty name="one" property="columnName" value="emp_id"></jsp:setProperty>
	<jsp:setProperty name="one" property="condition" value="="></jsp:setProperty>
	<jsp:setProperty name="one" property="value" value="E 200"></jsp:setProperty>
	</jsp:useBean>
	
		
	<jsp:useBean id="two" class="com.db.manager.QueryCriteria" scope="session">
	<jsp:setProperty name="two" property="columnName" value="first_name"></jsp:setProperty>
	<jsp:setProperty name="two" property="condition" value="="></jsp:setProperty>
	<jsp:setProperty name="two" property="value" value="Lakshmiprasad"></jsp:setProperty>
	<jsp:setProperty name="two" property="link" value="or"></jsp:setProperty>
	<jsp:setProperty name="two" property="queryCriteria" value="${one}"></jsp:setProperty>
	</jsp:useBean>
		
	<jsp:useBean id="three" class="com.db.manager.QueryCriteria" scope="session">
	<jsp:setProperty name="three" property="columnName" value="first_name"></jsp:setProperty>
	<jsp:setProperty name="three" property="condition" value="="></jsp:setProperty>
	<jsp:setProperty name="three" property="value" value="Anu"></jsp:setProperty>
	<jsp:setProperty name="three" property="link" value="and"></jsp:setProperty>
	<jsp:setProperty name="three" property="queryCriteria" value="${two}"></jsp:setProperty>
	</jsp:useBean>
	
	<%QueryCriteria qc1=(QueryCriteria)request.getSession().getAttribute("one");
		out.println("<p>"+qc1+"<p>");
		QueryCriteria qc2=(QueryCriteria)request.getSession().getAttribute("two");
		out.println("<p>"+qc2+"<p>");
		
		QueryCriteria qc3=(QueryCriteria)request.getSession().getAttribute("three");
		out.println("<p>"+qc3+"<p>");
		
	%>
	
<section class="container">
    <div class="switch">
      <input type="radio" class="switch-input" name="view" value="week" id="week" checked>
      <label for="week" class="switch-label switch-label-off">AND</label>
      <input type="radio" class="switch-input" name="view" value="month" id="month">
      <label for="month" class="switch-label switch-label-on">OR</label>
      <span class="switch-selection"></span>
    </div>
    
    <div class="switch switch-blue">
      <input type="radio" class="switch-input" name="view2" value="week2" id="week2" checked>
      <label for="week2" class="switch-label switch-label-off">AND</label>
      <input type="radio" class="switch-input" name="view2" value="month2" id="month2">
      <label for="month2" class="switch-label switch-label-on">OR</label>
      <span class="switch-selection"></span>
    </div>
    
    <form class="demo">
		<select>
		<c:forEach items="${MyMap}" var="mapItem">
				<option>
        <p class="ui-state-default">${mapItem.value} </p><br/>
    </option></c:forEach>
				
		</select>
		<select class="balck">
				<option>Less</option>
				<option>Equals </option>
				<option>Greater</option>
				<option>Greater than Equal</option>
				<option>Less than equal</option>
		</select>
  Field: <input type="text" name="fname" class="tb10"><br>
  
</form>

<select id="selectize" multiple>
    <option value=0 selected>Option 0</option>
    <option value=1 selected>Option 1</option>
</select>
<div id="result"></div>

  </section> 
    
           
    </div>
  <!-- /#page-content-wrapper -->

   <div id="footer"></div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->


</body>

</html>
