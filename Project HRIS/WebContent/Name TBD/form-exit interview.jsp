<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Exit Interview</title>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/header-footer.css" rel="stylesheet">
<link href="../css/profile.css" rel="stylesheet">
<link href="../css/responsive.css" rel="stylesheet">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="js/profile.js"></script>
<script>
	$(function() {
		$("#header").load("../header/header.jsp");
		$("#footer").load("../footer/footer.html");
	});
</script>
</head>
<body>
	<div id="header"></div>
	<div class="container-floating">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<form role="form">
			<div class="panel panel-default  exit-heading">
				<div class="panel-heading clearfix quickfix-exit">
					<h4 class="form-heading">Exit Interview</h4>
				</div>
				<div class="form-group col-sm-12">
					<div class="required field-item">
						<label class="control-label col-sm-2" for="employee-number">Employee
							Name:</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="employee-number" />
						</div>
					</div>
					<div class="field-item">
						<label class="control-label col-sm-1" for="designation">Position:
						</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="position">
						</div>
					</div>
					<div class="required field-item">
						<label class="control-label col-sm-2" for="Manager">Manager's
							Name:</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="mamager-name">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-12">What prompted you to
						seek alternative employment?</label>

					<table>
						<tr>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="type-of-work" name="alternativeEmployment"
									required="required">Type of Work</label></td>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="quality-of-supervision" name="alternativeEmployment"
									required="required">Quality of Supervision</label></td>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="compensation" name="alternativeEmployment"
									required="required">Compensation or Benefits</label></td>
						</tr>
						<tr>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="work-condition" name="alternativeEmployment"
									required="required">Work Condition</label></td>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="lack-of-recognition" name="alternativeEmployment"
									required="required">Lack of recognition</label></td>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="family-circumstances" name="alternativeEmployment"
									required="required">Family Circumstances</label></td>
						</tr>
						<tr>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="company-culture" name="alternativeEmployment"
									required="required">Company Culture</label></td>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="career-advancement-opportunities"
									name="alternativeEmployment" required="required">Career Advancement
									Opportunities</label></td>
							<td class="small-text-cell"><label class="radio-inline"><input
									type="radio" id="business" name="alternativeEmployment" required="required">Business
									or Product Direction</label></td>
						</tr>
						<tr>
							<td colspan="3" class="cell-text-field"><label
								class="control-label" for="other-reasons">Other</label> <input
								type="text" id="other" name="alternativeEmploymentOther"
								class="form-control">
						</tr>
					</table>

				</div>
				<div class="form-group">
					<label class="control-label col-sm-12">Before making your
						decision to leave, did you investigate other options that would
						allow you to stay?</label>
					<table>
						<tr class="table-textbox">
							<td><label class="radio-inline"> <input type="radio"
									id="company-culture" name="otherOptions" required="required">Yes
							</label></td>
							<td class="cell-text-field"><input type="text"
								name="otherOptionsReason" class="form-control"
								placeholder="Please Describe"></td>
						</tr>
						<tr class="table-textbox">
							<td><label class="radio-inline"> <input type="radio"
									id="company-culture" name="otherOptions" required="required">No
							</label></td>
							<td class="cell-text-field"><input type="text"
								name="otherOptionsReason" class="form-control"
								placeholder="Please Describe"></td>
						</tr>
					</table>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-12">Based on your
						experience, please rate the following:</label>


					<table>
						<caption>Core Business</caption>
						<tr>
							<th></th>
							<th>Strongly Agree</th>
							<th>Agree</th>
							<th>Neither Agree nor Disagree</th>
							<th>Disagree</th>
							<th>Strongly Disagree</th>
						</tr>
						<tr>
							<td class="cell-contails-text">The company has a positive
								image in the community</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="positiveImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="positiveImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="positiveImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="positiveImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="positiveImage" required="required"></td>
						</tr>
						<tr>

							<td class="cell-contails-text">the internal culture is
								aligned with the image it portrays</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="cultureAlignedImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="cultureAlignedImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="cultureAlignedImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="cultureAlignedImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="cultureAlignedImage" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">The company lives by its core
								values</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="coreValues" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="coreValues" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="coreValues" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="coreValues" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="coreValues" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I understood the company's
								goals</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="companyGoals" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="companyGoals" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="companyGoals" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="companyGoals" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="companyGoals" required="required"></td>
						</tr>
					</table>

					<table>
						<caption>Management</caption>
						<tr>
							<th>My Manager:</th>
							<th>Strongly Agree</th>
							<th>Agree</th>
							<th>Neither Agree nor Disagree</th>
							<th>Disagree</th>
							<th>Strongly Disagree</th>
						</tr>
						<tr>
							<td class="cell-contails-text">Demonstrated fairness and
								equality between employees</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairness" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairness" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairness" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairness" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairness" required="required"></td>

						</tr>
						<tr>

							<td class="cell-contails-text">Adequately recognized my
								efforts</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="recognition" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="recognition" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="recognition" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="recognition" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="recognition" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Fostered cooperation and
								team work within department</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentTeamWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentTeamWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentTeamWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentTeamWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentTeamWork" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Fostered cooperation and
								team work with other departments</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentTeamwork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentTeamwork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentTeamwork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentTeamwork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentTeamwork" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Encouraged/listened to
								suggestions</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="suggestions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="suggestions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="suggestions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="suggestions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="suggestions" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Resolved complaints and
								problems</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resolvingProblems" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resolvingProblems" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resolvingProblems" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resolvingProblems" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resolvingProblems" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Followed policies and
								practices</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followingPolicies" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followingPolicies" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followingPolicies" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followingPolicies" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followingPolicies" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Demonstrated follow through</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followThrough" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followThrough" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followThrough" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followThrough" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="followThrough" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Provided an adequate level of
								supervision/direction</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedDirection" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedDirection" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedDirection" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedDirection" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedDirection" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Provided adequate feedback
								regarding my performance</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedFeedback" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedFeedback" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedFeedback" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedFeedback" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="providedFeedback" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Ensured adequate resources
								were available to do my job</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="ensuredResources" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="ensuredResources" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="ensuredResources" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="ensuredResources" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="ensuredResources" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Communicated well with me</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCommunication" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Communicated well with the
								department</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interDepartmentCommunication" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">Communicated well with other
								departments</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentCommunication" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="intraDepartmentCommunication" required="required"></td>
						</tr>
					</table>

					<table>
						<caption>Competitive Practices</caption>
						<tr>
							<th></th>
							<th>Strongly Agree</th>
							<th>Agree</th>
							<th>Neither Agree nor Disagree</th>
							<th>Disagree</th>
							<th>Strongly Disagree</th>
						</tr>
						<tr>
							<td class="cell-contails-text">I was paid fairly</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairPay" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairPay" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairPay" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairPay" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="fairPay" required="required"></td>
						</tr>
						<tr>

							<td class="cell-contails-text">the internal culture is
								aligned with the image it portrays</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="competitonCultureImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="competitonCultureImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="competitonCultureImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="competitonCultureImage" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="competitonCultureImage" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">There was recognition for
								efforts, achievements, successes</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="effortsRecognised" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="effortsRecognised" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="effortsRecognised" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="effortsRecognised" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="effortsRecognised" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">The working conditions were
								favorable</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingConditions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingConditions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingConditions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingConditions" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingConditions" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">The work environment was free
								of hazards</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workEnvironment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workEnvironment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workEnvironment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workEnvironment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workEnvironment" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">There was stability with
								company (consistent employment)</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="stability" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="stability" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="stability" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="stability" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="stability" required="required"></td>
						</tr>
					</table>

					<table>
						<caption>Career Potential</caption>
						<tr>
							<th></th>
							<th>Strongly Agree</th>
							<th>Agree</th>
							<th>Neither Agree nor Disagree</th>
							<th>Disagree</th>
							<th>Strongly Disagree</th>
						</tr>
						<tr>
							<td class="cell-contails-text">I received adequate training
								to do my job</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="adequateTraining" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="adequateTraining" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="adequateTraining" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="adequateTraining" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="adequateTraining" required="required"></td>
						</tr>
						<tr>

							<td class="cell-contails-text">There were opportunities for
								me to do interesting work</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interestingWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interestingWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interestingWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interestingWork" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="interestingWork" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I was given the opportunity
								to learn new skills</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="learningNewSkills" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="learningNewSkills" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="learningNewSkills" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="learningNewSkills" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="learningNewSkills" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I was personally challenged</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCallenges" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCallenges" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCallenges" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCallenges" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="personalCallenges" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">There were adequate promotion
								opportunities</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="promotionOpportunities" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="promotionOpportunities" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="promotionOpportunities" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="promotionOpportunities" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="promotionOpportunities" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I had the opportunity to get
								involved in special assignments</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="specialAssignment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="specialAssignment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="specialAssignment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="specialAssignment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="specialAssignment" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">My manager was interested in
								my career development</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="careerDevelopment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="careerDevelopment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="careerDevelopment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="careerDevelopment" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="careerDevelopment" required="required"></td>
						</tr>
					</table>

					<table>
						<caption>Work Life Balance</caption>
						<tr>
							<th></th>
							<th>Strongly Agree</th>
							<th>Agree</th>
							<th>Neither Agree nor Disagree</th>
							<th>Disagree</th>
							<th>Strongly Disagree</th>
						</tr>
						<tr>
							<td class="cell-contails-text">The number of hours I worked
								each week was reasonable</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resonableWorkHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resonableWorkHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resonableWorkHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resonableWorkHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="resonableWorkHours" required="required"></td>
						</tr>
						<tr>

							<td class="cell-contails-text">I felt comfortable taking
								vacation</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableVacation" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableVacation" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableVacation" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableVacation" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableVacation" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I felt comfortable taking
								rest/meal breaks</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableBreaks" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableBreaks" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableBreaks" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableBreaks" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="comfortableBreaks" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">My job did not negatively
								impact my commitments outside of work</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLifebalance" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLifebalance" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLifebalance" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLifebalance" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLifebalance" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I had flexibility in my work
								schedule</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="flexibleWorkSchedule" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="flexibleWorkSchedule" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="flexibleWorkSchedule" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="flexibleWorkSchedule" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="flexibleWorkSchedule" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I did not feel like I needed
								to work outside normal work hours</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingExtraHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingExtraHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingExtraHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingExtraHours" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workingExtraHours" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">My work load was about right</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLoad" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLoad" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLoad" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLoad" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="workLoad" required="required"></td>
						</tr>
						<tr>
							<td class="cell-contails-text">I did not feel overly
								stressed by the expectations placed on me</td>
							<td><input type="radio" class="fancy-radiobutton"
								name="expectationStress" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="expectationStress" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="expectationStress" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="expectationStress" required="required"></td>
							<td><input type="radio" class="fancy-radiobutton"
								name="expectationStress" required="required"></td>
						</tr>
					</table>
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">What about the work
						environment would you like me to know?</label> <input type="text"
						id="other" name="workEnvironmentInformation"
						class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">What was most
						enjoyable about your job?</label> <input type="text" id="other"
						name="enjoyablePartOfJob" class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">What was most
						enjoyable the company?</label> <input type="text" id="other"
						name="enjoyablePartOfCompany" class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">What was most
						frustrating about your job?</label> <input type="text" id="other"
						name="frustratingPartOfJob" class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">What was most
						frustrating about the company?</label> <input type="text" id="other"
						name="frustrationPartOfCompany" class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">What is better about
						your new Job? Company? </label> <input type="text" id="other"
						name="betterAboutNewJob" class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">Overall, how
						satisfied were you with your job?</label> <label
						class="radio-inline radio-spacing"><input type="radio"
						id="type-of-work" name="overallSatisfaction" required="required">Very
						Satisfied</label> <label class="radio-inline radio-spacing"><input
						type="radio" id="type-of-work" name="overallSatisfaction"
						required="required">Satisfied</label> <label
						class="radio-inline radio-spacing"><input type="radio"
						id="type-of-work" name="overallSatisfaction" required="required">Neither</label>
					<label class="radio-inline radio-spacing"><input
						type="radio" id="type-of-work" name="overallSatisfaction"
						required="required">Dissatisfied</label> <label
						class="radio-inline radio-spacing"><input type="radio"
						id="type-of-work" name="overallSatisfaction" required="required">Very
						Dissatisfied</label>
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">On a scale of 1-5
						please rate your supervisor (1 being lowest and 5 being highest) </label>
					<input type="text" id="other" name="other-reasons-text"
						class="form-control question-text-field">
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">Would you recommend
						this company to others as a place to work?</label> <label
						class="radio-inline radio-spacing"><input type="radio"
						id="type-of-work" name="recommendation" required="required">Yes,
						without hesitation</label> <label class="radio-inline radio-spacing"><input
						type="radio" id="type-of-work" name="recommendation"
						required="required">Yes, with hesitation</label> <label
						class="radio-inline radio-spacing"><input type="radio"
						id="type-of-work" name="recommendation" required="required">No</label>
				</div>
				<div class="form-group question-text-group">
					<label class="control-label col-sm-12">Is there anything
						else you would like to add or share?</label> <input type="text"
						class="form-control question-text-field" name="other-reasons-text">
				</div>
			</div>
			<div class="col-sm-8"></div>
			<div class="col-sm-4">
				<button type="submit" class="submit-space">Submit</button>
			</div>
		</form>
	</div>
	</div>
	<div id="footer"></div>
</body>
</html>