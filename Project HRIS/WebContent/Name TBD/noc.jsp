<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Profile | Employment</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="../css/form.css" rel="stylesheet">

  <link href="../css/responsive.css" rel="stylesheet"> 
<link href="../css/overlaypopup.css" rel="stylesheet">
<link href="../css/ReimbursementForm.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" src="../js/reimbursement_append.js"></script>
<script type="text/javascript" src="../js/noc.js"></script>
<script src="../external/jquery/jquery.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.prettyPhoto.js"></script>
<script src="../js/jquery.isotope.min.js"></script>
<script src="../js/wow.min.js"></script>
<script src="../js/main.js"></script>
<script src="../js/jquery.js"></script>
 

<script>

</script>
<script type="text/javascript" src="../js/custom.js"></script>

<script> 
$(function(){
  $("#header").load("../header/header.jsp"); 
  $("#footer").load("../footer/footer.html"); 
});
</script>

</head>
<body>

 <div id="header">
 
 </div>
	<div class="container-floating">
		

		<div class="form-entire">

			<div class="col-sm-2"></div>

			<div class="col-sm-8 panel panel-default">
				<div class="form-group ">
					<div class="panel-heading clearfix quickfix3 noc-heading">
						<h4 class="form-heading">NO DUE/NO OBJECTION CERTIFICATE</h4>
				<button type="button" id="expandAll" class="submit " onclick="displayFormContent(this)" data-form-type='99' style="float:right ;background-color:#707070">
        <span class="glyphicon glyphicon-triangle-bottom"></span> Expand All 
        </button>
					</div>
				</div>
				<div class="col-sm-2"></div>
				<div class="col-sm-8 panel panel-default" >
					<form class="form-horizontal" role="form">
						<div class="form-group " >
							<div class="panel-heading clearfix quickfix3 changeColor">
								<h4 class="form-heading"  onclick="displayFormContent(this)" data-form-type='0'>Reporting Manager</h4>
								 <span class="glyphicon editIcon glyphicon-pencil" style="float:right" onclick="edit(this)" data-box-type="0">
								 </span>
							</div>
						</div>
						<div class="box" >
							<div  class="form-group required" >
								<div class="col-sm-2">
							
								</div>
								
								<div class="col-sm-10 custom-noc">
									<p>
									<div class="glyphicon glyphicon-ok tick "
										style="visibility: hidden"></div>
									</p>
									<p class="msg" style="color: green"></p>
									<br> 
									
									<label class="col-sm-7">
									
									<input class="boxchild" type="checkbox"  name="vehicle" value="1" >Handing
										over of responsibilities 
									  </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
									  <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
									  <pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br>
									
									
									  <label class="col-sm-7">
									  <input class="boxchild" type="checkbox"  name="vehicle" value="2"> Back up
									    of project files/docs
                                       </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
						               <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre> <br>
										
										
										<label class="col-sm-7">
										<input class="boxchild" type="checkbox"  name="vehicle" value="3">Knowledge
										transfer
										</label>
										<pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
									   <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br>
										
										<label class="col-sm-7 custom-noc1">
										<input class="boxchild" type="checkbox" name="vehicle" value="4">Removal
										of access rights to project folders
										</label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
								        <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br> 
										
										<div class="col-sm-2"></div>
												<div class="col-sm-12"></div>
									</div>	
									
									<div class="submit-button" id="hideAll">
									<div class="col-sm-3"></div>
									<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="1" data-box-type="0"
											onclick="noti(this)">Approve</button>
											
											</div>
											<div class="col-sm-2" style="padding-top:14.5px">
											<P ><a class="show-popup" href="#" data-showpopup="1" ><input type="submit" name="radio" data-status-type="2" data-box-type="0" id="noc" value="Reject" onclick="noti(this)" class="submit btn btn-success btn-table"   style="background-color:#E01F27;" /></a> </P>
											</div>
											<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="3" data-box-type="0"
											onclick="pending(this)">Submit</button>
											
											</div>
											<div class="col-sm-3"></div>
									</div>
								
								
							</div>
						</div>
					</form>
				</div>
				
				<div class="col-sm-2">
				<label><p  style="color:green" id="approve"><span class="glyphicon glyphicon-arrow-right arrow"></span>Approved</p></label>
				<label><p  style="color:orange" id="pending"><span class="glyphicon glyphicon-arrow-right arrow"></span>Pending</p></label>
				<label><p  style="color:red" id="rejected"><span class="glyphicon glyphicon-arrow-right arrow"></span>Rejected</p></label>
				</div>
				
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				
				<div class="col-sm-8 panel panel-default">
					<form class="form-horizontal" role="form">
						<div class="form-group " >
							<div class="panel-heading clearfix quickfix3 changeColor" >
								<h4 class="form-heading"  onclick="displayFormContent(this)" data-form-type='1'>Library</h4>
								 <span class="glyphicon editIcon glyphicon-pencil" style="float:right" onclick="edit(this)" data-box-type="1">
								 </span>
							</div>
						</div>
						<div class="box" >
							<div  class="form-group required" >
								<div class="col-sm-2">
							
								</div>
								
								<div class="col-sm-10 custom-noc">
									<p>
									<div class="glyphicon glyphicon-ok tick "
										style="visibility: hidden"></div>
									</p>
									<p class="msg" style="color: green"></p>
									<br> 
									
									<label class="col-sm-7">
									
									<input class="boxchild" type="checkbox"  name="vehicle" value="1" >Return of Technical books 
									  </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
									  <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="2" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
									  <pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br>
									
									
									  <label class="col-sm-7">
									  <input class="boxchild" type="checkbox"  name="vehicle" value="2"> Return of Non-Tech books 
                                       </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
						               <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="2" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre> <br>
										
										
										
										
										<div class="col-sm-2"></div>
												<div class="col-sm-12"></div>
									</div>	
									
									<div class="submit-button" id="hideAll">
									<div class="col-sm-3"></div>
									<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="1"
											onclick="noti(this)" data-box-type="1">Approve</button>
											
											</div>
											<div class="col-sm-2" style="padding-top:14.5px">
											<P ><a class="show-popup" href="#" data-showpopup="2" ><input type="submit" name="radio" data-status-type="2" data-box-type="1" id="noc" value="Reject" onclick="noti(this)" class="submit btn btn-success btn-table"   style="background-color:#E01F27" /></a> </P>
											</div>
											<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="3"
											data-box-type="1" onclick="pending(this)">Submit</button>
											
											</div>
											<div class="col-sm-3"></div>
									</div>
								
								
							</div>
						</div>
					</form>
				.</div>
				
				
				
				
				
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				
				
				
				<div class="col-sm-8 panel panel-default">
					<form class="form-horizontal" role="form">
						<div class="form-group " >
							<div class="panel-heading clearfix quickfix3 changeColor">
								<h4 class="form-heading"  onclick="displayFormContent(this)" data-form-type='2'>Administration Department</h4>
								 <span class="glyphicon editIcon glyphicon-pencil" style="float:right" onclick="edit(this)" data-box-type="2">
								 </span>
							</div>
						</div>
						<div class="box" >
							<div  class="form-group required" >
								<div class="col-sm-2">
							
								</div>
								
								<div class="col-sm-10 custom-noc">
									<p>
									<div class="glyphicon glyphicon-ok tick "
										style="visibility: hidden"></div>
									</p>
									<p class="msg" style="color: green"></p>
									<br> 
									
									<label class="col-sm-7">
									
									<input class="boxchild" type="checkbox"  name="vehicle" value="1" >Return of
										ID Card/removal of access rights
									  </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
									  <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="3" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
									  <pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br>
									
									
									  <label class="col-sm-7">
									  <input class="boxchild" type="checkbox"  name="vehicle" value="2">Return
										of keys/other possessions/furniture 
                                       </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
						               <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="3" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre> <br>
										
										
										
										
										<div class="col-sm-2"></div>
												<div class="col-sm-12"></div>
									</div>	
									
									<div class="submit-button" id="hideAll">
									<div class="col-sm-3"></div>
									<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="1" data-box-type="2"
											onclick="noti(this)">Approve</button>
											
											</div>
											<div class="col-sm-2" style="padding-top:14.5px">
											<P ><a class="show-popup" href="#" data-showpopup="3" ><input type="submit" name="radio" data-status-type="2" data-box-type="2" id="noc" value="Reject" onclick="noti(this)" class="submit btn btn-success btn-table"   style="background-color:#E01F27" /></a> </P>
											</div>
											<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="3" data-box-type="2"
											onclick="pending(this)">Submit</button>
											
											</div>
											<div class="col-sm-3"></div>
									</div>
								
								
							</div>
						</div>
					</form>
				</div>
				
				
				
				
				
				<div class="col-sm-2">
				
				</div>
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				
				
				<div class="col-sm-8 panel panel-default">
					<form class="form-horizontal" role="form">
						<div class="form-group " >
							<div class="panel-heading clearfix quickfix3 changeColor">
								<h4 class="form-heading"  onclick="displayFormContent(this)" data-form-type='3'>IT Department</h4>
								 <span class="glyphicon editIcon glyphicon-pencil" style="float:right" onclick="edit(this)" data-box-type="3">
								 </span>
							</div>
						</div>
						<div class="box" >
							<div  class="form-group required" >
								<div class="col-sm-2">
							
								</div>
								
								<div class="col-sm-10 custom-noc">
									<p>
									<div class="glyphicon glyphicon-ok tick "
										style="visibility: hidden"></div>
									</p>
									<p class="msg" style="color: green"></p>
									<br> 
									
									<label class="col-sm-7">
									
									<input class="boxchild" type="checkbox"  name="vehicle" value="1" >Network
											de-registration
									  </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre> 
									  <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="4" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
									  <pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br>
									
									
									  <label class="col-sm-7">
									  <input class="boxchild" type="checkbox"  name="vehicle" value="2"> Removal of mail id
                                       </label><pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a>  </pre> 
						               <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="4" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre> <br>
										
										
										<label class="col-sm-7">
										<input class="boxchild" type="checkbox"  name="vehicle" value="3">Return of hardware /assets
										</label>
										<pre class="pre custom col-sm-3 noti"style="color:green"><span class="glyphicon glyphicon-ok-sign"></span> Approved by Anu <a class="show-popup" href="#" data-showpopup="1" ><span style="color:green" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
									   <pre class="pre custom col-sm-3 reject"style="color:red"><span class="glyphicon glyphicon-remove-sign"></span> Rejected by Anu <a class="show-popup" href="#" data-showpopup="4" ><span style="color:red" class="glyphicon glyphicon-comment submit btn commentIcon btn-success comment btn-table"></span></a></pre>
										<pre class="pre custom col-sm-3 pending"style="color:blue"><span class="glyphicon glyphicon-alert"></span> Pending</pre><br>
										
										
										
										<div class="col-sm-2"></div>
												<div class="col-sm-12"></div>
									</div>	
									
									<div class="submit-button" id="hideAll">
									<div class="col-sm-3"></div>
									<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="1" data-box-type="3"
											onclick="noti(this)">Approve</button>
											
											</div>
											<div class="col-sm-2" style="padding-top:14.5px">
											<P ><a class="show-popup" href="#" data-showpopup="4" ><input type="submit" name="radio" data-status-type="2" data-box-type="3" id="noc" value="Reject" onclick="noti(this)" class="submit btn btn-success btn-table"   style="background-color:#E01F27" /></a> </P>
											</div>
											<div class="col-sm-2">
										<button id="noc" class="submit" type="button" data-status-type="3" data-box-type="3"
											onclick="pending(this)">Submit</button>
											
											</div>
											<div class="col-sm-3"></div>
									</div>
								
								
							</div>
						</div>
					</form>
				</div>
				
				
				<div class="col-sm-2">
				
				</div>
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-8 panel panel-default">
					<form class="form-horizontal" role="form" name="formname">
						<div class="form-group ">
							<div class="panel-heading clearfix quickfix3">
								<h4 class="form-heading"  onclick="displayFormContent(this)" data-form-type='4'>HR Department</h4>
								
							</div>
						</div>
						<div class="form-group required">
						<div class="box">
							<div class="col-sm-12">
								
									<p>
									<div class="glyphicon glyphicon-ok tick "
										style="visibility: hidden"></div>
									</p>
									<p class="msg" style="color: green"></p>
									<br> <label class="control-label boxchild " for="designation">No.
										of days salary to be paid</label><br> <input type="text"
										class="form-control" id="designation1" required="required">
									<br> <label class="control-label boxchild" for="designation">No.
										of days of salary to be deducted against notice period </label><br>
									<input type="text" class="form-control" id="designation"
										required="required"> <br> <label
										class="control-label boxchild " for="designation">Any other
										earnings/deductions</label> <input type="text" class="form-control"
										id="designation" required="required">
								</div>
							
							<div class="submit-button">
								<button id="noc" class="submit" type="button"
									onclick="">Submit</button>
							</div>
						</div>	
						</div>
						<div class="col-sm-3"></div>
					</form>
				</div>
				<div class="col-sm-2">
				
				</div>
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-8 panel panel-default">
					<form class="form-horizontal" role="form">
						<div class="form-group ">
							<div class="panel-heading clearfix quickfix3">
								<h4 class="form-heading"  onclick="displayFormContent(this)" data-form-type='5'>Accounts Department</h4>
								
							</div>
						</div>
						<div class="form-group required">
							<div class="col-sm-12">
								<div class="box">
									<div class="col-sm-6">
										<p>
										<div class="glyphicon glyphicon-ok tick "
											style="visibility: hidden"></div>
										</p>
										<p class="msg" style="color: green"></p>
										<br> <label for="designation">Earnings:</label><br>
										<label class="control-label boxchild " for="designation">Salary</label><br>
										<input type="text" class="form-control" id="designation"
											required="required"> <br> <label
											class="control-label boxchild " for="designation">EL
											encashment</label><br> <input type="text" class="form-control"
											id="designation" value="" required="required"> <br>
										<label class="control-label boxchild " for="designation">Other
											Earnings</label><br> <input type="text" class="form-control"
											id="designation" required="required"> <br> <label
											class="control-label boxchild " for="designation">Other
											Deductions</label><br> <input type="text" class="form-control"
											id="designation" required="required"> <br> <label
											class="control-label boxchild " for="designation">Total</label><br>
										<input type="text" class="form-control" id="designation"
											required="required"> <br> <label
											class="control-label boxchild " for="designation">Net Payable</label><br>
										<input type="text" class="form-control" id="designation"
											required="required"> <br>
										<div class="col-sm-12"></div>
										<div class="col-sm-5"></div>
										<div class="col-sm-2">
											<div class="submit-button">
												<button id="noc" class="submit" type="button"
													onclick="disableCheck('1','5')">Submit</button>
											</div>
										</div>
										<div class="col-sm-5"></div>
									</div>
									<div class="col-sm-6 noc_custom">
										<label for="designation">Deductions:</label><br> <label
											class="control-label boxchild " for="designation">PF</label><br>
										<input type="text" class="form-control" id="designation"
											required="required"> <br> <label
											class="control-label boxchild " for="designation">TDS</label><br>
										<input type="text" class="form-control" id="designation"
											required="required"> <br> <label
											class="control-label boxchild " for="designation">Notice
											Period</label><br> <input type="text" class="form-control"
											id="designation" required="required"> <br> <label
											class="control-label boxchild " for="designation">Total</label><br>
										<input type="text" class="form-control" id="designation"
											required="required"> <br>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-2">
				
				</div>
			</div>
		</div>
		<div class="col-sm-2">
		
		</div>
		<div class="col-sm-12"></div>
		<div class="col-sm-2"></div>
		<div class="col-sm-4">
			<div class="input_fields_wrap"></div>
		</div>
		<div class="col-sm-2"></div>
	</div>
	</div>
	
<div class="overlay-bg">
</div>
	
<div class="overlay-content popup1 commentpopup">
     <form action="" method="post">
         <div class="col-sm-3">
         </div>
             <div class="col-sm-6 COMMENT">
                   <textarea  name="comments" style="font-size:15px;height:100px;width:300px" id="comments" class="commentbox">
				   </textarea>
                       <div style="padding-left:105px">
                            <button id="noc" class="submit" type="button" onclick="abc(this)" data-overlay-type="0">Submit</button>
      
                        </div>
             </div>
	  </form>
</div>

<div class="overlay-content popup2 commentpopup">
		<form action="" method="post">
		<div class="col-sm-3">
		</div>
<div class="col-sm-6 COMMENT">
<textarea  name="comments" style="font-size:15px;height:100px;width:300px" id="comments" class="commentbox">

</textarea>
<div style="padding-left:105px">
<button id="noc" class="submit" type="button" onclick="abc(this)" data-overlay-type="1">Submit</button>


</div>
</div>
</form>
</div>

<div class="overlay-content popup3 commentpopup">
		<form action="" method="post">
		<div class="col-sm-3">
		</div>
<div class="col-sm-6 COMMENT">
<textarea  name="comments" style="font-size:15px;height:100px;width:300px" id="comments" class="commentbox">

</textarea>
<div style="padding-left:105px">
<button id="noc" class="submit" type="button" onclick="abc(this)" data-overlay-type="2">Submit</button>


</div>
</div>
</form>
</div>

<div class="overlay-content popup4 commentpopup">
		<form action="" method="post">
		<div class="col-sm-3">
		</div>
<div class="col-sm-6 COMMENT">
<textarea  name="comments" style="font-size:15px;height:100px;width:300px" id="comments" class="commentbox">

</textarea>
<div style="padding-left:105px">
<button id="noc" class="submit" type="button" onclick="abc(this)" data-overlay-type="3">Submit</button>


</div>
</div>
</form>
</div>

<div id="footer"></div>
 
                
                

</body>
</html>
