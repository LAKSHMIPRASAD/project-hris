<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Profile | Employment</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/mainHeader.css" rel="stylesheet">
 <link href="css/responsive.css" rel="stylesheet">      
<link href="css/form.css" rel="stylesheet">
<link href="css/process_bar.css" rel="stylesheet">
<link href="css/overlaypopup.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" src="js/reimbursement_append.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="external/jquery/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>
</head>
<body>

 <header id="header">
                    <nav class="navbar navbar-inverse" role="banner">
                        <div class="container">
                        
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.html"></a>
                                 <div id="mcfadyen">
                                    <img src="images/logo.gif" id="logo1" alt="logo1">



                                    <div id="leftones">
                                    <div id="welcome">
                                          <font color="white"><br>Welcome Shriya Shekhar| E297</font>
                                    </div>
                                 
                                    <ul class="nav navbar-nav">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Profile</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="profile-general-HR perspective.html">General Details</a></li>
                                                <li><a href="profile-employment-HR perspective.html">Employment Details</a></li>
                                                <li><a href="profile-passport.html">Passport and Visa Details</a></li>
                                                <li><a href="document-upload-HR perspective.html">Document Upload</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Finance</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="Reimbursement.html">Reimbursement Form</a></li>
                                                <li><a href="endorsement.html">Endorsement Form</a></li>
                                                <li><a href="form-certification-HR perspective.html">Certification Form</a></li>
                                                <li><a href="#">Salary Certificate</a></li>
                                                <li><a href="reimbursement_track.html">Reimbursement Track</a></li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">NAME TBD</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Releaving Formalities</a>
                                                <ul>
                                                     <li><a href="form-exit interview.html">&nbsp &nbsp Exit Form</a></li>
                                                     <li><a href="noc.html">&nbsp &nbsp NOC Form</a></li>
                                                    
                                                </ul>
                                                </li>

                                                <li><a href="#">Proof of Employment</a>
                                                
                                                </li>
                                                <li><a href="#">Proof of Address</a>
                                                
                                                </li>
                                                
                                            </ul>
                                        </li>

                                       
                                        <li><a href="#">TimeSheet</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">&nbsp &nbsp Standard Report</a>
                                                <ul>
                                                     <li><a href="report.html">&nbsp &nbsp HR Dashboard</a></li>
                                                     <li><a href="seperation.html">&nbsp &nbsp Attrition Report</a></li>
                                                    <li><a href="gender.html">&nbsp &nbsp Joiner Gender Ratio</a></li>
                                                     <li><a href="distribution.html"> &nbsp &nbsp Employee Distribution</a></li>
                                                    
                                                </ul>
                                                </li>

                                                <li><a href="#">Custom Report</a>
                                                <ul>
                                                 <li><a href="#">&nbsp &nbsp MyReport#1</a></li>
                                                <li><a href="#">&nbsp &nbsp MyReport#2</a></li>
                                                <li><a href="#">&nbsp &nbsp MyReport#3</a></li>
                                                 </ul>
                                                </li>
                                                <li><a href="#">Adhoc Report</a>
                                                <ul>
                                                <li><a href="adhoc.html">&nbsp &nbsp Generate New</a></li>
            
                                                 </ul>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Assests</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="Laptop.html"> Laptop</a>
                                                <li><a href="desktop.html"> Desktop</a>
                                                <li><a href="monitor.html"> Monitor</a>
                                                <li><a href="server1.html"> Server</a>
                                                <li><a href="storages.html"> Storages</a>
                                                <li><a href="printers.html"> Printer</a>
                                                <li><a href="#"> Network Equipment</a>
                                                <ul>
                                                     <li><a href="routers.html">&nbsp &nbsp Router</a></li>
                                                     <li><a href="switches.html">&nbsp &nbsp Switches</a></li>
                                                    <li><a href="firewalls.html">&nbsp &nbsp Firewall</a></li>
                                                     <li><a href="wifi_aps.html"> &nbsp &nbsp Wifi Aps</a></li>
                                                    
                                                </ul>
                                                </li>

                                                <li><a href="#">VC Unit</a>
                                                <ul>
                                                 <li><a href="vonage-storages.html">&nbsp &nbsp Vonage Router</a></li>
                                                <li><a href="polycom-phones">&nbsp &nbsp Polycom</a></li>
                                                <li><a href="#">&nbsp &nbsp Other Devices</a></li>
                                                 </ul>
                                                </li>
                                                <li><a href="#">Software</a>
                                                <ul>
                                                <li><a href="operating_system.html.html">&nbsp &nbsp Operating System</a></li>
                                                <li><a href="applications.html">&nbsp &nbsp Application</a></li>
            
                                                 </ul>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                         
                                        <li class="dropdown">
                                            <a href="policy.html" class="dropdown-toggle" data-toggle="dropdown">Policy Centre</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="navoff.html">Work Culture and Env. Policies</a></li>
                                                <li><a href="condofemp.html">Condition of Employment</a></li>
                                                <li><a href="payroll.html">Payroll and Employee benifits</a></li>
                                                <li><a href="leave.html">Leave Policies</a></li>
                                                <li><a href="relocation.html">Relocation Policy</a></li>
                                                <li><a href="#">Travel Policy</a></li>
                                                <li><a href="#">Performance appraisal</a></li>
                                                <li><a href="#">Discipline and misconduct</a></li>
                                                <li><a href="#">Dress code policy</a></li>
                                                <li><a href="#">General Policy</a></li>
                                                
                                            </ul>
                                        </li>
                                                               
                                    </ul>

                                    </div>
                                

                                    <div id="righttabs">
                                    
                                    <div id="personal">
                                     <div id="display">
                                        <img src="images/imagepic.jpg" id="logo2" alt="logo2" class="img-circle">
                                     </div>
                                     <div id="widgets">
                                        <ul class="nav navbar-nav">
                                       
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-th"></span></a>
                                            <ul class="dropdown-menu">
            
                                                <li> <label><input type="checkbox"  data-widget-name="js-widget-A" class="widget" checked/> &nbsp;Widget A</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-B" class="widget" checked/> &nbsp;Widget B</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-C" class="widget" checked/> &nbsp;Widget C</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-D" class="widget" checked/>&nbsp; Widget D</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-E" class="widget" checked/> &nbsp;Widget E</label></li>
                                                    <li><label><input type="checkbox"  data-widget-name="js-widget-F" class="widget" checked/> &nbsp;Widget F</label></li>
                                           
                                            </ul>
                                         </li>
                                         <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="blog-item.html">Item 1</a></li>
                                                <li><a href="pricing.html">Item 2</a></li>
                                                <li><a href="404.html">Item 3</a></li>
                                                <li><a href="shortcodes.html">Item 4</a></li>
                                            </ul>
                                        </li>
                                                               
                                        </ul>
                                    </div>
                                    </div>
                                          
                            </div>

                                 </div>
                        </div><!--/.container-->
                    </nav><!--/nav-->
                    
                </header><!--/header-->
	<div class="container-floating">
		
		
		<div class="form-entire">
			<form class="form-horizontal" role="form">
				<div class="col-sm-2"></div>
				<div class="col-sm-8 panel panel-default">
					<div class="form-group ">
						<div class="panel-heading clearfix quickfix3">
							<h4 class="form-heading">Reimbursement tracking</h4>
						</div>
					</div>

				 <div class="form-group required">
				 <div class="col-sm-6">
				 <label class="control-label col-sm-4 " for="designation">Date from</label>
              <div class="col-sm-6">
                <input type="date" class="form-control" id="designation"
                  required="required">
              </div>
			  </div>
			   <div class="col-sm-6">
				 <label class="control-label col-sm-4 " for="designation">Date to</label>
              <div class="col-sm-6">
                <input type="date" class="form-control" id="designation"
                  required="required">
              </div>
			  </div>
			  
             
              </div>
            </div>
					
				<div class="col-sm-2"></div>
				<div class="col-sm-12"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-3">
					
				</div>
				<div class="col-sm-2">
					<div class="submit-button button_search">
						<button class="submit " type="button" onclick="tableVisibility()">Search</button>
					</div>
				</div>
				<div class="col-sm-3">
					
				</div>
				<div class="col-sm-2"></div>
			</form>
			</div>
		<div class="col-sm-12"></div>	
		<div class="col-sm-12"></div>	
		<div class="col-sm-12"></div>	
		<div class="col-sm-2"></div>
		<div id="track_table" >
				<div class="col-sm-8 panel panel-default col-sm-8 custom">
					<div class="form-group">
						<div class="panel-heading clearfix quickfix3">
							<h4 class="form-heading">Reimbursement Details</h4>
						</div>
					</div>
					
					<table class="table table-hover table track" style="width:100%" >
  <tr>
    <th>Form Category</th>
	 <th>Date</th>
    <th>Bill No</th>  
	<th>Bill </th>  
    <th>Amount</th>
	<th>Status</th>
	<th>Track</th>
 </tr>
  <tr>
    <td>medical</td>
	 <td>2015-12-22</td>
    <td>1234</td>  
    <td></td>
	<td>3456</td>
	<td>Pending with finance</td>
	<td><a class="show-popup" href="#" data-showpopup="1" ><input type="submit" name="radio" id="radio" value="View Status" class="btn btn-success btn-table" style="background-color:#E01F27"/></a></td>
  </tr>
  
  <tr>
    <td>medical</td>
	 <td>2015-12-22</td>
    <td>1234</td>  
    <td></td>
	<td>3456</td>
	<td>Pending with finance</td>
	<td><P>  <a class="show-popup" href="#" data-showpopup="1" ><input type="submit" name="radio" id="radio" value="View Status" class="btn btn-success btn-table" style="background-color:#E01F27"/></a>  </P></td>
  </tr>
   <tr>
    <td>medical</td>
	 <td>2015-12-22</td>
    <td>1234</td>  
    <td></td>
	<td>3456</td>
	<td>Pending with finance</td>
	<td> <P><a class="show-popup" href="#" data-showpopup="1" ><input type="submit" name="radio" id="radio" value="View Status" class="btn btn-success btn-table"style="background-color:#E01F27" /></a> </P></td>
  </tr>
</table>
</div>

				 
            </div>
			<div class="col-sm-2"></div>
			
	</div>
	<div class="overlay-bg">
</div>
	
		<div class="overlay-content popup1">
		
   <ol class="progress progress custom" data-steps="4">
    
    <li class="done">
        <span class="name">Reporting Manager<br>
		Date:21/12/2015<br>
		Time:11:45am</span>
        <span class="step"><span>1</span></span><br>
		
	
    </li>
    <li class="done">
        <span class="name">Manager<br>
		Date:24/12/2015<br>
		Time:01:45pm</span>
        <span class="step"><span>2</span></span>
    </li>
    <li class="active">
        <span class="name">Finance<br>
		<br>
		<br></span>
        <span class="step"><span>3</span></span>
    </li>
	<li class="">
        <span class="name">Done<br>
		<br>
		<br></span>
        <span class="step"><span>4</span></span>
    </li>
</ol>
    
	
	
	
	
	

   
</div>

<footer id="footer" class="midnight-blue">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                &copy; 2015 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">McFadyen Consulting Solutions</a>. All Rights Reserved.
                            </div>
                            <div class="col-sm-6">
                                <ul class="pull-right">
                                    <li><a href="#">Help</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Faq</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </footer><!--/#footer-->
</body>
</html>