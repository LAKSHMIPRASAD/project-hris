package com.db.manager;

public class Pojo implements java.io.Serializable {
	private String a;
	private String b;
	private String c;
	private Pojo pojo;
	
	public Pojo getPojo() {
		return pojo;
	}
	public void setPojo(Pojo pojo) {
		System.out.println(this);
		this.pojo = pojo;
	}
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	
	public void method(){
		
	}
	
	
}
