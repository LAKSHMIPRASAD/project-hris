package com.db.manager;import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;



public class DBConnection {
	
	public String ArrayListToCSV(ArrayList<String> al){
		String s = "";
		int len=0;
		for(int i=0;i<al.size();i++){
			s+=al.get(i)+",";
		}
		s=s.substring(0, s.length()-1);
		return s;
		
//		return null;
	}
	
	
	public Map getCriterias() {
	
		return null;
	}
	public ArrayList<String> getSelectParameters(){
		ArrayList selectParameters=new ArrayList<>();
		selectParameters.add("emp_id");
		selectParameters.add("first_name");
		selectParameters.add("current_address_id");
		
		return selectParameters;
	}

	public static void main(String[] args) throws SQLException {
		Statement st=null;
		Connection con = null;
		ResultSet rs=null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hris", "root", "root");

		} catch (Exception err) {

			err.printStackTrace();
		}
		DBConnection db=new DBConnection();
		ArrayList selectParameters=db.getSelectParameters();
		StringBuilder sb=new StringBuilder();
//		sb=db.ArrayListToCSV(selectParameters);
		
		String s=db.ArrayListToCSV(selectParameters);
//		System.out.println(s);
		
		QueryCriteria criteriaOne= new QueryCriteria("emp_id", "=","E 100");
		QueryCriteria criteriaTwo=new QueryCriteria("emp_id", "=", "E 140");
		QueryCriteria criteriaThree=new QueryCriteria("first_name", "=", "Lakshmiprasad");
		
		st = con.createStatement();
//		System.out.println("SELECT "+s+" FROM personal_details where "+criteriaOne);
		rs = st.executeQuery("SELECT "+s+" FROM personal_details where "+criteriaOne.or(criteriaThree.or(criteriaTwo)));
		System.out.println("SELECT "+s+" FROM personal_details where "+criteriaOne.or(criteriaThree.or(criteriaTwo)));
		
		while(rs.next()){
			System.out.println(rs.getString("emp_id"));
			System.out.println(rs.getString("first_name"));
			System.out.println(rs.getString("current_address_id"));
		}
	}
}
