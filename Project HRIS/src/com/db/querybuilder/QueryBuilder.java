package com.db.querybuilder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.db.manager.QueryCriteria;

public class QueryBuilder {
	private String query;
	private ResultSet resultSet;
	private QueryCriteria queryCriteria;
    private	List selectParameters;
    static int criteria=0;
	public String getQuery() {
		return query;
	}
	public void setQuery() {
		this.query = "select "+this.ListToCSV(this.getSelectParameters())+" from adhoc_report";
		if(criteria==1){
			
			this.query+=" where "+this.queryCriteria.toString();
			criteria=10;
		}
		if(criteria!=1)this.query+=";";
	}
	public ResultSet getResultSet() throws SQLException {
		 Statement st=null;
		 Connection con = null;
		 ResultSet rs=null;
		 
		 try {
			 Class.forName("com.mysql.jdbc.Driver").newInstance();
			 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hris", "root", "root");
			 st = con.createStatement();
			 rs = st.executeQuery(this.query);
		} catch (Exception err) {

			err.printStackTrace();
		}
		
		return rs;
	}
	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}
	public QueryCriteria getQueryCriteria() {
		return queryCriteria;
	}
	public void setQueryCriteria(QueryCriteria queryCriteria) {
		criteria=1;
		this.queryCriteria = queryCriteria;
	}
	public List getSelectParameters() {
		return selectParameters;
	}
	public void setSelectParameters(List selectParameters) {
		this.selectParameters = selectParameters;
	}
	public String ListToCSV(List<String> al){
		String s = "";
		int len=0;
		for(int i=0;i<al.size();i++){
			s+=al.get(i)+",";
		}
		s=s.substring(0, s.length()-1);
		return s;
		
//		return null;
	}
	
	
}
