package com.mcfadyen.hris.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mcfadyen.hris.beans.EmploymentBean;
import com.mcfadyen.hris.manager.ManagerHRIS;

/**
 * Servlet implementation class EmploymentController
 */
@WebServlet("/EmploymentController")
public class EmploymentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmploymentController() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
		EmploymentBean employmentBean = new EmploymentBean();
		
		employmentBean.setCompanyName(request.getParameterValues("companyName"));
		employmentBean.setDesignation(request.getParameterValues("designation"));
		employmentBean.setDateOfJoining(request.getParameterValues("dateOfJoining"));
		employmentBean.setDateOfRelieving(request.getParameterValues("dateOfRelieving"));
		employmentBean.setAddressLineOne(request.getParameterValues("addressLineOne"));
		employmentBean.setAddressLineTwo(request.getParameterValues("addressLineTwo"));
		employmentBean.setCity(request.getParameterValues("city"));
		employmentBean.setState(request.getParameterValues("state"));
		employmentBean.setCountry(request.getParameterValues("country"));
		employmentBean.setPinCode(request.getParameterValues("pinCode"));
		employmentBean.setDepartmentName(request.getParameter("departmentName"));
		employmentBean.setManagerName(request.getParameter("managerName"));
		employmentBean.setComments(request.getParameter("comments"));
		
		ManagerHRIS manager = new ManagerHRIS();
		
		manager.EmploymentDetails(employmentBean);
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("My Profile/profile-employment.jsp");
		dispatcher.forward(request, response);

		// response.sendRedirect("loginerror.jsp");

	}
}
