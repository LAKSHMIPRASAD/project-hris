package com.mcfadyen.hris.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.mcfadyen.hris.beans.DocumentBean;

/**
 * Servlet implementation class DocumentController
 */
@WebServlet("/DocumentController")
@MultipartConfig
public class DocumentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	
	Connection con = null;
	Date date = new Date();
	
	
	
	
    public DocumentController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");  
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/hris","root","root");
			
			
			List<DocumentBean> documentBeanList = populateDocumentBean(request);
			insertDocuments(documentBeanList);
			
			
		}
		catch(SQLException | ClassNotFoundException e){
			e.printStackTrace();
			}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("My Profile/document-upload.jsp");
		dispatcher.forward(request, response);
		
	}
	
	private void insertDocuments(List<DocumentBean> documentBeanList) throws SQLException, IOException {
		// TODO insert into db
		
		int autoGenKey = -1;
		String insertDocuments = "INSERT INTO emp_doc_map (emp_id,doc_id,created_by,created_on) values (?,?,?,?)";
		String insertEncodedDocuments = "INSERT INTO emp_doc_encoded (emp_doc_map_id, doc_id, document, created_by, created_on) values (?,?,?,?,?)";
		
		PreparedStatement document = con.prepareStatement(insertDocuments, Statement.RETURN_GENERATED_KEYS);
		PreparedStatement encodedDocument = con.prepareStatement(insertEncodedDocuments);
		
		for(DocumentBean documentBean:documentBeanList){
			document.setString(1, "e 201");
			document.setInt(2, documentBean.getId());
			document.setString(3, null);
			document.setTimestamp(4, new Timestamp(date.getTime()));
			document.executeUpdate();
			
			ResultSet rs = document.getGeneratedKeys();
		       if(rs.next()){
		    	   autoGenKey = rs.getInt(1);
		       }
		       
	       encodedDocument.setInt(1, autoGenKey);
	       encodedDocument.setInt(2, documentBean.getId());
	       Part part = documentBean.getPart();
	       InputStream inputStream = part.getInputStream();
	       encodedDocument.setBlob(3, inputStream);
	       encodedDocument.setString(3, null);
	       encodedDocument.setTimestamp(4, new Timestamp(date.getTime()));
	       encodedDocument.executeUpdate();
		}
		

	}
	
	private List<DocumentBean> populateDocumentBean(HttpServletRequest request) throws SQLException, IllegalStateException, IOException, ServletException {
		// TODO fetch checklist from db and populate document bean
		
		
		List<DocumentBean> list = new ArrayList<>();
		String getChecklist = "SELECT * FROM checklist WHERE status = 1";
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(getChecklist);
		
		while(rs.next()){
			DocumentBean documentBean = new DocumentBean();
			String document = rs.getString("doc_type");
			int id = rs.getInt("doc_id");
			Part part = request.getPart(document);			
			documentBean.setId(id);
			documentBean.setType(document);
			documentBean.setPart(part);
			list.add(documentBean);
		}
		return list;

	}

}
