package com.mcfadyen.hris.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mcfadyen.hris.beans.EmploymentBean;
import com.mcfadyen.hris.beans.ExitFormBean;
import com.mcfadyen.hris.manager.ManagerHRIS;
import com.mcfadyen.hris.util.ReflectionUtil;

/**
 * Servlet implementation class ExitFormController
 */
@WebServlet("/ExitFormController")
public class ExitFormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExitFormController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ManagerHRIS manager = new ManagerHRIS();

		Object object = ReflectionUtil.CreateBean(ExitFormBean.class, request);
		ExitFormBean bean = (ExitFormBean) object;
		
		manager.ExitDetails(bean);
		RequestDispatcher dispatcher = request.getRequestDispatcher("form-exit interview.jsp");
		dispatcher.forward(request, response);

		// response.sendRedirect("loginerror.jsp");

	}

}
