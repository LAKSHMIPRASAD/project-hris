package com.mcfadyen.hris.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.mcfadyen.hris.beans.PassportVisaBean;
import com.mcfadyen.hris.manager.ManagerHRIS;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/passport")
@MultipartConfig


public class PassportVisaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PassportVisaController() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		PassportVisaBean passportBean = new PassportVisaBean();
		
		passportBean.setPassportNumber(request.getParameter("passportNumber"));
		passportBean.setDateOfPassportIssue(request.getParameter("dateOfPassportIssue"));
		passportBean.setDateOfPassportExpiry(request.getParameter("dateOfPassportExpiry"));
		passportBean.setPassport(request.getPart("passport"));
		passportBean.setVisaType(request.getParameterValues("visaType"));
		passportBean.setCountry(request.getParameterValues("country"));
		passportBean.setDateOfVisaExpiry(request.getParameterValues("dateOfVisaExpiry"));
		passportBean.setVisa((List<Part>) request.getParts());
		
		ManagerHRIS manager = new ManagerHRIS();
	
		manager.passportDetails(passportBean);
		
		// ProfileBean display=ob.personalDetailsDisplay(displayBean);
		// response.sendRedirect("loginsuccess.jsp");
		// request.setAttribute("displayBean", display );
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("My Profile/profile-passport.jsp");
		dispatcher.forward(request, response);

		// response.sendRedirect("loginerror.jsp");

	}


}
