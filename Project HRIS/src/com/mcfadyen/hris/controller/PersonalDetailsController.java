package com.mcfadyen.hris.controller;




import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mcfadyen.hris.beans.*;

import com.mcfadyen.hris.manager.PersonalDetailsManager;
import com.mcfadyen.hris.util.ReflectionUtil;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/My Profile/profile.do" )

public class PersonalDetailsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
     public PersonalDetailsController() {
		// TODO Auto-generated constructor stub
 
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
     
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// TODO Auto-generated method stub
		
		PersonalDetailsManager ob=new PersonalDetailsManager();
		
		Object object=ReflectionUtil.CreateBean(ProfileBean.class, request);
		ProfileBean bean=(ProfileBean) object;
		
		//ProfileBean bean=new ProfileBean();
		//ProfileBean displayBean=new ProfileBean();
		
		// TODO Auto-generated method stub
//		bean.setEmployeeId(request.getParameter("employee-number"));
//		bean.setAadharId(request.getParameter("aadhar-id"));
//		bean.setPanNumber(request.getParameter("pan-card")); 
//		bean.setFirstName(request.getParameter("first-name"));
//		bean.setMiddleName(request.getParameter("middle-name"));
//		bean.setLastName(request.getParameter("last-name"));
//		bean.setUan(request.getParameter("UAN"));
//		bean.setGender(request.getParameter("optradio"));
//		//bean.setDob(Date(request.getParameter("dob")));
//		bean.setPhoneNumber1(request.getParameter("phone-number-1"));
//		bean.setPhoneNumber2(request.getParameter("phone-number-2"));
//		bean.setEmailId(request.getParameter("email"));
//		bean.setMessenger(request.getParameter("messenger-type"));//add the name in the jsp..
//		bean.setMessenger_Id(request.getParameter("messenger"));
//		
//		bean.setPermanentAddressLine1(request.getParameter("permanent-address-line-1"));
//		bean.setPermanentAddressLine2(request.getParameter("permanent-address-line-2"));
//		bean.setPermanentAddressCity(request.getParameter("permanent-address-city"));
//		bean.setPermanentAddressState(request.getParameter("permanent-address-state"));
//		bean.setPermanentAddressCountry(request.getParameter("permanent-address-country"));
//		bean.setPermanentAddresspincode(request.getParameter("permanent-address-pin"));
//		
//		bean.setCurrentAddressLine1(request.getParameter("current-address-line-1"));
//		bean.setCurrenttAddressLine2(request.getParameter("currentt-address-line-2"));
//		bean.setCurrentAddressCity(request.getParameter("current-address-city"));
//		bean.setCurrentAddressState(request.getParameter("current-address-state"));
//		bean.setCurrentAddressCountry(request.getParameter("currentt-address-country"));
//		bean.setCurrentAddresspincode(request.getParameter("current-address-pin"));
//		
//		bean.setEmergencyPersonName(request.getParameter("emergency-contact-person"));
//		bean.setEmergencyPersonPhoneNumber(request.getParameter("emergency-relation"));
//		bean.setEmergencyPersonRelationship(request.getParameter("energency-contact-number"));//spell
		
		ob.personaldetails(bean);
		
		//ProfileBean display=ob.personalDetailsDisplay(displayBean);
		
		
		
//			response.sendRedirect("loginsuccess.jsp");
		  //  request.setAttribute("displayBean", display ); 
			RequestDispatcher dispatcher = request.getRequestDispatcher("profile-general-HR perspective.jsp");
			dispatcher.forward(request, response);
		
		 
			//response.sendRedirect("loginerror.jsp");
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

