package com.mcfadyen.hris.controller;




import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.mcfadyen.hris.beans.*;
import com.mcfadyen.hris.manager.ModeManager;
import com.mcfadyen.hris.manager.PersonalDetailsManager;
import com.mcfadyen.hris.utils.ReflectionUtil;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/My Profile/getMode.do" )

public class ModeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String ROLE;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
     public ModeController() {
		// TODO Auto-generated constructor stub
 
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		// TODO Auto-generated method stub
		
		ProfileBean displayBean;
		
		ModeAndRoleBean modeBean=new ModeAndRoleBean();
		ModeAndRoleBean modeBeanResult;
		ModeManager ob=new ModeManager();
		modeBeanResult=ob.fetchMode(modeBean);
		
		ROLE=modeBeanResult.getRole();
		System.out.println(ROLE);
		
		displayBean=ob.displayEmployeeDetails(new ProfileBeanMode());	
		
		Gson gson = new Gson();

		// convert java object to JSON format,
		// and returned as JSON formatted string
		//String json = gson.toJson(modeBeanResult);
		 // response.setContentType("application/json");
	      //response.getWriter().print(json);
		String json = gson.toJson(displayBean);
		  response.setContentType("application/json");
	      response.getWriter().print(json);
	      
	     // session.setAttribute("displayBean", displayBean );       
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

