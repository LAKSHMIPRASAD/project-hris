package com.mcfadyen.hris.cryptography;

public class CryptographyFactory {
	public Cryptography getInstance(CryptographyOptions co)
	{
		if(co == CryptographyOptions.RSA){
			return new RSACryptography();
		}
		else if(co == CryptographyOptions.AES){
			return new AESCryptography();
		}
		else 
			return null;
	}
}
