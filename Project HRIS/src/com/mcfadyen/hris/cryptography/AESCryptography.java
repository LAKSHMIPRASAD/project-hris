package com.mcfadyen.hris.cryptography;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class AESCryptography implements Cryptography{
	/*public static void main(String[] argv) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
			NoSuchAlgorithmException, NoSuchPaddingException, IOException {

		KeyGenerator keygenerator = KeyGenerator.getInstance("AES");
		SecretKey key = keygenerator.generateKey();

	}
*/
	public byte[] encrypt(String DataFile, Key key) throws NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException, NoSuchAlgorithmException{
		Cipher cipher = Cipher.getInstance("AES");
		byte[] data = null;
		cipher.init(Cipher.ENCRYPT_MODE, (SecretKey)key);
		Path path = Paths.get(DataFile);
		data = Files.readAllBytes(path);
		byte[] encryptedData = cipher.doFinal(data);
		return encryptedData;
	}

	public byte[] encrypt(byte[] data, Key key) throws IllegalBlockSizeException, BadPaddingException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encryptedData = cipher.doFinal(data);
		return encryptedData;
	}

	/*public byte[] AESEncryption(File file, SecretKey key) throws IllegalBlockSizeException, BadPaddingException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
		byte[] encryptedData = AESEncryption(file.getAbsolutePath(), key);
		return encryptedData;
	}

	public byte[] AESEncryption(Path path, SecretKey key) throws IllegalBlockSizeException, BadPaddingException,
			InvalidKeyException, NoSuchAlgorithmException, IOException, NoSuchPaddingException {
		File file = path.toFile();
		byte[] encryptedData = AESEncryption(file.getAbsolutePath(), key);
		return encryptedData;
	}*/

	public byte[] decrypt(String DataFile, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, IOException {
		Cipher cipher = Cipher.getInstance("AES");
		byte[] data = null;
		cipher.init(Cipher.DECRYPT_MODE, key);
		Path path = Paths.get(DataFile);
		data = Files.readAllBytes(path);
		byte[] decryptedData = cipher.doFinal(data);
		return decryptedData;
	}

	public byte[] decrypt(byte[] data, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decryptedData = cipher.doFinal(data);
		return decryptedData;
	}


	/*public byte[] AESDecryption(File file, SecretKey key) throws IllegalBlockSizeException, BadPaddingException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IOException {
		byte[] decryptedData = AESDecryption(file.getAbsolutePath(), key);
		return decryptedData;
	}

	public byte[] AESDecryption(Path path, SecretKey key) throws IllegalBlockSizeException, BadPaddingException,
			InvalidKeyException, NoSuchAlgorithmException, IOException, NoSuchPaddingException {
		File file = path.toFile();
		byte[] decryptedData = AESDecryption(file.getAbsolutePath(), key);
		return decryptedData;
	}*/

}
