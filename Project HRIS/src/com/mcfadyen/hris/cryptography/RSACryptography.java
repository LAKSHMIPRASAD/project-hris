package com.mcfadyen.hris.cryptography;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

public class RSACryptography implements Cryptography {

	//long encryptedSize;
	//static PublicKey publicKey;
	//static PrivateKey privateKey;

	/*public static void main(String[] args) throws IOException, InvalidKeySpecException {

		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(2048);
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			publicKey = keyPair.getPublic();
			privateKey = keyPair.getPrivate();
			System.out.println("Public Key - " + publicKey);
			System.out.println("Private Key - " + privateKey);

			RSAEncryptionDecryptionFile rsaObj = new RSAEncryptionDecryptionFile();
			rsaObj.encryptData("C:\\Users\\msrinath\\Desktop\\pic.jpg", publicKey);
			rsaObj.decryptData("C:\\Users\\msrinath\\Desktop\\RSAEncrypted.jpg", privateKey);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}*/

	public byte[] encrypt(byte[] dataFile, Key publicKey)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		
		/*ByteArrayOutputStream fos = new ByteArrayOutputStream();
		fos.write(dataFile);
		fos.flush();
		fos.close();
		byte[] encryptedData = encrypt(file.getAbsolutePath(), publicKey);
		return encryptedData;*/
		
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		RSAPublicKeySpec rsaPubKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		int publicKeyLength = rsaPubKeySpec.getModulus().bitLength();
		int payloadLength = publicKeyLength / 8 - 11;

		byte[][] data = RSACryptography.getData(dataFile, payloadLength);
		int dataLength = data.length;
		byte[][] encryptedData = new byte[dataLength][];

		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);

			for (int i = 0; i < dataLength; i++) {
				encryptedData[i] = cipher.doFinal(data[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		for (byte b[] : encryptedData) {
			for (byte t : b)
				fos.write(t);
		}
		fos.flush();
		fos.close();

		return fos.toByteArray();
	}

	/*public static byte[] encryptData(Path path, Key publicKey)
			throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		File file = path.toFile();
		byte[] encryptedData = encryptData(file.getAbsolutePath(), publicKey);
		return encryptedData;
	}

	public static byte[] encryptData(File file, Key publicKey)
			throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		byte[] encryptedData = encryptData(file.getAbsolutePath(), publicKey);
		return encryptedData;
	}*/

	public byte[] encrypt(String dataFile, Key publicKey)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		Path path = Paths.get(dataFile);
		byte[] data = Files.readAllBytes(path);
		byte[] encryptedData = encrypt(data,publicKey);
		return encryptedData;
	}

	public byte[] decrypt(String dataFile, Key privateKey)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		
		Path path = Paths.get(dataFile);
		byte[] data = Files.readAllBytes(path);
		byte[] decryptedData = decrypt(data, privateKey);
		return decryptedData;
	}

	public byte[] decrypt(byte[] dataFile, Key privateKey)
			throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		
		byte[][] decryptedData = null;
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
		int privateKeyLength = rsaPrivKeySpec.getModulus().bitLength();
		int payloadLength = privateKeyLength / 8;
		byte[][] data = RSACryptography.getData(dataFile, payloadLength);

		int dataLength = data.length;
		decryptedData = new byte[dataLength][];

		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			for (int i = 0; i < dataLength; i++) {
				decryptedData[i] = cipher.doFinal(data[i]);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		for (byte b[] : decryptedData) {
			for (byte t : b)
				baos.write(t);
		}
		baos.flush();
		baos.close();

		return baos.toByteArray();
		
		
	}

	/*public static void decryptData(Path path, Key privateKey)
			throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		File file = path.toFile();
		decryptData(file.getAbsolutePath(), privateKey);
	}

	public static void decryptData(File file, Key privateKey)
			throws InvalidKeySpecException, NoSuchAlgorithmException, IOException {
		decryptData(file.getAbsolutePath(), privateKey);
	}*/

	private static byte[][] getData(byte[] data, int length) {

		byte[][] data2 = null;
		long dataSize = 0;
		try {
			dataSize = data.length;
			int numberOfParts = (int) dataSize / length;
			if (dataSize % length != 0) {
				numberOfParts++;
			}
			data2 = new byte[numberOfParts][length];
			int i = 0;
			int j = 0;
			for (byte b : data) {
				if (i == (length - 1)) {
					data2[j][i] = b;
					j++;
					i = 0;
				} else {
					data2[j][i] = b;
					i++;
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("It's happeneing in the file import");
		}
		return data2;
	}
}
