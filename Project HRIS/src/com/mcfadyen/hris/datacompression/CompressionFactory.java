package com.mcfadyen.hris.datacompression;

public class CompressionFactory {
	
	public CompressionDecompression getInstance(CompressionOptions co)
	{
		if(co == CompressionOptions.ZIP)
		{
			return new ZipCompression();
		}
		else if(co == CompressionOptions.GZIP)
		{
			return new GZipCompression();
		}
		else 
			return null;
	}
	

}
