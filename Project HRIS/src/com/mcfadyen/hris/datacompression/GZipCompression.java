package com.mcfadyen.hris.datacompression;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZipCompression implements CompressionDecompression {

	/*public static void main(String[] args) throws IOException {
		String file = "C:\\Users\\msrinath\\Downloads\\Sierra K., Bates B. - OCA OCP Java SE 7 Programmer I & II Study Guide - 2015.pdf";
		String gzipFile = "C:\\Users\\msrinath\\Downloads\\sierra.gz";
		String newFile = "C:\\Users\\msrinath\\Downloads\\Sierra K., Bates B. - OCA OCP Java SE 7 Programmer I & II Study Guide - 2015_new.pdf";

		compressGzipFile(file);

		decompressGzipFile(gzipFile);

	}*/
	
	/*public static String decompressGzipFile(File gzipFile) {
		String newFile = null;
		 newFile = decompressGzipFile(gzipFile.getAbsolutePath());
		 return newFile;
	}
	public static String decompressGzipFile(Path gzipFile) {
		String newFile = null;
		newFile = decompressGzipFile(gzipFile.toFile());
		return newFile;
	}*/
	public byte[] decompress(byte[] data) throws IOException {
		
			
			ByteArrayInputStream fis = new ByteArrayInputStream(data);
			GZIPInputStream gis = new GZIPInputStream(fis);
			ByteArrayOutputStream fos = new ByteArrayOutputStream();
			byte[] buffer = new byte[10000];
			int length;
			while ((length = gis.read(buffer)) != -1) {
				fos.write(buffer, 0, length);
			}
			fos.flush();
			fos.close();
			gis.close();
		
		return fos.toByteArray();
	}

	public byte[] decompress(String GzipFile) throws IOException {
		//String newFile = null;
		FileInputStream fis = new FileInputStream(GzipFile);
		byte[] data = new byte[100000];
		fis.read(data);
		fis.close();
		return data;
	}

	public byte[] compress(byte[] data) throws IOException {
		ByteArrayOutputStream fos = new ByteArrayOutputStream();
		GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
		gzipOS.write(data);
		fos.close();
		gzipOS.flush();
		gzipOS.close();
		return fos.toByteArray();
	}

	public byte[] compress(String file) throws IOException {
		
			byte[] data = new byte[100000];
			FileInputStream fis = new FileInputStream(file);
			fis.read(data);
			fis.close();
			byte[] compressedData = compress(data);
			return compressedData;
		
	}
	/*public static String compressGzipFile(File file) {
		String gzipFile = null;
		 gzipFile = decompressGzipFile(file.getAbsolutePath());
		 return gzipFile;
	}
	public static String compressGzipFile(Path file) {
		String gzipFile = null;
		gzipFile = decompressGzipFile(file.toFile());
		return gzipFile;
	}*/
	
}
