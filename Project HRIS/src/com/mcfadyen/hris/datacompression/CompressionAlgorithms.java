package com.mcfadyen.hris.datacompression;

public enum CompressionAlgorithms {
		ZIP,
		GZIP
};