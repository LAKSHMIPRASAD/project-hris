package com.mcfadyen.hris.datacompression;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ZipCompression implements CompressionDecompression {
	
	  /*public static void main(String[] args) throws IOException,
	  DataFormatException { Path file = Paths.get("C:\\Users\\msrinath\\Downloads\\Sierra K., Bates B. - OCA OCP Java SE 7 Programmer I & II Study Guide - 2015.pdf"); 
	  Path zipFile = Paths.get("C:\\Users\\msrinath\\Downloads\\sierra.zip"); 
	  String newFile = "C:\\Users\\msrinath\\Downloads\\sierra_new.pdf";
	  ZipCompression z = new ZipCompression();
	  z.compress(Files.readAllBytes(file), Deflater.BEST_COMPRESSION);
	  z.decompress(Files.readAllBytes(zipFile)); 
	  }*/
	 

	public byte[] compress(String fileName, int degreeOfCompression) throws IOException {
		Path path = Paths.get(fileName);
		byte[] zipData = compress(Files.readAllBytes(path), degreeOfCompression);
		return zipData;
	}

	public byte[] compress(byte[] data, int degreeOfCompression) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		Deflater deflater = new Deflater(degreeOfCompression);
		deflater.setInput(data);
		deflater.finish(); // Indicates that compression should end with currenty
							// contents of the input buffer i.e. null
		byte[] buffer = new byte[100000];
		int count=0;
		while (!deflater.finished()) {
			count = deflater.deflate(buffer);
			
		}
		outputStream.write(buffer, 0, count);
		outputStream.flush();
		outputStream.close();
		deflater.end();
		return outputStream.toByteArray();
	}

	public byte[] compress(byte[] data) throws IOException {
		byte[] zipData = null;
		zipData = compress(data, Deflater.DEFAULT_COMPRESSION);
		return zipData;
	}

	public byte[] compress(String fileName) throws IOException {
		byte[] zipData = null;
		Path path = Paths.get(fileName);
		zipData = compress(Files.readAllBytes(path), 7);
		return zipData;
	}

	public byte[] decompress(byte[] data) throws IOException, DataFormatException {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[100000];
		while (!inflater.finished()) {
			int count = inflater.inflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		outputStream.flush();
		outputStream.close();
		inflater.end();
		return outputStream.toByteArray();

	}

	public byte[] decompress(String zipFile) throws IOException, DataFormatException {
		Path path = Paths.get(zipFile);
		byte[] decompressedData = decompress(Files.readAllBytes(path));
		return decompressedData;
	}
}
