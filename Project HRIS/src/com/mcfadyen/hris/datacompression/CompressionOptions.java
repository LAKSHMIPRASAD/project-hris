package com.mcfadyen.hris.datacompression;

public enum CompressionOptions {
		ZIP,
		GZIP
};