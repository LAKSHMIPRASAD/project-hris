package com.mcfadyen.hris.datacompression;

import java.io.IOException;
import java.util.zip.DataFormatException;

public interface CompressionDecompression {

	public byte[] compress(byte[] data) throws IOException;
	
	public byte[] decompress(byte[] data) throws IOException, DataFormatException;
	
	public byte[] compress(String filePath) throws IOException;
	
	public byte[] decompress(String filePath) throws IOException, DataFormatException;
	
}
