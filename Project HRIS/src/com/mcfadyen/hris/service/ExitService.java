package com.mcfadyen.hris.service;

import com.mcfadyen.hris.beans.ExitFormBean;
import com.mcfadyen.hris.dao.Dao;
import com.mcfadyen.hris.daoimplementation.ExitDaoImplementation;

public class ExitService {

	public void exitDAOCall(ExitFormBean bean) {
		
		Dao exitDAOImplementation = new ExitDaoImplementation();
		exitDAOImplementation.insert(bean);
		
	}

}
