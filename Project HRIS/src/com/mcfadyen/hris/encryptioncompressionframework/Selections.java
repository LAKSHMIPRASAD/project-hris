/*package com.mcfadyen.hris.encryptioncompressionframework;

import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import com.mcfadyen.hris.datacompression.*;
import com.mcfadyen.hris.dataencryption.*;

public class Selections {
	public byte[] compressionEncryptionSelection(CompressionOptions co, CryptographyOptions eo, byte[] data, Key key) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException
	{
		if((co == CompressionOptions.GZIP)&&(eo == CryptographyOptions.RSA)){
			String  path = GZipCompression.compressGzipFile(data);
			byte[] encryptedData = RSACryptography.encryptData(path,key);
			return encryptedData;
		}
		else if((co == CompressionOptions.ZIP)&&(eo == CryptographyOptions.RSA)){
			String  path = ZipCompression.compressZipFile(data);
			byte[] encryptedData = RSACryptography.encryptData(path,key);
			return encryptedData;
		}
		else 
		{
			return null;
		}
	}
	public byte[] compressionEncryptionSelection(CompressionOptions co, CryptographyOptions eo, byte[] data, Key key, int degreeOfCompresion) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException
	{
		if((co == CompressionOptions.ZIP)&&(eo == CryptographyOptions.RSA)){
			String  path = ZipCompression.compressZipFile(data,degreeOfCompresion);
			byte[] encryptedData = RSACryptography.encryptData(path,key);
			return encryptedData;
		}
		else 
		{
			return null;
		}
	}
	public String compressionSelection(CompressionOptions co ,byte[] data, int degreeOfCompression) throws IOException
	{
		if(co == CompressionOptions.ZIP)
		{
			String  path = ZipCompression.compressZipFile(data,degreeOfCompression);
			return path;
		}
		else
		{
			return null;
		}
	}
	public String compressionSelection(CompressionOptions co ,byte[] data) throws IOException
	{
		if(co == CompressionOptions.ZIP)
		{
			String  path = ZipCompression.compressZipFile(data);
			return path;
		}
		else if(co == CompressionOptions.GZIP)
		{
			String  path = GZipCompression.compressGzipFile(data);
			return path;
		}
		else
		{
			return null;
		}
	}
	public byte[] compressionEncryptionSelection(CompressionOptions co, CryptographyOptions eo, String data, Key key) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException
	{
		if((co == CompressionOptions.GZIP)&&(eo == CryptographyOptions.RSA)){
			String  path = GZipCompression.compressGzipFile(data);
			byte[] encryptedData = RSACryptography.encryptData(path,key);
			return encryptedData;
		}
		else if((co == CompressionOptions.ZIP)&&(eo == CryptographyOptions.RSA)){
			String  path = ZipCompression.compressZipFile(data);
			byte[] encryptedData = RSACryptography.encryptData(path,key);
			return encryptedData;
		}
		else 
		{
			return null;
		}
	}
	public byte[] compressionEncryptionSelection(CompressionOptions co, CryptographyOptions eo, String data, Key key, int degreeOfCompresion) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException
	{
		if((co == CompressionOptions.ZIP)&&(eo == CryptographyOptions.RSA)){
			String  path = ZipCompression.compressZipFile(data,degreeOfCompresion);
			byte[] encryptedData = RSACryptography.encryptData(path,key);
			return encryptedData;
		}
		else 
		{
			return null;
		}
	}
	public String compressionSelection(CompressionOptions co ,String data, int degreeOfCompression) throws IOException
	{
		if(co == CompressionOptions.ZIP)
		{
			String  path = ZipCompression.compressZipFile(data,degreeOfCompression);
			return path;
		}
		else
		{
			return null;
		}
	}
	public String compressionSelection(CompressionOptions co ,String data) throws IOException
	{
		if(co == CompressionOptions.ZIP)
		{
			String  path = ZipCompression.compressZipFile(data);
			return path;
		}
		else if(co == CompressionOptions.GZIP)
		{
			String  path = GZipCompression.compressGzipFile(data);
			return path;
		}
		else
		{
			return null;
		}
	}
}
*/