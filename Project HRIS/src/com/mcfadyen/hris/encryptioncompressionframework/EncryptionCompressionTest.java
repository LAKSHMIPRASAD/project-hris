package com.mcfadyen.hris.encryptioncompressionframework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Properties;
import java.util.zip.DataFormatException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import com.mcfadyen.hris.cryptography.Cryptography;
import com.mcfadyen.hris.cryptography.CryptographyFactory;
import com.mcfadyen.hris.cryptography.CryptographyOptions;
import com.mcfadyen.hris.datacompression.CompressionDecompression;
import com.mcfadyen.hris.datacompression.CompressionFactory;
import com.mcfadyen.hris.datacompression.CompressionOptions;

public class EncryptionCompressionTest {

	public static void main(String args[]) throws IOException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, DataFormatException {

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();
		KeyGenerator keygenerator = KeyGenerator.getInstance("AES");
		SecretKey key = keygenerator.generateKey();
		String s = "C:\\Users\\msrinath\\Desktop\\pic.jpg";
		Properties prop = new Properties();
		InputStream input = new FileInputStream("config.properties");

		prop.load(input);
		// System.out.println(new File(".").getAbsolutePath());
		// get the property value and print it out
		// System.out.println(prop.getProperty("encryption"));
		// System.out.println(prop.getProperty("compression"));

		CryptographyFactory cryptf = new CryptographyFactory();
		CompressionFactory compf = new CompressionFactory();

		Cryptography crypt = cryptf.getInstance(CryptographyOptions.RSA);
		CompressionDecompression cd = compf.getInstance(CompressionOptions.GZIP);

		//byte[] data = cd.compress(s);
		byte[] data = crypt.encrypt(s, publicKey);
		byte[] decryptedData = crypt.decrypt(data, privateKey);
		//byte[] decompressedData = cd.decompress(data);
		
		File file = new File("C:\\Users\\msrinath\\Desktop\\pic2.jpg");
		FileOutputStream fos = new FileOutputStream(file);
		
		fos.write(decryptedData);
		fos.flush();
		fos.close();

	}
}
