package com.mcfadyen.hris.beans;

import java.util.List;

public class EmploymentBean implements Bean{
	
	private String[] companyName;
	private String[] designation;
	private String[] dateOfJoining;
	private String[] dateOfRelieving;
	private String[] addressLineOne;
	private String[] addressLineTwo;
	private String[] city;
	private String[] state;
	private String[] country;
	private String[] pinCode;	
	private String departmentName;
	private String managerName;
	private String comments;
	
	public String[] getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String[] companyName) {
		this.companyName = companyName;
	}
	public String[] getDesignation() {
		return designation;
	}
	public void setDesignation(String[] designation) {
		this.designation = designation;
	}
	public String[] getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(String[] dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public String[] getDateOfRelieving() {
		return dateOfRelieving;
	}
	public void setDateOfRelieving(String[] dateOfRelieving) {
		this.dateOfRelieving = dateOfRelieving;
	}
	public String[] getAddressLineOne() {
		return addressLineOne;
	}
	public void setAddressLineOne(String[] addressLineOne) {
		this.addressLineOne = addressLineOne;
	}
	public String[] getAddressLineTwo() {
		return addressLineTwo;
	}
	public void setAddressLineTwo(String[] addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}
	public String[] getCity() {
		return city;
	}
	public void setCity(String[] city) {
		this.city = city;
	}
	public String[] getState() {
		return state;
	}
	public void setState(String[] state) {
		this.state = state;
	}
	public String[] getCountry() {
		return country;
	}
	public void setCountry(String[] country) {
		this.country = country;
	}
	public String[] getPinCode() {
		return pinCode;
	}
	public void setPinCode(String[] pinCode) {
		this.pinCode = pinCode;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	

	
}
