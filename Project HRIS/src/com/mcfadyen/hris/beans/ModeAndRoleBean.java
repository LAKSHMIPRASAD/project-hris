package com.mcfadyen.hris.beans;

public class ModeAndRoleBean {
	private String role;
	private int mode;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getMode() {
		return mode;
	}
	public void setMode(int mode) {
		this.mode = mode;
	}

}
