package com.mcfadyen.hris.beans;

import java.util.List;

import javax.servlet.http.Part;

public class PassportVisaBean implements Bean {
	
	private String passportNumber;
	private String dateOfPassportIssue;
	private String dateOfPassportExpiry;
	private Part passport;
	private String[] visaType;
	private String[] country;
	private String[] dateOfVisaExpiry;
	private List<Part> visa;
	
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getDateOfPassportIssue() {
		return dateOfPassportIssue;
	}
	public void setDateOfPassportIssue(String dateOfPassportIssue) {
		this.dateOfPassportIssue = dateOfPassportIssue;
	}
	public String getDateOfPassportExpiry() {
		return dateOfPassportExpiry;
	}
	public void setDateOfPassportExpiry(String dateOfPassportExpiry) {
		this.dateOfPassportExpiry = dateOfPassportExpiry;
	}
	public String[] getVisaType() {
		return visaType;
	}
	public void setVisaType(String[] visaType) {
		this.visaType = visaType;
	}
	public String[] getCountry() {
		return country;
	}
	public void setCountry(String[] country) {
		this.country = country;
	}
	public String[] getDateOfVisaExpiry() {
		return dateOfVisaExpiry;
	}
	public void setDateOfVisaExpiry(String[] dateOfVisaExpiry) {
		this.dateOfVisaExpiry = dateOfVisaExpiry;
	}
	public Part getPassport() {
		return passport;
	}
	public void setPassport(Part passport) {
		this.passport = passport;
	}
	public List<Part> getVisa() {
		return visa;
	}
	public void setVisa(List<Part> visa) {
		this.visa = visa;
	}

}
