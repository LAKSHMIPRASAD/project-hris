package com.mcfadyen.hris.beans;

import javax.servlet.http.Part;

public class DocumentBean implements Bean {

	private int id;
	private String type;
	private Part part;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Part getPart() {
		return part;
	}
	public void setPart(Part part) {
		this.part = part;
	}
}
