package com.mcfadyen.hris.beans;

import java.util.Date;

public class ProfileBean {
	private String employeeId;
	private String aadharId;
	private String panNumber;
	private String firstName;
	private String middleName;
	private String lastName;
	private String uan;
	private String gender;
	private String phoneNumber1;
	private String phoneNumber2;
	private String emailId;
	private String emergencyPersonName;
	private String emergencyPersonRelationship;
	private String emergencyPersonPhoneNumber;
	private String permanentAddressLine1;
	private String permanentAddressLine2;
	private String permanentAddressCity;
	private String permanentAddressState;
	private String permanentAddressCountry;
	private String permanentAddresspincode;
	private String currentAddressLine1;
	private String currenttAddressLine2;
	private String currentAddressCity;
	private String currentAddressState;
	private String currentAddressCountry;
	private String currentAddresspincode;
	private String messenger;
	private String messengerId;
	private String role;
	private String userName;
	
	//private Date   dob;
	
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public ProfileBean(){
		
	}
	
	public String getCurrentAddressLine1() {
		return currentAddressLine1;
	}
	public void setCurrentAddressLine1(String currentAddressLine1) {
		this.currentAddressLine1 = currentAddressLine1;
	}
	public String getCurrenttAddressLine2() {
		return currenttAddressLine2;
	}
	public void setCurrenttAddressLine2(String currenttAddressLine2) {
		this.currenttAddressLine2 = currenttAddressLine2;
	}
	public String getCurrentAddressCity() {
		return currentAddressCity;
	}
	public void setCurrentAddressCity(String currentAddressCity) {
		this.currentAddressCity = currentAddressCity;
	}
	public String getCurrentAddressState() {
		return currentAddressState;
	}
	public void setCurrentAddressState(String currentAddressState) {
		this.currentAddressState = currentAddressState;
	}
	public String getCurrentAddressCountry() {
		return currentAddressCountry;
	}
	public void setCurrentAddressCountry(String currentAddressCountry) {
		this.currentAddressCountry = currentAddressCountry;
	}
	public String getCurrentAddresspincode() {
		return currentAddresspincode;
	}
	public void setCurrentAddresspincode(String currentAddresspincode) {
		this.currentAddresspincode = currentAddresspincode;
	}
	
	
	
	public String getMessengerId() {
		return messengerId;
	}

	public void setMessengerId(String messengerId) {
		this.messengerId = messengerId;
	}

	public String getMessenger() {
		return messenger;
	}
	public void setMessenger(String messenger) {
		this.messenger = messenger;
	}
	
	public String getPermanentAddressLine1() {
		return permanentAddressLine1;
	}
	public void setPermanentAddressLine1(String permanentAddressLine1) {
		this.permanentAddressLine1 = permanentAddressLine1;
	}
	public String getPermanentAddressLine2() {
		return permanentAddressLine2;
	}
	public void setPermanentAddressLine2(String permanentAddressLine2) {
		this.permanentAddressLine2 = permanentAddressLine2;
	}
	public String getPermanentAddressCity() {
		return permanentAddressCity;
	}
	public void setPermanentAddressCity(String permanentAddressCity) {
		this.permanentAddressCity = permanentAddressCity;
	}
	public String getPermanentAddressState() {
		return permanentAddressState;
	}
	public void setPermanentAddressState(String permanentAddressState) {
		this.permanentAddressState = permanentAddressState;
	}
	public String getPermanentAddressCountry() {
		return permanentAddressCountry;
	}
	public void setPermanentAddressCountry(String permanentAddressCountry) {
		this.permanentAddressCountry = permanentAddressCountry;
	}
	public String getPermanentAddresspincode() {
		return permanentAddresspincode;
	}
	public void setPermanentAddresspincode(String permanentAddresspincode) {
		this.permanentAddresspincode = permanentAddresspincode;
	}
//	public Date getDob() {
//		return dob;
//	}
//	public void setDob(Date dob) {
//		this.dob = dob;
//	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getAadharId() {
		return aadharId;
	}
	public void setAadharId(String aadharId) {
		this.aadharId = aadharId;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUan() {
		return uan;
	}
	public void setUan(String uan) {
		this.uan = uan;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhoneNumber1() {
		return phoneNumber1;
	}
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}
	public String getPhoneNumber2() {
		return phoneNumber2;
	}
	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getEmergencyPersonName() {
		return emergencyPersonName;
	}
	public void setEmergencyPersonName(String emergencyPersonName) {
		this.emergencyPersonName = emergencyPersonName;
	}
	public String getEmergencyPersonRelationship() {
		return emergencyPersonRelationship;
	}
	public void setEmergencyPersonRelationship(String emergencyPersonRelationship) {
		this.emergencyPersonRelationship = emergencyPersonRelationship;
	}
	public String getEmergencyPersonPhoneNumber() {
		return emergencyPersonPhoneNumber;
	}
	public void setEmergencyPersonPhoneNumber(String emergencyPersonPhoneNumber) {
		this.emergencyPersonPhoneNumber = emergencyPersonPhoneNumber;
	}
	
	

}
