package com.mcfadyen.hris.beans;

public class ExitFormBean implements Bean{
	
	private String employeeID;
	private String interviewer;
	private String title;
	private String alternativeEmployment;
	private String alternativeEmploymentOther;
	private String otherOptions;
	private String otherOptionsReason;
	private String positiveImage;
	private String cultureAlignedImage;
	private String coreValues;
	private String companyGoals;
	private String fairness;
	private String recognition;
	private String interDepartmentTeamWork;
	private String intraDepartmentTeamwork;
	private String suggestions;
	private String resolvingProblems;
	private String followingPolicies;
	private String followThrough;
	private String providedDirection;
	private String providedFeedback;
	private String ensuredResources;
	private String personalCommunication;
	private String interDepartmentCommunication;
	private String intraDepartmentCommunication;
	private String fairPay;
	private String competitonCultureImage;
	private String effortsRecognised;
	private String workingConditions;
	private String workEnvironment;
	private String stability;
	private String adequateTraining;
	private String interestingWork;
	private String learningNewSkills;
	private String personalCallenges;
	private String promotionOpportunities;
	private String specialAssignment;
	private String careerDevelopment;
	private String resonableWorkHours;
	private String comfortableVacation;
	private String comfortableBreaks;
	private String workLifebalance;
	private String flexibleWorkSchedule;
	private String workingExtraHours;
	private String workLoad;
	private String expectationStress;
	private String workEnvironmentInformation;
	private String enjoyablePartOfJob;
	private String enjoyablePartOfCompany;
	private String frustratingPartOfJob;
	private String frustrationPartOfCompany;
	private String betterAboutNewJob;
	private String overallSatisfaction;
	private String supervisorRating;
	private String recommendation;
	private String additionalComments;
	
	public String getAlternativeEmployment() {
		return alternativeEmployment;
	}
	public void setAlternativeEmployment(String alternativeEmployment) {
		this.alternativeEmployment = alternativeEmployment;
	}
	public String getAlternativeEmploymentOther() {
		return alternativeEmploymentOther;
	}
	public void setAlternativeEmploymentOther(String alternativeEmploymentOther) {
		this.alternativeEmploymentOther = alternativeEmploymentOther;
	}
	public String getOtherOptions() {
		return otherOptions;
	}
	public void setOtherOptions(String otherOptions) {
		this.otherOptions = otherOptions;
	}
	public String getOtherOptionsReason() {
		return otherOptionsReason;
	}
	public void setOtherOptionsReason(String otherOptionsReason) {
		this.otherOptionsReason = otherOptionsReason;
	}
	public String getPositiveImage() {
		return positiveImage;
	}
	public void setPositiveImage(String positiveImage) {
		this.positiveImage = positiveImage;
	}
	public String getCultureAlignedImage() {
		return cultureAlignedImage;
	}
	public void setCultureAlignedImage(String cultureAlignedImage) {
		this.cultureAlignedImage = cultureAlignedImage;
	}
	public String getCoreValues() {
		return coreValues;
	}
	public void setCoreValues(String coreValues) {
		this.coreValues = coreValues;
	}
	public String getCompanyGoals() {
		return companyGoals;
	}
	public void setCompanyGoals(String companyGoals) {
		this.companyGoals = companyGoals;
	}
	public String getFairness() {
		return fairness;
	}
	public void setFairness(String fairness) {
		this.fairness = fairness;
	}
	public String getRecognition() {
		return recognition;
	}
	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}
	public String getInterDepartmentTeamWork() {
		return interDepartmentTeamWork;
	}
	public void setInterDepartmentTeamWork(String interDepartmentTeamWork) {
		this.interDepartmentTeamWork = interDepartmentTeamWork;
	}
	public String getIntraDepartmentTeamwork() {
		return intraDepartmentTeamwork;
	}
	public void setIntraDepartmentTeamwork(String intraDepartmentTeamwork) {
		this.intraDepartmentTeamwork = intraDepartmentTeamwork;
	}
	public String getSuggestions() {
		return suggestions;
	}
	public void setSuggestions(String suggestions) {
		this.suggestions = suggestions;
	}
	public String getResolvingProblems() {
		return resolvingProblems;
	}
	public void setResolvingProblems(String resolvingProblems) {
		this.resolvingProblems = resolvingProblems;
	}
	public String getFollowingPolicies() {
		return followingPolicies;
	}
	public void setFollowingPolicies(String followingPolicies) {
		this.followingPolicies = followingPolicies;
	}
	public String getFollowThrough() {
		return followThrough;
	}
	public void setFollowThrough(String followThrough) {
		this.followThrough = followThrough;
	}
	public String getProvidedDirection() {
		return providedDirection;
	}
	public void setProvidedDirection(String providedDirection) {
		this.providedDirection = providedDirection;
	}
	public String getProvidedFeedback() {
		return providedFeedback;
	}
	public void setProvidedFeedback(String providedFeedback) {
		this.providedFeedback = providedFeedback;
	}
	public String getEnsuredResources() {
		return ensuredResources;
	}
	public void setEnsuredResources(String ensuredResources) {
		this.ensuredResources = ensuredResources;
	}
	public String getPersonalCommunication() {
		return personalCommunication;
	}
	public void setPersonalCommunication(String personalCommunication) {
		this.personalCommunication = personalCommunication;
	}
	public String getInterDepartmentCommunication() {
		return interDepartmentCommunication;
	}
	public void setInterDepartmentCommunication(String interDepartmentCommunication) {
		this.interDepartmentCommunication = interDepartmentCommunication;
	}
	public String getIntraDepartmentCommunication() {
		return intraDepartmentCommunication;
	}
	public void setIntraDepartmentCommunication(String intraDepartmentCommunication) {
		this.intraDepartmentCommunication = intraDepartmentCommunication;
	}
	public String getFairPay() {
		return fairPay;
	}
	public void setFairPay(String fairPay) {
		this.fairPay = fairPay;
	}
	public String getCompetitonCultureImage() {
		return competitonCultureImage;
	}
	public void setCompetitonCultureImage(String competitonCultureImage) {
		this.competitonCultureImage = competitonCultureImage;
	}
	public String getEffortsRecognised() {
		return effortsRecognised;
	}
	public void setEffortsRecognised(String effortsRecognised) {
		this.effortsRecognised = effortsRecognised;
	}
	public String getWorkingConditions() {
		return workingConditions;
	}
	public void setWorkingConditions(String workingConditions) {
		this.workingConditions = workingConditions;
	}
	public String getWorkEnvironment() {
		return workEnvironment;
	}
	public void setWorkEnvironment(String workEnvironment) {
		this.workEnvironment = workEnvironment;
	}
	public String getStability() {
		return stability;
	}
	public void setStability(String stability) {
		this.stability = stability;
	}
	public String getAdequateTraining() {
		return adequateTraining;
	}
	public void setAdequateTraining(String adequateTraining) {
		this.adequateTraining = adequateTraining;
	}
	public String getInterestingWork() {
		return interestingWork;
	}
	public void setInterestingWork(String interestingWork) {
		this.interestingWork = interestingWork;
	}
	public String getLearningNewSkills() {
		return learningNewSkills;
	}
	public void setLearningNewSkills(String learningNewSkills) {
		this.learningNewSkills = learningNewSkills;
	}
	public String getPersonalCallenges() {
		return personalCallenges;
	}
	public void setPersonalCallenges(String personalCallenges) {
		this.personalCallenges = personalCallenges;
	}
	public String getPromotionOpportunities() {
		return promotionOpportunities;
	}
	public void setPromotionOpportunities(String promotionOpportunities) {
		this.promotionOpportunities = promotionOpportunities;
	}
	public String getSpecialAssignment() {
		return specialAssignment;
	}
	public void setSpecialAssignment(String specialAssignment) {
		this.specialAssignment = specialAssignment;
	}
	public String getCareerDevelopment() {
		return careerDevelopment;
	}
	public void setCareerDevelopment(String careerDevelopment) {
		this.careerDevelopment = careerDevelopment;
	}
	public String getResonableWorkHours() {
		return resonableWorkHours;
	}
	public void setResonableWorkHours(String resonableWorkHours) {
		this.resonableWorkHours = resonableWorkHours;
	}
	public String getComfortableVacation() {
		return comfortableVacation;
	}
	public void setComfortableVacation(String comfortableVacation) {
		this.comfortableVacation = comfortableVacation;
	}
	public String getComfortableBreaks() {
		return comfortableBreaks;
	}
	public void setComfortableBreaks(String comfortableBreaks) {
		this.comfortableBreaks = comfortableBreaks;
	}
	public String getWorkLifebalance() {
		return workLifebalance;
	}
	public void setWorkLifebalance(String workLifebalance) {
		this.workLifebalance = workLifebalance;
	}
	public String getFlexibleWorkSchedule() {
		return flexibleWorkSchedule;
	}
	public void setFlexibleWorkSchedule(String flexibleWorkSchedule) {
		this.flexibleWorkSchedule = flexibleWorkSchedule;
	}
	public String getWorkingExtraHours() {
		return workingExtraHours;
	}
	public void setWorkingExtraHours(String workingExtraHours) {
		this.workingExtraHours = workingExtraHours;
	}
	public String getWorkLoad() {
		return workLoad;
	}
	public void setWorkLoad(String workLoad) {
		this.workLoad = workLoad;
	}
	public String getExpectationStress() {
		return expectationStress;
	}
	public void setExpectationStress(String expectationStress) {
		this.expectationStress = expectationStress;
	}
	public String getWorkEnvironmentInformation() {
		return workEnvironmentInformation;
	}
	public void setWorkEnvironmentInformation(String workEnvironmentInformation) {
		this.workEnvironmentInformation = workEnvironmentInformation;
	}
	public String getEnjoyablePartOfJob() {
		return enjoyablePartOfJob;
	}
	public void setEnjoyablePartOfJob(String enjoyablePartOfJob) {
		this.enjoyablePartOfJob = enjoyablePartOfJob;
	}
	public String getEnjoyablePartOfCompany() {
		return enjoyablePartOfCompany;
	}
	public void setEnjoyablePartOfCompany(String enjoyablePartOfCompany) {
		this.enjoyablePartOfCompany = enjoyablePartOfCompany;
	}
	public String getFrustratingPartOfJob() {
		return frustratingPartOfJob;
	}
	public void setFrustratingPartOfJob(String frustratingPartOfJob) {
		this.frustratingPartOfJob = frustratingPartOfJob;
	}
	public String getFrustrationPartOfCompany() {
		return frustrationPartOfCompany;
	}
	public void setFrustrationPartOfCompany(String frustrationPartOfCompany) {
		this.frustrationPartOfCompany = frustrationPartOfCompany;
	}
	public String getBetterAboutNewJob() {
		return betterAboutNewJob;
	}
	public void setBetterAboutNewJob(String betterAboutNewJob) {
		this.betterAboutNewJob = betterAboutNewJob;
	}
	public String getOverallSatisfaction() {
		return overallSatisfaction;
	}
	public void setOverallSatisfaction(String overallSatisfaction) {
		this.overallSatisfaction = overallSatisfaction;
	}
	public String getSupervisorRating() {
		return supervisorRating;
	}
	public void setSupervisorRating(String supervisorRating) {
		this.supervisorRating = supervisorRating;
	}
	public String getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	public String getAdditionalComments() {
		return additionalComments;
	}
	public void setAdditionalComments(String additionalComments) {
		this.additionalComments = additionalComments;
	}
	public String getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}
	public String getInterviewer() {
		return interviewer;
	}
	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
