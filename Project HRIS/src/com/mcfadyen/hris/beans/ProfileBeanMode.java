package com.mcfadyen.hris.beans;

public class ProfileBeanMode extends ProfileBean {
	private String mode;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

}
