package com.mcfadyen.hris.manager;

import com.mcfadyen.hris.beans.ProfileBean;
import com.mcfadyen.hris.service.PersonalDetailsService;

public class PersonalDetailsManager {
	public void personaldetails(ProfileBean bean){
		PersonalDetailsService service=new PersonalDetailsService();
		service.personaldetailsservice(bean);
	}

	public ProfileBean personalDetailsDisplay(ProfileBean displayBean ){
		PersonalDetailsService service=new PersonalDetailsService();
		ProfileBean display=service.personaldetailDis(displayBean);
		return display;
	}
	
}
