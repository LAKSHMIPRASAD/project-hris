package com.mcfadyen.hris.manager;

import com.mcfadyen.hris.beans.*;
import com.mcfadyen.hris.service.*;

public class ManagerHRIS {
	public void personaldetails(ProfileBean bean){
		PersonalDetailsService service=new PersonalDetailsService();
		service.personaldetailsservice(bean);
	}

	public ProfileBean personalDetailsDisplay(ProfileBean displayBean ){
		PersonalDetailsService service=new PersonalDetailsService();
		ProfileBean display=service.personaldetailDis(displayBean);
		return display;
	}
	
	public void passportDetails(PassportVisaBean bean){
		PassportVisaService passportService= new PassportVisaService();
		passportService.passportDaoCallService(bean);
	}

	public void EmploymentDetails(EmploymentBean bean) {
		EmploymentService employmentService = new EmploymentService();
		employmentService.employmentDAOCall(bean);
	}

	public void ExitDetails(ExitFormBean bean) {
		
		ExitService exitService = new ExitService();
		exitService.exitDAOCall(bean);
	}
	
	
	public void DocumentDetails(DocumentBean bean) {
			
		DocumentService documentService = new DocumentService();
		documentService.documentDAOCall(bean);
	}


}
