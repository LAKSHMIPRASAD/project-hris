package com.mcfadyen.hris.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mcfadyen.hris.beans.ModeAndRoleBean;
import com.mcfadyen.hris.beans.ProfileBean;
import com.mcfadyen.hris.beans.ProfileBeanMode;
import com.mcfadyen.hris.daoimplementation.PersonalDaoImplementation;
import com.mysql.jdbc.Statement;

public class ModeManager {
	public static String EMP_ID;
	public ModeAndRoleBean fetchMode(ModeAndRoleBean modeBean) {
		ResultSet modeResult = null;
		ResultSet employeeIdResult=null;
		
		try {

			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			System.out.println("Error1");
		}
		String url = "jdbc:mysql://localhost/hris";
		String user = "root";
		String pass = "";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("connected");
		} catch (SQLException e) {
			System.out.println("Error2");
		}
		PreparedStatement mode,employeeId;
		try {
			ProfileBean employeeDetailsEnteredByHr=new ProfileBean();
			String sqlFetchMode;
			String empIdQuery;
			
			
			empIdQuery="SELECT emp_id FROM personal_details WHERE user_name=?";
			employeeId=conn.prepareStatement(empIdQuery);
		    employeeId.setString(1, PersonalDaoImplementation.userName);
			employeeIdResult=employeeId.executeQuery();
			employeeIdResult.next();
			
			
			sqlFetchMode="SELECT mode,role FROM personal_details WHERE emp_id=?";
			mode=conn.prepareStatement(sqlFetchMode);
			mode.setString(1, employeeIdResult.getString(1));
			modeResult = mode.executeQuery();
		    modeResult.next();
			modeBean.setMode(modeResult.getInt(1));
			modeBean.setRole(modeResult.getString(2));
			
			
			conn.close();
			
			

		} catch (SQLException e) {
			System.out.println("exception in dao insert");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return modeBean;
	}
	
	public ProfileBeanMode displayEmployeeDetails(ProfileBeanMode employeeDetailsEnteredByHr){
      ResultSet resultSetdetailsOfEmployeeEnteredByHR=null;
      ResultSet employeeIdResult=null;
		
		try {

			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			System.out.println("Error1");
		}
		String url = "jdbc:mysql://localhost/hris";
		String user = "root";
		String pass = "";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("connected");
		} catch (SQLException e) {
			System.out.println("Error2");
		}
		PreparedStatement prepareddetailsOfEmployeeEnteredByHR,employeeId;
		try {
			
			
			String detailsOfEmployeeEnteredByHR;
            String empIdQuery;
			
			
			empIdQuery="SELECT emp_id FROM personal_details WHERE user_name=?";
			employeeId=conn.prepareStatement(empIdQuery);
		    employeeId.setString(1, PersonalDaoImplementation.userName);
			employeeIdResult=employeeId.executeQuery();
			employeeIdResult.next();
			
			EMP_ID=employeeIdResult.getString(1);
			
			detailsOfEmployeeEnteredByHR="SELECT emp_id,aadhar_id,pan_number,first_name,middle_name,last_name,UAN,date_of_birth,gender,user_name,mode,role FROM personal_details WHERE emp_id=?";
			prepareddetailsOfEmployeeEnteredByHR=conn.prepareStatement(detailsOfEmployeeEnteredByHR);
			prepareddetailsOfEmployeeEnteredByHR.setString(1, employeeIdResult.getString(1));
			
			resultSetdetailsOfEmployeeEnteredByHR=prepareddetailsOfEmployeeEnteredByHR.executeQuery();
			resultSetdetailsOfEmployeeEnteredByHR.next();
			
			employeeDetailsEnteredByHr.setEmployeeId(resultSetdetailsOfEmployeeEnteredByHR.getString("emp_id"));
			employeeDetailsEnteredByHr.setAadharId(resultSetdetailsOfEmployeeEnteredByHR.getString("aadhar_id"));
			employeeDetailsEnteredByHr.setPanNumber(resultSetdetailsOfEmployeeEnteredByHR.getString("pan_number"));
			employeeDetailsEnteredByHr.setFirstName(resultSetdetailsOfEmployeeEnteredByHR.getString("first_name"));
			employeeDetailsEnteredByHr.setMiddleName(resultSetdetailsOfEmployeeEnteredByHR.getString("middle_name"));
			employeeDetailsEnteredByHr.setLastName(resultSetdetailsOfEmployeeEnteredByHR.getString("last_name"));
			employeeDetailsEnteredByHr.setUan(resultSetdetailsOfEmployeeEnteredByHR.getString("UAN"));
			employeeDetailsEnteredByHr.setUan(resultSetdetailsOfEmployeeEnteredByHR.getString("UAN"));

			employeeDetailsEnteredByHr.setGender(resultSetdetailsOfEmployeeEnteredByHR.getString("gender"));
			//dob
			employeeDetailsEnteredByHr.setUserName(resultSetdetailsOfEmployeeEnteredByHR.getString("user_name"));
			employeeDetailsEnteredByHr.setMode(resultSetdetailsOfEmployeeEnteredByHR.getString("mode"));
			employeeDetailsEnteredByHr.setRole(resultSetdetailsOfEmployeeEnteredByHR.getString("role"));
			
			
			 conn.close();
			
			
			

		} catch (SQLException e) {
			System.out.println("exception in dao insert");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return employeeDetailsEnteredByHr;
		
	}

}
