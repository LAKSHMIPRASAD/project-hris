package com.mcfadyen.hris.daoimplementation;

import java.io.Console;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mcfadyen.hris.beans.ModeAndRoleBean;
import com.mcfadyen.hris.beans.ProfileBean;
import com.mcfadyen.hris.controller.ModeController;
import com.mcfadyen.hris.dao.PersonalDaoInterface;
import com.mcfadyen.hris.manager.ModeManager;
import com.mysql.jdbc.Statement;

public class PersonalDaoImplementation implements PersonalDaoInterface {
	public static String userName;
	public static void setUserNameFromLdap(String user){
	userName=user;
	}
	
	public void insert(ProfileBean bean) {
		ResultSet rs = null, messengerRs = null, modeRs=null,	Emp_IdInTableRs=null;
		// DataBaseConnection connect=new DataBaseConnection();
		// connect.connection();
		// System.out.println(password);
		try {

			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			System.out.println("Error1");
		}
		String url = "jdbc:mysql://localhost/hris";
		String user = "root";
		String pass = "";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("connected");
		} catch (SQLException e) {
			System.out.println("Error2");
		}
		PreparedStatement profileDetails = null, messengerDetails = null, addressDetails = null, mode=null,CheckEmp_idInTable=null,PreparedUpdatePersonalDetails=null;
		try {
			ModeAndRoleBean role=new ModeAndRoleBean();
			String sqlProfileDetails, sqlMessengerDetails, sqlAddress,sqlmode,sqlCheckEmp_idInTable,sqlUpdatePersonalDetails;
			// sql ="SELECT id,password FROM user WHERE id= ? AND password= ?";
			sqlUpdatePersonalDetails="UPDATE personal_details SET phone_number_1=?,phone_number_2=?,email_id=?,emergency_contact_person=?,Relation=?,emergency_contact_number=?,mode=?,current_address_id=?,permanent_address_id=?,messenger_key=? WHERE emp_id=?"	;
			sqlCheckEmp_idInTable="SELECT user_name FROM personal_details WHERE user_name=?";
			sqlmode="SELECT mode FROM role_access WHERE role=? ";
			sqlAddress = "INSERT INTO address_table(line1,line2,city,state,country,pincode) VALUES(?,?,?,?,?,?)";
			sqlMessengerDetails = "INSERT INTO messenger_table(type_of_messenger,messenger_id) VALUES(?,?)";
			sqlProfileDetails = "INSERT INTO personal_details(emp_id,first_name,middle_name,last_name,gender,date_of_birth,aadhar_id,pan_number,UAN,role,mode,user_name) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";

		    CheckEmp_idInTable=conn.prepareStatement(sqlCheckEmp_idInTable);
		    CheckEmp_idInTable.setString(1,PersonalDaoImplementation.userName);
		    Emp_IdInTableRs=CheckEmp_idInTable.executeQuery();
		    
		    System.out.println(ModeController.ROLE);
	
			if(ModeController.ROLE.equals("user")){
				System.out.println("personalDao USER");
				addressDetails = conn.prepareStatement(sqlAddress, Statement.RETURN_GENERATED_KEYS);
				messengerDetails = conn.prepareStatement(sqlMessengerDetails, Statement.RETURN_GENERATED_KEYS);
				

				addressDetails.setString(1, bean.getCurrentAddressLine1());
				addressDetails.setString(2, bean.getCurrenttAddressLine2());
				addressDetails.setString(3, bean.getCurrentAddressCity());
				addressDetails.setString(4, bean.getCurrentAddressState());
				addressDetails.setString(5, bean.getCurrentAddressCountry());
				addressDetails.setString(6, bean.getCurrentAddresspincode());

				addressDetails.executeUpdate();
				rs = addressDetails.getGeneratedKeys();
				rs.next();
				//System.out.println(rs.getInt(1));

				messengerDetails.setString(1, bean.getMessenger());
				messengerDetails.setString(2, bean.getMessengerId());

				messengerDetails.executeUpdate();
				messengerRs = messengerDetails.getGeneratedKeys();
				messengerRs.next();

			    PreparedUpdatePersonalDetails=conn.prepareStatement(sqlUpdatePersonalDetails);
			    PreparedUpdatePersonalDetails.setString(1, bean.getPhoneNumber1());
			    PreparedUpdatePersonalDetails.setString(2, bean.getPhoneNumber2());
			    PreparedUpdatePersonalDetails.setString(3, bean.getEmailId());
			    PreparedUpdatePersonalDetails.setString(4, bean.getEmergencyPersonName());
			    PreparedUpdatePersonalDetails.setString(5, bean.getEmergencyPersonRelationship());
			    PreparedUpdatePersonalDetails.setString(6, bean.getEmergencyPersonPhoneNumber());
			    PreparedUpdatePersonalDetails.setInt(7,1);
			    PreparedUpdatePersonalDetails.setInt(8, rs.getInt(1));
			    PreparedUpdatePersonalDetails.setInt(9, rs.getInt(1));
			    PreparedUpdatePersonalDetails.setInt(10, messengerRs.getInt(1));
			    PreparedUpdatePersonalDetails.setString(11, ModeManager.EMP_ID);
			    
			    PreparedUpdatePersonalDetails.executeUpdate();
				
			}
			
			if(ModeController.ROLE.equals("HR")){
				System.out.println("personalDao HR");
				mode=conn.prepareStatement(sqlmode);
				mode.setString(1, bean.getRole());
				modeRs=mode.executeQuery();
				modeRs.next();
				
				profileDetails = conn.prepareStatement(sqlProfileDetails);
				
				profileDetails.setString(1, bean.getEmployeeId());
				profileDetails.setString(2, bean.getFirstName());
				profileDetails.setString(3, bean.getMiddleName());
				profileDetails.setString(4, bean.getLastName());
				profileDetails.setString(5, bean.getGender());
				profileDetails.setString(6, null);
				
				profileDetails.setString(7, bean.getAadharId());
				profileDetails.setString(8, bean.getPanNumber());
				profileDetails.setString(9, bean.getUan());
				profileDetails.setString(10, bean.getRole());
				profileDetails.setInt(11, modeRs.getInt(1));
				profileDetails.setString(12, bean.getUserName());

				profileDetails.executeUpdate();
			}

			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public ProfileBean display(ProfileBean displayBean) {
		try {

			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			System.out.println("Error1");
		}
		String url = "jdbc:mysql://localhost/hris";
		String user = "root";
		String pass = "";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("connected");
		} catch (SQLException e) {
			System.out.println("Error2");
		}
		String sql = "select * from personal_details where emp_id='111'";
		PreparedStatement statement = null;
		try {
			statement = conn.prepareStatement(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			ResultSet result = statement.executeQuery();
			while (result.next()) {

				displayBean.setEmployeeId(result.getString("emp_id"));
				displayBean.setFirstName((result.getString("first_name")));
				displayBean.setMiddleName((result.getString("middle_name")));
				displayBean.setLastName((result.getString("last_name")));
				displayBean.setGender((result.getString("gender")));
				displayBean.setPhoneNumber1((result.getString("phone_number_1")));
				displayBean.setPhoneNumber2((result.getString("phone_number_2")));
				displayBean.setEmailId((result.getString("email_id")));
				displayBean.setMessengerId((result.getString("messenger_key")));
				displayBean.setEmergencyPersonName((result.getString("emergency_contact_person")));
				displayBean.setEmergencyPersonRelationship((result.getString("Relation")));
				displayBean.setEmergencyPersonPhoneNumber((result.getString("emergency_contact_number")));
				displayBean.setAadharId((result.getString("aadhar_id")));
				displayBean.setPanNumber((result.getString("pan_number")));
				displayBean.setUan((result.getString("UAN")));
				// etc.
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return displayBean;

	}

	@Override
	public void insert() {
		// TODO Auto-generated method stub

	}
}
