package com.mcfadyen.hris.daoimplementation;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.mcfadyen.hris.beans.*;
import com.mcfadyen.hris.dao.Dao;

public class PassportDaoImplementation implements Dao {
	Connection con = null;
	Date date = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

	@Override
	public Bean display(Bean displayBean) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(Bean bean) {

		PassportVisaBean passportBean = (PassportVisaBean) bean;

		try {
			int autoGenKey = -1;
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hris", "root", "root");

			String passportDetails = "INSERT INTO passport_details(emp-id,passport_number,data_of_issue,date_of_expiry, passport_document_id, created_by, created_on) VALUES(?,?,?,?,?,?,?)";
			String visaDetails = "INSERT INTO visa_details(passport_number,date_of_expiry,visa_type,country, visa_document_id, created_by, created_on) VALUES(?,?,?,?,?,?)";

			PreparedStatement passportStatement = con.prepareStatement(passportDetails);
			PreparedStatement visaStatement = con.prepareStatement(visaDetails);
			autoGenKey = insertDocument(passportBean, null);
			passportStatement.setInt(5, autoGenKey);
			passportStatement.setString(6, null);
			passportStatement.setTimestamp(7, new Timestamp(date.getTime()));
			passportStatement.executeUpdate();

			int numberOfVisas = passportBean.getVisaType().length;
			for (int i = 0; i < numberOfVisas; i++) {
				visaStatement.setString(1, passportBean.getPassportNumber());
				Date dateOfVisaExpiry = dateFormat.parse(passportBean.getDateOfVisaExpiry()[i]);
				visaStatement.setDate(2, new java.sql.Date(dateOfVisaExpiry.getTime()));
				visaStatement.setString(3, passportBean.getVisaType()[i]);
				visaStatement.setString(4, passportBean.getCountry()[i]);
				autoGenKey = insertDocument(passportBean, i);
				visaStatement.setInt(5, autoGenKey);
				visaStatement.setString(7, null);
				visaStatement.setTimestamp(8, new Timestamp(date.getTime()));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private int insertDocument(PassportVisaBean passportBean, Integer i) throws SQLException, IOException {

		InputStream inputStream = null;
		String insertDocuments = "INSERT INTO emp_doc_map (emp_id,doc_path,created_by,created_on) values (?,?,?,?)";
		String insertEncodedDocuments = "INSERT INTO emp_doc_encoded (emp_doc_map_id, document, created_by, created_on) values (?,?,?,?)";
		int autoGenKey = -1;

		PreparedStatement document = con.prepareStatement(insertDocuments, Statement.RETURN_GENERATED_KEYS);
		PreparedStatement encodedDocument = con.prepareStatement(insertEncodedDocuments);

		document.setString(1, "e 201");
		document.setString(2, "");
		document.setString(3, null);
		document.setTimestamp(4, new Timestamp(date.getTime()));
		document.executeUpdate();

		ResultSet rs = document.getGeneratedKeys();
		if (rs.next()) {
			autoGenKey = rs.getInt(1);
		}

		encodedDocument.setInt(1, autoGenKey);
		if (i != null) {
			inputStream = passportBean.getVisa().get(i).getInputStream();
		} else {
			inputStream = passportBean.getPassport().getInputStream();
		}
		encodedDocument.setBlob(2, inputStream);
		encodedDocument.setString(3, null);
		encodedDocument.setTimestamp(4, new Timestamp(date.getTime()));
		
		encodedDocument.executeUpdate();

		return autoGenKey;
	}

}
