package com.mcfadyen.hris.daoimplementation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mcfadyen.hris.beans.Bean;
import com.mcfadyen.hris.beans.EmploymentBean;
import com.mcfadyen.hris.dao.Dao;

public class EmploymentDaoImplementation implements Dao {
	
	static Connection con = null;
	static Date date = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

	@Override
	public void insert(Bean bean) {
		
		EmploymentBean employmentBean = (EmploymentBean)bean;
		String insertNewEntry="INSERT INTO employment_details (is_current,emp_id,company_name,location,date_of_joining,date_of_relieving,designation,created_by,created_on) values (?,?,?,?,?,?,?,?,?)";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hris","root","root");
			
			
			PreparedStatement statement = con.prepareStatement(insertNewEntry);
			int i;
			int length= employmentBean.getCompanyName().length;
			for(i=0;i<length;i++){
            statement.setInt(1, 1);
            statement.setString(2, "e 201");
            statement.setString(3, employmentBean.getCompanyName()[i]);
            int autoGenKey = EmploymentDaoImplementation.newAddress(employmentBean,i);
            statement.setInt(4, autoGenKey);
            Date joiningDate = dateFormat.parse(employmentBean.getDateOfJoining()[i]);
            statement.setDate(5, new java.sql.Date(joiningDate.getTime()));
            if(i==0){
            	statement.setDate(6, null);
            }
            else{
            	 Date relievingDate = dateFormat.parse(employmentBean.getDateOfRelieving()[i-1]);
                 statement.setDate(6, new java.sql.Date(relievingDate.getTime()));
            }
            statement.setString(7, employmentBean.getDesignation()[i]);
            statement.setString(8, null);
            statement.setTimestamp(9, new Timestamp(date.getTime()));
            statement.executeUpdate();
			}
            

            con.close();

			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e){
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
	}

	@Override
	public Bean display(Bean displayBean) {
		
		return null;
	}
	
	private static int newAddress(EmploymentBean employmentBean, int i) throws SQLException{
		
		int autoGenKey = -1;
		
		String insertNewLocation = "INSERT INTO address_table (line1, line2, city, state, country, pincode, created_by, created_on) values (?,?,?,?,?,?,?,?)";
		PreparedStatement addressStatement = con.prepareStatement(insertNewLocation,Statement.RETURN_GENERATED_KEYS);
		addressStatement.setString(1, employmentBean.getAddressLineOne()[i]);
        addressStatement.setString(2,employmentBean.getAddressLineTwo()[i]);
        addressStatement.setString(3, employmentBean.getCity()[i]);
        addressStatement.setString(4, employmentBean.getState()[i]);
        addressStatement.setString(5, employmentBean.getCountry()[i]);
        addressStatement.setString(6, employmentBean.getPinCode()[i]);
        addressStatement.setString(7, null);
        addressStatement.setTimestamp(8, new Timestamp(date.getTime()));
        addressStatement.executeUpdate();
        ResultSet rs = addressStatement.getGeneratedKeys();
        if(rs.next()){
        	autoGenKey = rs.getInt(1);
        }
		
        return autoGenKey;
	}


		

}
