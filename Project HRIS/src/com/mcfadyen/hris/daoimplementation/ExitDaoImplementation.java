package com.mcfadyen.hris.daoimplementation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mcfadyen.hris.beans.Bean;
import com.mcfadyen.hris.beans.ExitFormBean;
import com.mcfadyen.hris.dao.Dao;

public class ExitDaoImplementation implements Dao {
	
	Connection con = null;
	Date date = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

	@Override
	public void insert(Bean bean) {
		
		ExitFormBean exitBean = (ExitFormBean)bean;
		String insertResponse = "INSERT INTO exit_form_question_response (emp_id, question_id, option_id, responce, remarks, created_by, created_on) VALUES (?,?,?,?,?,?,?)";
		String exitFormQuestions = "SELECT * FROM exit_form_question";
		try {
			
			Class.forName("com.mysql.jdbc.Driver");  
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hris","root","root");
			PreparedStatement statement = con.prepareStatement(insertResponse);
			Statement questions = con.createStatement();
			ResultSet rs = questions.executeQuery(exitFormQuestions);
			while(rs.next()){
				int questionID = rs.getInt("question_id");
				statement.setString(1, "e 201");
				statement.setInt(2, questionID);
				statement.setInt(3,0);
				
				
				}
			}
		catch(Exception e){
			e.printStackTrace();
			}
		}

	@Override
	public Bean display(Bean displayBean) {
		return null;
	}

}
