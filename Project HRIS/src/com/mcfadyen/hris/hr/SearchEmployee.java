package com.mcfadyen.hris.hr;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/employee-search")
public class SearchEmployee extends HttpServlet implements DbConnection {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7539661764659592492L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Map<String, Object>> resultList = empSearch(req);
		req.setAttribute("resultList", resultList);
		RequestDispatcher dispatcher = req.getRequestDispatcher("employee-search.jsp");
		dispatcher.forward(req, resp);
	}

	private static List<Map<String, Object>> empSearch(HttpServletRequest req) {

		String searchType = req.getParameter("type");
		String searchText = req.getParameter("searchText");
		try {
			Class.forName(DRIVER);
			Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			Statement stmt = con.createStatement();
			StringBuilder query = new StringBuilder(
					"SELECT emp_id,first_name,middle_name,last_name,gender,date_of_birth,phone_number_1,email_id  from hris.personal_details ");
			if (null != searchText && searchText != "") {
				switch (searchType) {
				case "employeeId": {
					query.append("where emp_id like '%" + searchText + "%' ");
					break;
				}
				case "employeeName": {
					query.append(" where first_name like '%" + searchText + "%' or " + "middle_name like '%"
							+ searchText + "%' or " + "last_name like '%" + searchText + "%' ");
					break;
				}
				case "phoneNumber": {
					query.append("where phone_number_1 like '%" + searchText + "%' or phone_number_2 like '%"
							+ searchText + "%' ");
					break;
				}
				case "email": {
					query.append("where email_id like '%" + searchText + "%' ");
					break;
				}
				}
			}
			ResultSet rs = stmt.executeQuery(query.toString());
			List<Map<String, Object>> resultList = new LinkedList<Map<String, Object>>();
			ResultSetMetaData meta = rs.getMetaData();
			while (rs.next()) {
				Map<String, Object> currentRow = new LinkedHashMap<String, Object>();
				currentRow.put(meta.getColumnName(1), rs.getString(1));
				currentRow.put(meta.getColumnName(2), rs.getString(2));
				currentRow.put(meta.getColumnName(3), rs.getString(3));
				currentRow.put(meta.getColumnName(4), rs.getString(4));
				currentRow.put(meta.getColumnName(5), rs.getString(5));
				currentRow.put(meta.getColumnName(6), rs.getDate(6));
				currentRow.put(meta.getColumnName(7), rs.getString(7));
				currentRow.put(meta.getColumnName(8), rs.getString(8));
				resultList.add(currentRow);
			}
			return resultList;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}
