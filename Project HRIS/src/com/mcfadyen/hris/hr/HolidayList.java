package com.mcfadyen.hris.hr;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/holiday")
public class HolidayList extends HttpServlet implements DbConnection {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7099753518998446743L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String json = new Gson().toJson(getHolidays(request));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json);

	}

	private List<Map<String, Object>> getHolidays(HttpServletRequest req) {
		String location = req.getParameter("location");

		try {
			Class.forName(DRIVER);
			Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			Statement st = con.createStatement();

			String query = "Select * from hris.holiday_list";
			if (!location.equals("all")) {
				query += " where holiday_location = '" + location + "'";
			}
			ResultSet rs = st.executeQuery(query);
			List<Map<String, Object>> resultList = new LinkedList<Map<String, Object>>();
			while (rs.next()) {
				Map<String, Object> mp = new LinkedHashMap<String, Object>();
				mp.put("holidayId", rs.getInt(1));
				mp.put("holidayName", rs.getString(2));
				mp.put("holidayDate", rs.getDate(3));
				mp.put("holidayLocation", rs.getString(4));
				resultList.add(mp);
			}
			return resultList;
		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {

		}

		return null;

	}
}
