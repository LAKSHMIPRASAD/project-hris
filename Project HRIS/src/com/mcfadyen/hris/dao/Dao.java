package com.mcfadyen.hris.dao;

import com.mcfadyen.hris.beans.*;

 public interface Dao{
	public void insert(Bean bean);
	public Bean display(Bean displayBean);
}
