package com.mcfadyen.hris.dao;

import com.mcfadyen.hris.beans.ProfileBean;

public interface PersonalDaoInterface {
	public void insert();
	public void insert(ProfileBean bean);
	public ProfileBean display(ProfileBean displayBean);

}
