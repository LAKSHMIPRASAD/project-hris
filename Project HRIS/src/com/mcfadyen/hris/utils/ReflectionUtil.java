package com.mcfadyen.hris.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

public class ReflectionUtil {
	
	public static Object CreateBean(Class beanClassObject, HttpServletRequest request ){
		int index=0,length;
		String[] name;
		
		Object beanObject = null;
		
		try {
			
             beanObject=beanClassObject.getConstructor().newInstance();
			Method[] methods=beanClassObject.getMethods();
			length=methods.length;
			
			
			
			 name=new String[length];
		
		
			for(Method method : methods){
				
                 if(isSetter(method)) {
              
			    name[index]=method.getName();	
			 
                	 
		    	String variable=ConvertToVariable(name[index]);
		   
		    	/**
		    	 * method = beanClassObject.getDeclaredMethod(name[index], paramString);
		    	 */
		    
		    	
				index++;
				method.invoke(beanObject, request.getParameter(variable));
				
				
			}
			
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Exception in Reflection class");
		}
		
		return beanObject;
		
	}
	
	
	
	private static boolean isSetter(Method method) {
		if (!method.getName().startsWith("set"))
			return false;
		if (method.getParameterTypes().length != 1)
			return false;
		return true;
	}
	
	private static String ConvertToVariable(String methodName){
		return (methodName.charAt(3)+"").toLowerCase()+methodName.substring(4);
		
	}

}
