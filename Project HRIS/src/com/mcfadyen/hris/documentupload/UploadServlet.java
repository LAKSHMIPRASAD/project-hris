package com.mcfadyen.hris.documentupload;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.mcfadyen.hris.databaseconnection.DatabaseAccess;

/**
 * Servlet implementation class UploadServlet
 */
@WebServlet("/UploadServlet")
@MultipartConfig
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		Part filePart = request.getPart("file");
		String fileName = getSubmittedFileName(filePart);
		//try {
			//DatabaseAccess da = new DatabaseAccess();
			//Connection conn = da.getConnectionPool("mysql", "test");
			/*String sql = "INSERT INTO person (first_name, last_name, photo) values(?, ?, ?)";
			PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "A");
            statement.setString(2, "B");
            InputStream inputStream = filePart.getInputStream();
            statement.setBlob(3, inputStream);
            statement.executeUpdate();
            
            conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} */
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/FileUpload.jsp");
		request.setAttribute("name", name);
		request.setAttribute("fileName", fileName);
		rd.forward(request, response);
	}
	
	private static String getSubmittedFileName(Part part)
	{
		for (String cd : part.getHeader("content-disposition").split(";")) {
	        if (cd.trim().startsWith("filename")) {
	            String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
	            return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
	        }
	    }
	    return null;
	}

}
