package com.mcfadyen.userauthentication;

import static com.mcfadyen.userauthentication.AuthenticationConstants.LDAP_BASE_DN;
import static com.mcfadyen.userauthentication.AuthenticationConstants.LDAP_BIND_DN;
import static com.mcfadyen.userauthentication.AuthenticationConstants.LDAP_BIND_PASSWORD;
import static com.mcfadyen.userauthentication.AuthenticationConstants.LDAP_SERVER;
import static com.mcfadyen.userauthentication.AuthenticationConstants.LDAP_SERVER_PORT;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.SizeLimitExceededException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.registry.infomodel.User;

import com.mcfadyen.hris.util.PropertyUtils;

public class UserAuthentication {
	

	public boolean authenticateUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// get properties from the properties file
		PropertyUtils ru=new PropertyUtils();
		Properties prop=ru.getLdapProperties();
		// TODO Auto-generated method stub
		String action = request.getParameter("customAction");
		Hashtable<String, String> env = new Hashtable<String, String>();
		String userName = request.getParameter("username");
		String userPassword = request.getParameter("password");
		env.put(Context.REFERRAL, "follow");
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, "ldap://" + prop.getProperty(LDAP_SERVER) + ":" +prop.getProperty(LDAP_SERVER_PORT) + "/" + prop.getProperty(LDAP_BASE_DN));

		// To get rid of the PartialResultException when using Active Directory

		// Needed for the Bind (User Authorized to Query the LDAP server)
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, prop.getProperty(LDAP_BIND_DN));
		env.put(Context.SECURITY_CREDENTIALS, prop.getProperty(LDAP_BIND_PASSWORD));
		DirContext ctx;
		try {
			ctx = new InitialDirContext(env);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}

		NamingEnumeration<SearchResult> results = null;

		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE); // Search
																	// Entire
																	// Subtree
			controls.setCountLimit(1); // Sets the maximum number of entries to
										// be returned as a result of the search
			controls.setTimeLimit(5000); // Sets the time limit of these
											// SearchControls in milliseconds

			String searchString = "(&(objectCategory=user)(sAMAccountName=" + userName + "))";

			results = ctx.search("", searchString, controls);

			if (results.hasMore()) {

				SearchResult result = (SearchResult) results.next();
				Attributes attrs = result.getAttributes();
				Attribute dnAttr = attrs.get("distinguishedName");
				String dn = (String) dnAttr.get();

				// User Exists, Validate the Password

				env.put(Context.SECURITY_PRINCIPAL, dn);
				// System.out.println(userPassword);

				env.put(Context.SECURITY_CREDENTIALS, userPassword);
				new InitialDirContext(env);
				return true;
				// Exception will be thrown on Invalid case
			} else {
				return false;
			}

		} catch (AuthenticationException e) { // Invalid Login
			request.setAttribute("error", "Invalid UserName or Password");
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("login.jsp");
			dispatcher1.forward(request, response);
		} catch (NameNotFoundException e) { // The base context was not found.
			return false;
		} catch (SizeLimitExceededException e) {
			throw new RuntimeException("LDAP Query Limit Exceeded, adjust the query to bring back less records", e);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		} finally {

			if (results != null) {
				try {
					results.close();
				} catch (Exception e) {
				}
			}

			if (ctx != null) {
				try {
					ctx.close();
				} catch (Exception e) {
				}
			}
		}
		return false;

	}

	private static LdapContext getLdapContext()  {
		LdapContext ctx = null;
		try {
			PropertyUtils ru=new PropertyUtils();
			Properties prop=ru.getLdapProperties();
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, "ldap://" + prop.getProperty(LDAP_SERVER) + ":" + prop.getProperty(LDAP_SERVER_PORT) + "/" + prop.getProperty(LDAP_BASE_DN));
			env.put(Context.REFERRAL, "follow");
			// Needed for the Bind (User Authorized to Query the LDAP server)
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, prop.getProperty(LDAP_BIND_DN));
			env.put(Context.SECURITY_CREDENTIALS, prop.getProperty(LDAP_BIND_PASSWORD));
			ctx = new InitialLdapContext(env, null);
			System.out.println("LDAP Connection: COMPLETE");
		} catch (NamingException nex) {
			System.out.println("LDAP Connection: FAILED");
			nex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ctx;
	}

	private static SearchControls getSearchControls() {
		SearchControls cons = new SearchControls();
		cons.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String[] attrIDs = { "displayName", "sn", "givenname", "mail", "mobile", "thumbnailPhoto" };
		cons.setReturningAttributes(attrIDs);
		return cons;
	}

	public String getDisplayName(String userName) {
		LdapContext ldapContext = getLdapContext();
		SearchControls searchControls = getSearchControls();
		return getUserName(userName, ldapContext, searchControls);
	}

	private static String getUserName(String userName, LdapContext ctx, SearchControls searchControls) {
		User user = null;
		try {
			PropertyUtils ru=new PropertyUtils();
			Properties prop=ru.getLdapProperties();
			NamingEnumeration<SearchResult> answer = ctx.search(
					"ldap://" + prop.getProperty(LDAP_SERVER) + ":" + prop.getProperty(LDAP_SERVER_PORT) + "/" + prop.getProperty(LDAP_BASE_DN), "sAMAccountName=" + userName,
					searchControls);
			if (answer.hasMore()) {
				Attributes attrs = answer.next().getAttributes();
				Attribute displayName=(Attribute)attrs.get("displayName");
				return displayName.get().toString();
			} 
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
