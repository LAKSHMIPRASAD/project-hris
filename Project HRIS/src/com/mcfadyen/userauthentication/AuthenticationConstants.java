package com.mcfadyen.userauthentication;

public class AuthenticationConstants {
	
	public static final String LDAP_SERVER = "LDAP_SERVER";
	public static final String LDAP_SERVER_PORT = "LDAP_SERVER_PORT";
	public static final String LDAP_BASE_DN = "LDAP_BASE_DN";
	public static final String LDAP_BIND_DN = "LDAP_BIND_DN";
	public static final String LDAP_BIND_PASSWORD = "LDAP_BIND_PASSWORD";
}
