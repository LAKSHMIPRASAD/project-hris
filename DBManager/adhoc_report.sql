create or replace view adhoc_report as
select personal_details.emp_id,personal_details.first_name, personal_details.middle_name,personal_details.last_name, personal_details.gender,
personal_details.email_id,personal_details.date_of_birth,personal_details.emergency_contact_number,personal_details.Relation,
passport_details.passport_number, passport_details.date_of_issue,passport_details.date_of_expiry as passport_date_of_expiry,
visa_details.visa_type, visa_details.date_of_expiry as visa_date_of_expiry, visa_details.country as visa_country,
messenger_table.type_of_messenger,
employment_details.location, employment_details.date_of_joining, employment_details.designation,employment_details.company_name,
CONCAT_ws('\n',a.line1,a.line2,Char(32), a.city,Char(32),  a.state,Char(32), a.country,Char(32), a.pincode) as current_address,
CONCAT_ws('\n',b.line1,b.line2,Char(32), b.city,Char(32),  b.state,Char(32), b.country,Char(32), b.pincode) as permanent_address


from  personal_details,passport_details,visa_details,employment_details,messenger_table,address_table a,address_table b
where (personal_details.emp_id=passport_details.emp_id) and (passport_details.passport_number=visa_details.passport_number)
  and(personal_details.messenger_id=messenger_table.messenger_id)and(employment_details.emp_id=personal_details.emp_id)and(personal_details.current_address_id=a.address_id and personal_details.permanent_address_id=b.address_id);
