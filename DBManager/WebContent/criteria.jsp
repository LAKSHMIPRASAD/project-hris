<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<select class="column_name" name="criteria">
        	<option selected="selected">Column</option>
	        <option value="emp_id">Employee ID</option>
	        <option value="first_name">First Name</option>
	        <option value="middle_name">Middle Name</option>
	        <option	value="last_name" >Last Name</option>
	        <option value="company_name">Company</option>
	        <option value="designation">Designation</option>
	        <option value="passport_number">Passport Number</option>
	        <option	value="visa_type" >Visa Type</option>
	        <option value="visa_country">Visa Country</option>
			<option value="type_of_messenger">Messenger</option>
        </select>
        <select class="condition" name="criteria">
        	<option selected="selected">Condtion</option>
        	<option value="=">Equal</option>
        	<option value="!=">Not Equal</option>
        	<option value='<'>Less than</option>
        	<option value='>'> Greater than</option>
        </select>
        <input type="text" name="criteria"/>