package com.db.manager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

public class DBConnection {
	
	
	public Map getCriterias() {
	
		return null;
	}
	public ArrayList<String> getSelectParameters(){
		ArrayList selectParameters=new ArrayList<>();
		selectParameters.add("emp_id");
		selectParameters.add("first_name");
		selectParameters.add("current_address_id");
		
		return selectParameters;
	}

	public static void main(String[] args) throws SQLException {
		Statement st=null;
		Connection con = null;
		ResultSet rs=null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hris", "root", "root");

		} catch (Exception err) {

			err.printStackTrace();
		}
		ArrayList selectParameters=new DBConnection().getSelectParameters();
		StringBuilder sb=new StringBuilder();
		for(int i = 0;i<selectParameters.size();i++){
			sb.append(selectParameters.get(i)+",");
		}
		int len=sb.length();
		sb.deleteCharAt(len-1);
		System.out.println(sb);
		st = con.createStatement();
		rs = st.executeQuery("SELECT "+sb+" FROM personal_details where emp_id='E 100'");
		while(rs.next()){
			System.out.println(rs.getString("emp_id"));
			System.out.println(rs.getString("first_name"));
			System.out.println(rs.getString("current_address_id"));
		}
	}
}
