package com.db.manager;
public class QueryCriteria implements java.io.Serializable {

	private String column_name;
	private String condition;
	private String value;
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public QueryCriteria(){
		
	}
	public QueryCriteria(String column_name, String condition, String value) {
		super();
		this.column_name = column_name;
		this.condition = condition;
		this.value = value;
	}
	
	public String toString(){
		return this.column_name+ " "+ this.condition+" '"+ this.value+"'";
	}
	
	public String and(QueryCriteria qc)
	{
		return "("+this+" and "+qc+")";
	}
	
	public String or(QueryCriteria qc)
	{
		return "("+this+" or "+qc+")";
	}
	public String or(String s){
		return "("+this+" or "+s+")";
	}
	public String and(String s){
		return "("+this+" and "+s+")";
	}
}
