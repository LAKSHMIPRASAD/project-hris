package com.db.manager;

import java.util.ArrayList;
import java.util.List;

public class QueryCriteria implements java.io.Serializable {

	private String condition;
	private String columnName;
	private String value;
	private String link;
	private QueryCriteria queryCriteria;
	private List selectParameter;
	
	
	public List getSelectParameter() {
		return selectParameter;
	}
	public void setSelectParameter(List selectParameter) {
		this.selectParameter = selectParameter;
	}
	public QueryCriteria getQueryCriteria() {
		return queryCriteria;
	}
	public void setQueryCriteria(QueryCriteria queryCriteria) {
		this.queryCriteria = queryCriteria;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public QueryCriteria(){
		
	}
	public QueryCriteria(String columnName, String condition, String value) {
		super();
		this.columnName = columnName;
		this.condition = condition;
		this.value = value;
	}
	
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString(){
			
			if("and".equals(this.link))
				return "("+this.columnName+ " "+ this.condition+" '"+ this.value+"'"+" and "+this.queryCriteria+")";
			else if("or".equals(this.link))
				return "("+this.columnName+ " "+ this.condition+" '"+ this.value+"'"+" or "+this.queryCriteria+")";
			else
			return this.columnName+ " "+ this.condition+" '"+ this.value+"'";
		}
		
		public String getLink() {
			return link;
		}
		public void setLink(String link) {
			this.link = link;
		}
		public String and(QueryCriteria qc)
		{
			return "("+this+" and "+qc+")";
		}
		
		public String or(QueryCriteria qc)
		{
			return "("+this+" or "+qc+")";
		}
		public String or(String s){
			return "("+this+" or "+s+")";
		}
		public String and(String s){
			return "("+this+" and "+s+")";
		}
		
		public static void main(String[] args) {
			QueryCriteria[] qca=new QueryCriteria[4];
			qca[0]=new QueryCriteria("a", "b", "c");
			qca[1]=new  QueryCriteria("x", "y", "z");
			qca[1].setLink("and");
			qca[1].setQueryCriteria(qca[0]);
			System.out.println(qca[1]);
		}
}
