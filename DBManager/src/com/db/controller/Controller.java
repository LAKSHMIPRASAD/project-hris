package com.db.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.StringUtils;

import com.db.manager.QueryCriteria;
import com.db.querybuilder.QueryBuilder;

@WebServlet(name = "Controller", description = "controller", urlPatterns = { "/Controller.do" })
public class Controller extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		QueryBuilder qb = new QueryBuilder();
		if(null!=request.getParameterValues("select")){
		qb.setSelectParameters(Arrays.asList(request.getParameterValues("select")));
		List criteria=Arrays.asList(request.getParameterValues("criteria"));
		System.out.println(criteria);
		System.out.println(criteria);
		QueryCriteria qcOne=new QueryCriteria();
		QueryCriteria[] qcArray=new QueryCriteria[20];
		int j=0;
		int criteriaSize = criteria.size();
		System.out.println(!criteria.get(2).toString().equals(""));
		
		if(criteriaSize<5 ){
			if(criteria.get(2)!="")
			qb.setQueryCriteria(new QueryCriteria(criteria.get(0).toString(), criteria.get(1).toString(), criteria.get(2).toString()));
		}else{
			for(int i=0;i<=criteriaSize/4;i++){
				j=i;
				qcArray[i]=new QueryCriteria(criteria.get(4*i).toString(), criteria.get(4*i+1).toString(), criteria.get(4*i+2).toString());
				System.out.println(qcArray[i]);
			}
			for(int i=j;i>0;i--){
				qcArray[i].setQueryCriteria(qcArray[i-1]);
				qcArray[i].setLink(criteria.get(4*(i-1)+3).toString());
				System.out.println(qcArray[i].getLink()+qcArray[i].getQueryCriteria());
				System.out.println(qcArray[i].toString());
			}
			qb.setQueryCriteria(qcArray[j]);
		}
		
		
		
		System.out.println(qb.getQueryCriteria());
		qb.setQuery();
		System.out.println(qb.getQuery());
		request.setAttribute("query", qb.getQuery());
		try {
			ResultSet rs = qb.getResultSet();
			if(rs!=null){
				
			
			int columnCount = rs.getMetaData().getColumnCount();
			ArrayList columnNames=new ArrayList<>();
			for(int i =1;i<=columnCount;i++)
				columnNames.add(rs.getMetaData().getColumnLabel(i));
			
//			int rowCount=rs.getMetaData().
			ArrayList results=new ArrayList();
			int rowCount=0;
			while (rs.next()) {
				ArrayList result = new ArrayList();
				rowCount++;
				for (int i = 1; i <= columnCount; i++) {
//					System.out.print(rs.getString(i) + "\t");
					result.add(rs.getString(i));
				}
				results.add(result);
			}
			request.setAttribute("columnNames", columnNames);
			request.setAttribute("rowCount", rowCount);
			request.setAttribute("results", results);
			request.setAttribute("columnCount", columnCount);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("adhoc.jsp");
		dispatcher.forward(request, response);
		}
		else{
			RequestDispatcher dispatcher = request.getRequestDispatcher("adhoc.jsp");
			dispatcher.forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
